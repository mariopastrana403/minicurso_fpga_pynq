// Copyright 1986-2018 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2018.3 (lin64) Build 2405991 Thu Dec  6 23:36:41 MST 2018
// Date        : Sat Jan  7 14:38:04 2023
// Host        : albano-Satellite-C655 running 64-bit Ubuntu 22.04.1 LTS
// Command     : write_verilog -force -mode synth_stub
//               /home/albano/VideosTutoriales/PynqFPGA/Semana4/Semana4PWMTest/Semana4PWMTest.srcs/sources_1/bd/semana3/ip/semana3_comblock_0_0/semana3_comblock_0_0_stub.v
// Design      : semana3_comblock_0_0
// Purpose     : Stub declaration of top-level module interface
// Device      : xc7z020clg400-1
// --------------------------------------------------------------------------------

// This empty module with port declaration file causes synthesis tools to infer a black box for IP.
// The synthesis directives are for Synopsys Synplify support to prevent IO buffer insertion.
// Please paste the declaration into a Verilog source file or add the file as an additional source.
(* x_core_info = "axi_comblock,Vivado 2018.3" *)
module semana3_comblock_0_0(reg0_o, reg1_o, reg2_o, reg3_o, axil_aclk, 
  axil_aresetn, axil_awaddr, axil_awprot, axil_awvalid, axil_awready, axil_wdata, axil_wstrb, 
  axil_wvalid, axil_wready, axil_bresp, axil_bvalid, axil_bready, axil_araddr, axil_arprot, 
  axil_arvalid, axil_arready, axil_rdata, axil_rresp, axil_rvalid, axil_rready)
/* synthesis syn_black_box black_box_pad_pin="reg0_o[7:0],reg1_o[7:0],reg2_o[7:0],reg3_o[7:0],axil_aclk,axil_aresetn,axil_awaddr[7:0],axil_awprot[2:0],axil_awvalid,axil_awready,axil_wdata[31:0],axil_wstrb[3:0],axil_wvalid,axil_wready,axil_bresp[1:0],axil_bvalid,axil_bready,axil_araddr[7:0],axil_arprot[2:0],axil_arvalid,axil_arready,axil_rdata[31:0],axil_rresp[1:0],axil_rvalid,axil_rready" */;
  output [7:0]reg0_o;
  output [7:0]reg1_o;
  output [7:0]reg2_o;
  output [7:0]reg3_o;
  input axil_aclk;
  input axil_aresetn;
  input [7:0]axil_awaddr;
  input [2:0]axil_awprot;
  input axil_awvalid;
  output axil_awready;
  input [31:0]axil_wdata;
  input [3:0]axil_wstrb;
  input axil_wvalid;
  output axil_wready;
  output [1:0]axil_bresp;
  output axil_bvalid;
  input axil_bready;
  input [7:0]axil_araddr;
  input [2:0]axil_arprot;
  input axil_arvalid;
  output axil_arready;
  output [31:0]axil_rdata;
  output [1:0]axil_rresp;
  output axil_rvalid;
  input axil_rready;
endmodule
