-- Copyright 1986-2018 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2018.3 (lin64) Build 2405991 Thu Dec  6 23:36:41 MST 2018
-- Date        : Sat Jan  7 14:37:45 2023
-- Host        : albano-Satellite-C655 running 64-bit Ubuntu 22.04.1 LTS
-- Command     : write_vhdl -force -mode synth_stub
--               /home/albano/VideosTutoriales/PynqFPGA/Semana4/Semana4PWMTest/Semana4PWMTest.srcs/sources_1/bd/semana3/ip/semana3_PWM_V1_0_0/semana3_PWM_V1_0_0_stub.vhdl
-- Design      : semana3_PWM_V1_0_0
-- Purpose     : Stub declaration of top-level module interface
-- Device      : xc7z020clg400-1
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity semana3_PWM_V1_0_0 is
  Port ( 
    dc : in STD_LOGIC_VECTOR ( 7 downto 0 );
    clk : in STD_LOGIC;
    reset : in STD_LOGIC;
    pwm : out STD_LOGIC
  );

end semana3_PWM_V1_0_0;

architecture stub of semana3_PWM_V1_0_0 is
attribute syn_black_box : boolean;
attribute black_box_pad_pin : string;
attribute syn_black_box of stub : architecture is true;
attribute black_box_pad_pin of stub : architecture is "dc[7:0],clk,reset,pwm";
attribute x_core_info : string;
attribute x_core_info of stub : architecture is "PWM_V1,Vivado 2018.3";
begin
end;
