// Copyright 1986-2018 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2018.3 (lin64) Build 2405991 Thu Dec  6 23:36:41 MST 2018
// Date        : Sat Jan  7 14:37:45 2023
// Host        : albano-Satellite-C655 running 64-bit Ubuntu 22.04.1 LTS
// Command     : write_verilog -force -mode funcsim -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
//               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ semana3_PWM_V1_0_0_sim_netlist.v
// Design      : semana3_PWM_V1_0_0
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7z020clg400-1
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_PWM_V1
   (pwm,
    clk,
    dc,
    reset);
  output pwm;
  input clk;
  input [7:0]dc;
  input reset;

  wire clk;
  wire [23:0]count;
  wire [23:0]count_0;
  wire \count_reg[12]_i_2_n_0 ;
  wire \count_reg[12]_i_2_n_1 ;
  wire \count_reg[12]_i_2_n_2 ;
  wire \count_reg[12]_i_2_n_3 ;
  wire \count_reg[16]_i_2_n_0 ;
  wire \count_reg[16]_i_2_n_1 ;
  wire \count_reg[16]_i_2_n_2 ;
  wire \count_reg[16]_i_2_n_3 ;
  wire \count_reg[20]_i_2_n_0 ;
  wire \count_reg[20]_i_2_n_1 ;
  wire \count_reg[20]_i_2_n_2 ;
  wire \count_reg[20]_i_2_n_3 ;
  wire \count_reg[23]_i_2_n_2 ;
  wire \count_reg[23]_i_2_n_3 ;
  wire \count_reg[4]_i_2_n_0 ;
  wire \count_reg[4]_i_2_n_1 ;
  wire \count_reg[4]_i_2_n_2 ;
  wire \count_reg[4]_i_2_n_3 ;
  wire \count_reg[8]_i_2_n_0 ;
  wire \count_reg[8]_i_2_n_1 ;
  wire \count_reg[8]_i_2_n_2 ;
  wire \count_reg[8]_i_2_n_3 ;
  wire [23:1]data0;
  wire [7:0]dc;
  wire p_0_in;
  wire [18:0]preset;
  wire preset0_n_100;
  wire preset0_n_101;
  wire preset0_n_102;
  wire preset0_n_103;
  wire preset0_n_104;
  wire preset0_n_105;
  wire preset0_n_78;
  wire preset0_n_79;
  wire preset0_n_80;
  wire preset0_n_81;
  wire preset0_n_82;
  wire preset0_n_83;
  wire preset0_n_84;
  wire preset0_n_85;
  wire preset0_n_86;
  wire preset0_n_87;
  wire preset0_n_88;
  wire preset0_n_89;
  wire preset0_n_90;
  wire preset0_n_91;
  wire preset0_n_92;
  wire preset0_n_93;
  wire preset0_n_94;
  wire preset0_n_95;
  wire preset0_n_96;
  wire preset0_n_97;
  wire preset0_n_98;
  wire preset0_n_99;
  wire preset__145_carry__0_i_1_n_0;
  wire preset__145_carry__0_i_2_n_0;
  wire preset__145_carry__0_i_3_n_0;
  wire preset__145_carry__0_i_4_n_0;
  wire preset__145_carry__0_n_0;
  wire preset__145_carry__0_n_1;
  wire preset__145_carry__0_n_2;
  wire preset__145_carry__0_n_3;
  wire preset__145_carry__0_n_4;
  wire preset__145_carry__0_n_5;
  wire preset__145_carry__0_n_6;
  wire preset__145_carry__0_n_7;
  wire preset__145_carry__1_i_1_n_0;
  wire preset__145_carry__1_i_2_n_0;
  wire preset__145_carry__1_i_3_n_0;
  wire preset__145_carry__1_i_4_n_0;
  wire preset__145_carry__1_n_0;
  wire preset__145_carry__1_n_1;
  wire preset__145_carry__1_n_2;
  wire preset__145_carry__1_n_3;
  wire preset__145_carry__1_n_4;
  wire preset__145_carry__1_n_5;
  wire preset__145_carry__1_n_6;
  wire preset__145_carry__1_n_7;
  wire preset__145_carry__2_i_1_n_0;
  wire preset__145_carry__2_i_2_n_0;
  wire preset__145_carry__2_i_3_n_0;
  wire preset__145_carry__2_i_4_n_0;
  wire preset__145_carry__2_n_0;
  wire preset__145_carry__2_n_1;
  wire preset__145_carry__2_n_2;
  wire preset__145_carry__2_n_3;
  wire preset__145_carry__2_n_4;
  wire preset__145_carry__2_n_5;
  wire preset__145_carry__2_n_6;
  wire preset__145_carry__2_n_7;
  wire preset__145_carry__3_i_1_n_0;
  wire preset__145_carry__3_i_2_n_0;
  wire preset__145_carry__3_i_3_n_0;
  wire preset__145_carry__3_i_4_n_0;
  wire preset__145_carry__3_n_0;
  wire preset__145_carry__3_n_1;
  wire preset__145_carry__3_n_2;
  wire preset__145_carry__3_n_3;
  wire preset__145_carry__3_n_4;
  wire preset__145_carry__3_n_5;
  wire preset__145_carry__3_n_6;
  wire preset__145_carry__3_n_7;
  wire preset__145_carry__4_i_1_n_0;
  wire preset__145_carry__4_i_2_n_0;
  wire preset__145_carry__4_i_3_n_0;
  wire preset__145_carry__4_i_4_n_0;
  wire preset__145_carry__4_n_0;
  wire preset__145_carry__4_n_1;
  wire preset__145_carry__4_n_2;
  wire preset__145_carry__4_n_3;
  wire preset__145_carry__4_n_4;
  wire preset__145_carry__4_n_5;
  wire preset__145_carry__4_n_6;
  wire preset__145_carry__4_n_7;
  wire preset__145_carry__5_i_1_n_0;
  wire preset__145_carry__5_i_2_n_0;
  wire preset__145_carry__5_i_3_n_0;
  wire preset__145_carry__5_i_4_n_0;
  wire preset__145_carry__5_n_0;
  wire preset__145_carry__5_n_1;
  wire preset__145_carry__5_n_2;
  wire preset__145_carry__5_n_3;
  wire preset__145_carry__5_n_4;
  wire preset__145_carry__5_n_5;
  wire preset__145_carry__5_n_6;
  wire preset__145_carry__5_n_7;
  wire preset__145_carry__6_i_1_n_0;
  wire preset__145_carry__6_i_2_n_0;
  wire preset__145_carry__6_i_3_n_0;
  wire preset__145_carry__6_n_0;
  wire preset__145_carry__6_n_2;
  wire preset__145_carry__6_n_3;
  wire preset__145_carry__6_n_5;
  wire preset__145_carry__6_n_6;
  wire preset__145_carry__6_n_7;
  wire preset__145_carry_i_1_n_0;
  wire preset__145_carry_i_2_n_0;
  wire preset__145_carry_i_3_n_0;
  wire preset__145_carry_n_0;
  wire preset__145_carry_n_1;
  wire preset__145_carry_n_2;
  wire preset__145_carry_n_3;
  wire preset__145_carry_n_4;
  wire preset__145_carry_n_5;
  wire preset__145_carry_n_6;
  wire preset__207_carry__0_i_1_n_0;
  wire preset__207_carry__0_i_2_n_0;
  wire preset__207_carry__0_i_3_n_0;
  wire preset__207_carry__0_i_4_n_0;
  wire preset__207_carry__0_n_0;
  wire preset__207_carry__0_n_1;
  wire preset__207_carry__0_n_2;
  wire preset__207_carry__0_n_3;
  wire preset__207_carry__0_n_4;
  wire preset__207_carry__0_n_5;
  wire preset__207_carry__0_n_6;
  wire preset__207_carry__0_n_7;
  wire preset__207_carry__1_i_1_n_0;
  wire preset__207_carry__1_i_2_n_0;
  wire preset__207_carry__1_i_3_n_0;
  wire preset__207_carry__1_i_4_n_0;
  wire preset__207_carry__1_n_0;
  wire preset__207_carry__1_n_1;
  wire preset__207_carry__1_n_2;
  wire preset__207_carry__1_n_3;
  wire preset__207_carry__1_n_4;
  wire preset__207_carry__1_n_5;
  wire preset__207_carry__1_n_6;
  wire preset__207_carry__1_n_7;
  wire preset__207_carry__2_i_1_n_0;
  wire preset__207_carry__2_i_2_n_0;
  wire preset__207_carry__2_i_3_n_0;
  wire preset__207_carry__2_i_4_n_0;
  wire preset__207_carry__2_n_0;
  wire preset__207_carry__2_n_1;
  wire preset__207_carry__2_n_2;
  wire preset__207_carry__2_n_3;
  wire preset__207_carry__2_n_4;
  wire preset__207_carry__2_n_5;
  wire preset__207_carry__2_n_6;
  wire preset__207_carry__2_n_7;
  wire preset__207_carry__3_i_1_n_0;
  wire preset__207_carry__3_i_2_n_0;
  wire preset__207_carry__3_i_3_n_0;
  wire preset__207_carry__3_i_4_n_0;
  wire preset__207_carry__3_n_0;
  wire preset__207_carry__3_n_1;
  wire preset__207_carry__3_n_2;
  wire preset__207_carry__3_n_3;
  wire preset__207_carry__3_n_4;
  wire preset__207_carry__3_n_5;
  wire preset__207_carry__3_n_6;
  wire preset__207_carry__3_n_7;
  wire preset__207_carry__4_i_1_n_0;
  wire preset__207_carry__4_i_2_n_0;
  wire preset__207_carry__4_i_3_n_0;
  wire preset__207_carry__4_i_4_n_0;
  wire preset__207_carry__4_n_0;
  wire preset__207_carry__4_n_1;
  wire preset__207_carry__4_n_2;
  wire preset__207_carry__4_n_3;
  wire preset__207_carry__4_n_4;
  wire preset__207_carry__4_n_5;
  wire preset__207_carry__4_n_6;
  wire preset__207_carry__4_n_7;
  wire preset__207_carry__5_i_1_n_0;
  wire preset__207_carry__5_i_2_n_0;
  wire preset__207_carry__5_i_3_n_0;
  wire preset__207_carry__5_i_4_n_0;
  wire preset__207_carry__5_n_0;
  wire preset__207_carry__5_n_1;
  wire preset__207_carry__5_n_2;
  wire preset__207_carry__5_n_3;
  wire preset__207_carry__5_n_4;
  wire preset__207_carry__5_n_5;
  wire preset__207_carry__5_n_6;
  wire preset__207_carry__5_n_7;
  wire preset__207_carry__6_i_1_n_0;
  wire preset__207_carry__6_i_2_n_0;
  wire preset__207_carry__6_i_3_n_0;
  wire preset__207_carry__6_n_2;
  wire preset__207_carry__6_n_3;
  wire preset__207_carry__6_n_5;
  wire preset__207_carry__6_n_6;
  wire preset__207_carry__6_n_7;
  wire preset__207_carry_i_1_n_0;
  wire preset__207_carry_i_2_n_0;
  wire preset__207_carry_i_3_n_0;
  wire preset__207_carry_n_0;
  wire preset__207_carry_n_1;
  wire preset__207_carry_n_2;
  wire preset__207_carry_n_3;
  wire preset__207_carry_n_4;
  wire preset__207_carry_n_5;
  wire preset__207_carry_n_6;
  wire preset__268_carry__0_i_1_n_0;
  wire preset__268_carry__0_i_2_n_0;
  wire preset__268_carry__0_i_3_n_0;
  wire preset__268_carry__0_i_4_n_0;
  wire preset__268_carry__0_i_5_n_0;
  wire preset__268_carry__0_i_6_n_0;
  wire preset__268_carry__0_i_7_n_0;
  wire preset__268_carry__0_i_8_n_0;
  wire preset__268_carry__0_n_0;
  wire preset__268_carry__0_n_1;
  wire preset__268_carry__0_n_2;
  wire preset__268_carry__0_n_3;
  wire preset__268_carry__1_i_10_n_0;
  wire preset__268_carry__1_i_1_n_0;
  wire preset__268_carry__1_i_2_n_0;
  wire preset__268_carry__1_i_3_n_0;
  wire preset__268_carry__1_i_4_n_0;
  wire preset__268_carry__1_i_5_n_0;
  wire preset__268_carry__1_i_6_n_0;
  wire preset__268_carry__1_i_7_n_0;
  wire preset__268_carry__1_i_8_n_0;
  wire preset__268_carry__1_i_9_n_0;
  wire preset__268_carry__1_n_0;
  wire preset__268_carry__1_n_1;
  wire preset__268_carry__1_n_2;
  wire preset__268_carry__1_n_3;
  wire preset__268_carry__2_i_10_n_0;
  wire preset__268_carry__2_i_11_n_0;
  wire preset__268_carry__2_i_12_n_0;
  wire preset__268_carry__2_i_13_n_0;
  wire preset__268_carry__2_i_14_n_0;
  wire preset__268_carry__2_i_15_n_0;
  wire preset__268_carry__2_i_16_n_0;
  wire preset__268_carry__2_i_1_n_0;
  wire preset__268_carry__2_i_2_n_0;
  wire preset__268_carry__2_i_3_n_0;
  wire preset__268_carry__2_i_4_n_0;
  wire preset__268_carry__2_i_5_n_0;
  wire preset__268_carry__2_i_6_n_0;
  wire preset__268_carry__2_i_7_n_0;
  wire preset__268_carry__2_i_8_n_0;
  wire preset__268_carry__2_i_9_n_0;
  wire preset__268_carry__2_n_0;
  wire preset__268_carry__2_n_1;
  wire preset__268_carry__2_n_2;
  wire preset__268_carry__2_n_3;
  wire preset__268_carry__3_i_10_n_0;
  wire preset__268_carry__3_i_11_n_0;
  wire preset__268_carry__3_i_12_n_0;
  wire preset__268_carry__3_i_13_n_0;
  wire preset__268_carry__3_i_14_n_0;
  wire preset__268_carry__3_i_15_n_0;
  wire preset__268_carry__3_i_16_n_0;
  wire preset__268_carry__3_i_1_n_0;
  wire preset__268_carry__3_i_2_n_0;
  wire preset__268_carry__3_i_3_n_0;
  wire preset__268_carry__3_i_4_n_0;
  wire preset__268_carry__3_i_5_n_0;
  wire preset__268_carry__3_i_6_n_0;
  wire preset__268_carry__3_i_7_n_0;
  wire preset__268_carry__3_i_8_n_0;
  wire preset__268_carry__3_i_9_n_0;
  wire preset__268_carry__3_n_0;
  wire preset__268_carry__3_n_1;
  wire preset__268_carry__3_n_2;
  wire preset__268_carry__3_n_3;
  wire preset__268_carry__4_i_10_n_0;
  wire preset__268_carry__4_i_11_n_0;
  wire preset__268_carry__4_i_12_n_0;
  wire preset__268_carry__4_i_13_n_0;
  wire preset__268_carry__4_i_14_n_0;
  wire preset__268_carry__4_i_15_n_0;
  wire preset__268_carry__4_i_16_n_0;
  wire preset__268_carry__4_i_1_n_0;
  wire preset__268_carry__4_i_2_n_0;
  wire preset__268_carry__4_i_3_n_0;
  wire preset__268_carry__4_i_4_n_0;
  wire preset__268_carry__4_i_5_n_0;
  wire preset__268_carry__4_i_6_n_0;
  wire preset__268_carry__4_i_7_n_0;
  wire preset__268_carry__4_i_8_n_0;
  wire preset__268_carry__4_i_9_n_0;
  wire preset__268_carry__4_n_0;
  wire preset__268_carry__4_n_1;
  wire preset__268_carry__4_n_2;
  wire preset__268_carry__4_n_3;
  wire preset__268_carry__4_n_4;
  wire preset__268_carry__4_n_5;
  wire preset__268_carry__4_n_6;
  wire preset__268_carry__5_i_10_n_0;
  wire preset__268_carry__5_i_11_n_0;
  wire preset__268_carry__5_i_12_n_0;
  wire preset__268_carry__5_i_13_n_0;
  wire preset__268_carry__5_i_14_n_0;
  wire preset__268_carry__5_i_15_n_0;
  wire preset__268_carry__5_i_16_n_0;
  wire preset__268_carry__5_i_1_n_0;
  wire preset__268_carry__5_i_2_n_0;
  wire preset__268_carry__5_i_3_n_0;
  wire preset__268_carry__5_i_4_n_0;
  wire preset__268_carry__5_i_5_n_0;
  wire preset__268_carry__5_i_6_n_0;
  wire preset__268_carry__5_i_7_n_0;
  wire preset__268_carry__5_i_8_n_0;
  wire preset__268_carry__5_i_9_n_0;
  wire preset__268_carry__5_n_0;
  wire preset__268_carry__5_n_1;
  wire preset__268_carry__5_n_2;
  wire preset__268_carry__5_n_3;
  wire preset__268_carry__5_n_4;
  wire preset__268_carry__5_n_5;
  wire preset__268_carry__5_n_6;
  wire preset__268_carry__5_n_7;
  wire preset__268_carry__6_i_10_n_0;
  wire preset__268_carry__6_i_11_n_0;
  wire preset__268_carry__6_i_12_n_0;
  wire preset__268_carry__6_i_13_n_0;
  wire preset__268_carry__6_i_14_n_0;
  wire preset__268_carry__6_i_15_n_0;
  wire preset__268_carry__6_i_1_n_0;
  wire preset__268_carry__6_i_2_n_0;
  wire preset__268_carry__6_i_3_n_0;
  wire preset__268_carry__6_i_4_n_0;
  wire preset__268_carry__6_i_5_n_0;
  wire preset__268_carry__6_i_6_n_0;
  wire preset__268_carry__6_i_7_n_0;
  wire preset__268_carry__6_i_8_n_0;
  wire preset__268_carry__6_i_9_n_0;
  wire preset__268_carry__6_n_0;
  wire preset__268_carry__6_n_1;
  wire preset__268_carry__6_n_2;
  wire preset__268_carry__6_n_3;
  wire preset__268_carry__6_n_4;
  wire preset__268_carry__6_n_5;
  wire preset__268_carry__6_n_6;
  wire preset__268_carry__6_n_7;
  wire preset__268_carry__7_i_10_n_0;
  wire preset__268_carry__7_i_11_n_0;
  wire preset__268_carry__7_i_12_n_0;
  wire preset__268_carry__7_i_13_n_0;
  wire preset__268_carry__7_i_14_n_0;
  wire preset__268_carry__7_i_15_n_0;
  wire preset__268_carry__7_i_16_n_0;
  wire preset__268_carry__7_i_1_n_0;
  wire preset__268_carry__7_i_2_n_0;
  wire preset__268_carry__7_i_3_n_0;
  wire preset__268_carry__7_i_4_n_0;
  wire preset__268_carry__7_i_5_n_0;
  wire preset__268_carry__7_i_6_n_0;
  wire preset__268_carry__7_i_7_n_0;
  wire preset__268_carry__7_i_8_n_0;
  wire preset__268_carry__7_i_9_n_0;
  wire preset__268_carry__7_n_0;
  wire preset__268_carry__7_n_1;
  wire preset__268_carry__7_n_2;
  wire preset__268_carry__7_n_3;
  wire preset__268_carry__7_n_4;
  wire preset__268_carry__7_n_5;
  wire preset__268_carry__7_n_6;
  wire preset__268_carry__7_n_7;
  wire preset__268_carry__8_i_10_n_0;
  wire preset__268_carry__8_i_11_n_0;
  wire preset__268_carry__8_i_12_n_0;
  wire preset__268_carry__8_i_13_n_0;
  wire preset__268_carry__8_i_14_n_0;
  wire preset__268_carry__8_i_15_n_0;
  wire preset__268_carry__8_i_16_n_0;
  wire preset__268_carry__8_i_1_n_0;
  wire preset__268_carry__8_i_2_n_0;
  wire preset__268_carry__8_i_3_n_0;
  wire preset__268_carry__8_i_4_n_0;
  wire preset__268_carry__8_i_5_n_0;
  wire preset__268_carry__8_i_6_n_0;
  wire preset__268_carry__8_i_7_n_0;
  wire preset__268_carry__8_i_8_n_0;
  wire preset__268_carry__8_i_9_n_0;
  wire preset__268_carry__8_n_0;
  wire preset__268_carry__8_n_1;
  wire preset__268_carry__8_n_2;
  wire preset__268_carry__8_n_3;
  wire preset__268_carry__8_n_4;
  wire preset__268_carry__8_n_5;
  wire preset__268_carry__8_n_6;
  wire preset__268_carry__8_n_7;
  wire preset__268_carry__9_i_1_n_0;
  wire preset__268_carry__9_i_2_n_0;
  wire preset__268_carry__9_i_3_n_0;
  wire preset__268_carry__9_i_4_n_0;
  wire preset__268_carry__9_n_3;
  wire preset__268_carry__9_n_6;
  wire preset__268_carry__9_n_7;
  wire preset__268_carry_i_1_n_0;
  wire preset__268_carry_i_2_n_0;
  wire preset__268_carry_i_3_n_0;
  wire preset__268_carry_i_4_n_0;
  wire preset__268_carry_i_5_n_0;
  wire preset__268_carry_i_6_n_0;
  wire preset__268_carry_i_7_n_0;
  wire preset__268_carry_i_8_n_0;
  wire preset__268_carry_n_0;
  wire preset__268_carry_n_1;
  wire preset__268_carry_n_2;
  wire preset__268_carry_n_3;
  wire preset__371_carry__0_i_1_n_0;
  wire preset__371_carry__0_i_2_n_0;
  wire preset__371_carry__0_i_3_n_0;
  wire preset__371_carry__0_i_4_n_0;
  wire preset__371_carry__0_n_0;
  wire preset__371_carry__0_n_1;
  wire preset__371_carry__0_n_2;
  wire preset__371_carry__0_n_3;
  wire preset__371_carry__0_n_4;
  wire preset__371_carry__0_n_5;
  wire preset__371_carry__0_n_6;
  wire preset__371_carry__0_n_7;
  wire preset__371_carry__1_i_1_n_0;
  wire preset__371_carry__1_i_2_n_0;
  wire preset__371_carry__1_i_3_n_0;
  wire preset__371_carry__1_i_4_n_0;
  wire preset__371_carry__1_n_0;
  wire preset__371_carry__1_n_1;
  wire preset__371_carry__1_n_2;
  wire preset__371_carry__1_n_3;
  wire preset__371_carry__1_n_4;
  wire preset__371_carry__1_n_5;
  wire preset__371_carry__1_n_6;
  wire preset__371_carry__1_n_7;
  wire preset__371_carry__2_i_1_n_0;
  wire preset__371_carry__2_i_2_n_0;
  wire preset__371_carry__2_i_3_n_0;
  wire preset__371_carry__2_i_4_n_0;
  wire preset__371_carry__2_n_0;
  wire preset__371_carry__2_n_1;
  wire preset__371_carry__2_n_2;
  wire preset__371_carry__2_n_3;
  wire preset__371_carry__2_n_4;
  wire preset__371_carry__2_n_5;
  wire preset__371_carry__2_n_6;
  wire preset__371_carry__2_n_7;
  wire preset__371_carry__3_n_0;
  wire preset__371_carry__3_n_1;
  wire preset__371_carry__3_n_2;
  wire preset__371_carry__3_n_3;
  wire preset__371_carry__3_n_4;
  wire preset__371_carry__3_n_5;
  wire preset__371_carry__3_n_6;
  wire preset__371_carry__3_n_7;
  wire preset__371_carry__4_n_3;
  wire preset__371_carry__4_n_6;
  wire preset__371_carry__4_n_7;
  wire preset__371_carry_i_1_n_0;
  wire preset__371_carry_i_2_n_0;
  wire preset__371_carry_i_3_n_0;
  wire preset__371_carry_n_0;
  wire preset__371_carry_n_1;
  wire preset__371_carry_n_2;
  wire preset__371_carry_n_3;
  wire preset__371_carry_n_4;
  wire preset__371_carry_n_5;
  wire preset__371_carry_n_6;
  wire preset__371_carry_n_7;
  wire preset__430_carry__0_i_1_n_0;
  wire preset__430_carry__0_i_2_n_0;
  wire preset__430_carry__0_i_3_n_0;
  wire preset__430_carry__0_i_4_n_0;
  wire preset__430_carry__0_i_5_n_0;
  wire preset__430_carry__0_i_6_n_0;
  wire preset__430_carry__0_i_7_n_0;
  wire preset__430_carry__0_i_8_n_0;
  wire preset__430_carry__0_n_0;
  wire preset__430_carry__0_n_1;
  wire preset__430_carry__0_n_2;
  wire preset__430_carry__0_n_3;
  wire preset__430_carry__1_i_1_n_0;
  wire preset__430_carry__1_i_2_n_0;
  wire preset__430_carry__1_i_3_n_0;
  wire preset__430_carry__1_i_4_n_0;
  wire preset__430_carry__1_i_5_n_0;
  wire preset__430_carry__1_i_6_n_0;
  wire preset__430_carry__1_i_7_n_0;
  wire preset__430_carry__1_i_8_n_0;
  wire preset__430_carry__1_n_0;
  wire preset__430_carry__1_n_1;
  wire preset__430_carry__1_n_2;
  wire preset__430_carry__1_n_3;
  wire preset__430_carry__2_i_1_n_0;
  wire preset__430_carry__2_i_2_n_0;
  wire preset__430_carry__2_i_3_n_0;
  wire preset__430_carry__2_i_4_n_0;
  wire preset__430_carry__2_i_5_n_0;
  wire preset__430_carry__2_i_6_n_0;
  wire preset__430_carry__2_i_7_n_0;
  wire preset__430_carry__2_i_8_n_0;
  wire preset__430_carry__2_n_0;
  wire preset__430_carry__2_n_1;
  wire preset__430_carry__2_n_2;
  wire preset__430_carry__2_n_3;
  wire preset__430_carry__3_i_1_n_0;
  wire preset__430_carry__3_i_2_n_0;
  wire preset__430_carry__3_i_3_n_0;
  wire preset__430_carry__3_i_4_n_0;
  wire preset__430_carry__3_i_5_n_0;
  wire preset__430_carry__3_i_6_n_0;
  wire preset__430_carry__3_i_7_n_0;
  wire preset__430_carry__3_i_8_n_0;
  wire preset__430_carry__3_n_0;
  wire preset__430_carry__3_n_1;
  wire preset__430_carry__3_n_2;
  wire preset__430_carry__3_n_3;
  wire preset__430_carry__4_i_1_n_0;
  wire preset__430_carry__4_i_2_n_0;
  wire preset__430_carry__4_i_3_n_0;
  wire preset__430_carry__4_i_4_n_0;
  wire preset__430_carry__4_i_5_n_0;
  wire preset__430_carry__4_i_6_n_0;
  wire preset__430_carry__4_i_7_n_0;
  wire preset__430_carry__4_i_8_n_0;
  wire preset__430_carry__4_n_0;
  wire preset__430_carry__4_n_1;
  wire preset__430_carry__4_n_2;
  wire preset__430_carry__4_n_3;
  wire preset__430_carry__5_i_1_n_0;
  wire preset__430_carry__5_i_2_n_0;
  wire preset__430_carry__5_i_3_n_0;
  wire preset__430_carry__5_i_4_n_0;
  wire preset__430_carry__5_i_5_n_0;
  wire preset__430_carry__5_i_6_n_0;
  wire preset__430_carry__5_n_1;
  wire preset__430_carry__5_n_2;
  wire preset__430_carry__5_n_3;
  wire preset__430_carry_i_1_n_0;
  wire preset__430_carry_i_2_n_0;
  wire preset__430_carry_i_3_n_0;
  wire preset__430_carry_i_4_n_0;
  wire preset__430_carry_i_5_n_0;
  wire preset__430_carry_i_6_n_0;
  wire preset__430_carry_i_7_n_0;
  wire preset__430_carry_n_0;
  wire preset__430_carry_n_1;
  wire preset__430_carry_n_2;
  wire preset__430_carry_n_3;
  wire preset__483_carry__0_n_0;
  wire preset__483_carry__0_n_1;
  wire preset__483_carry__0_n_2;
  wire preset__483_carry__0_n_3;
  wire preset__483_carry__0_n_4;
  wire preset__483_carry__0_n_5;
  wire preset__483_carry__0_n_6;
  wire preset__483_carry__0_n_7;
  wire preset__483_carry__1_n_0;
  wire preset__483_carry__1_n_1;
  wire preset__483_carry__1_n_2;
  wire preset__483_carry__1_n_3;
  wire preset__483_carry__1_n_4;
  wire preset__483_carry__1_n_5;
  wire preset__483_carry__1_n_6;
  wire preset__483_carry__1_n_7;
  wire preset__483_carry__2_n_0;
  wire preset__483_carry__2_n_1;
  wire preset__483_carry__2_n_2;
  wire preset__483_carry__2_n_3;
  wire preset__483_carry__2_n_4;
  wire preset__483_carry__2_n_5;
  wire preset__483_carry__2_n_6;
  wire preset__483_carry__2_n_7;
  wire preset__483_carry__3_n_0;
  wire preset__483_carry__3_n_1;
  wire preset__483_carry__3_n_2;
  wire preset__483_carry__3_n_3;
  wire preset__483_carry__3_n_4;
  wire preset__483_carry__3_n_5;
  wire preset__483_carry__3_n_6;
  wire preset__483_carry__3_n_7;
  wire preset__483_carry__4_n_7;
  wire preset__483_carry_i_1_n_0;
  wire preset__483_carry_n_0;
  wire preset__483_carry_n_1;
  wire preset__483_carry_n_2;
  wire preset__483_carry_n_3;
  wire preset__483_carry_n_4;
  wire preset__483_carry_n_5;
  wire preset__483_carry_n_6;
  wire preset__483_carry_n_7;
  wire preset__83_carry__0_i_1_n_0;
  wire preset__83_carry__0_i_2_n_0;
  wire preset__83_carry__0_i_3_n_0;
  wire preset__83_carry__0_i_4_n_0;
  wire preset__83_carry__0_n_0;
  wire preset__83_carry__0_n_1;
  wire preset__83_carry__0_n_2;
  wire preset__83_carry__0_n_3;
  wire preset__83_carry__0_n_4;
  wire preset__83_carry__0_n_5;
  wire preset__83_carry__0_n_6;
  wire preset__83_carry__0_n_7;
  wire preset__83_carry__1_i_1_n_0;
  wire preset__83_carry__1_i_2_n_0;
  wire preset__83_carry__1_i_3_n_0;
  wire preset__83_carry__1_i_4_n_0;
  wire preset__83_carry__1_n_0;
  wire preset__83_carry__1_n_1;
  wire preset__83_carry__1_n_2;
  wire preset__83_carry__1_n_3;
  wire preset__83_carry__1_n_4;
  wire preset__83_carry__1_n_5;
  wire preset__83_carry__1_n_6;
  wire preset__83_carry__1_n_7;
  wire preset__83_carry__2_i_1_n_0;
  wire preset__83_carry__2_i_2_n_0;
  wire preset__83_carry__2_i_3_n_0;
  wire preset__83_carry__2_i_4_n_0;
  wire preset__83_carry__2_n_0;
  wire preset__83_carry__2_n_1;
  wire preset__83_carry__2_n_2;
  wire preset__83_carry__2_n_3;
  wire preset__83_carry__2_n_4;
  wire preset__83_carry__2_n_5;
  wire preset__83_carry__2_n_6;
  wire preset__83_carry__2_n_7;
  wire preset__83_carry__3_i_1_n_0;
  wire preset__83_carry__3_i_2_n_0;
  wire preset__83_carry__3_i_3_n_0;
  wire preset__83_carry__3_i_4_n_0;
  wire preset__83_carry__3_n_0;
  wire preset__83_carry__3_n_1;
  wire preset__83_carry__3_n_2;
  wire preset__83_carry__3_n_3;
  wire preset__83_carry__3_n_4;
  wire preset__83_carry__3_n_5;
  wire preset__83_carry__3_n_6;
  wire preset__83_carry__3_n_7;
  wire preset__83_carry__4_i_1_n_0;
  wire preset__83_carry__4_i_2_n_0;
  wire preset__83_carry__4_i_3_n_0;
  wire preset__83_carry__4_i_4_n_0;
  wire preset__83_carry__4_n_0;
  wire preset__83_carry__4_n_1;
  wire preset__83_carry__4_n_2;
  wire preset__83_carry__4_n_3;
  wire preset__83_carry__4_n_4;
  wire preset__83_carry__4_n_5;
  wire preset__83_carry__4_n_6;
  wire preset__83_carry__4_n_7;
  wire preset__83_carry__5_i_1_n_0;
  wire preset__83_carry__5_i_2_n_0;
  wire preset__83_carry__5_i_3_n_0;
  wire preset__83_carry__5_i_4_n_0;
  wire preset__83_carry__5_n_0;
  wire preset__83_carry__5_n_1;
  wire preset__83_carry__5_n_2;
  wire preset__83_carry__5_n_3;
  wire preset__83_carry__5_n_4;
  wire preset__83_carry__5_n_5;
  wire preset__83_carry__5_n_6;
  wire preset__83_carry__5_n_7;
  wire preset__83_carry__6_i_1_n_0;
  wire preset__83_carry__6_i_2_n_0;
  wire preset__83_carry__6_i_3_n_0;
  wire preset__83_carry__6_n_0;
  wire preset__83_carry__6_n_2;
  wire preset__83_carry__6_n_3;
  wire preset__83_carry__6_n_5;
  wire preset__83_carry__6_n_6;
  wire preset__83_carry__6_n_7;
  wire preset__83_carry_i_1_n_0;
  wire preset__83_carry_i_2_n_0;
  wire preset__83_carry_i_3_n_0;
  wire preset__83_carry_n_0;
  wire preset__83_carry_n_1;
  wire preset__83_carry_n_2;
  wire preset__83_carry_n_3;
  wire preset__83_carry_n_4;
  wire preset__83_carry_n_5;
  wire preset__83_carry_n_6;
  wire preset_carry__0_i_1_n_0;
  wire preset_carry__0_i_2_n_0;
  wire preset_carry__0_i_3_n_0;
  wire preset_carry__0_i_4_n_0;
  wire preset_carry__0_n_0;
  wire preset_carry__0_n_1;
  wire preset_carry__0_n_2;
  wire preset_carry__0_n_3;
  wire preset_carry__0_n_4;
  wire preset_carry__0_n_5;
  wire preset_carry__0_n_6;
  wire preset_carry__0_n_7;
  wire preset_carry__1_i_1_n_0;
  wire preset_carry__1_i_2_n_0;
  wire preset_carry__1_i_3_n_0;
  wire preset_carry__1_i_4_n_0;
  wire preset_carry__1_n_0;
  wire preset_carry__1_n_1;
  wire preset_carry__1_n_2;
  wire preset_carry__1_n_3;
  wire preset_carry__1_n_4;
  wire preset_carry__1_n_5;
  wire preset_carry__1_n_6;
  wire preset_carry__1_n_7;
  wire preset_carry__2_i_1_n_0;
  wire preset_carry__2_i_2_n_0;
  wire preset_carry__2_i_3_n_0;
  wire preset_carry__2_i_4_n_0;
  wire preset_carry__2_n_0;
  wire preset_carry__2_n_1;
  wire preset_carry__2_n_2;
  wire preset_carry__2_n_3;
  wire preset_carry__2_n_4;
  wire preset_carry__2_n_5;
  wire preset_carry__2_n_6;
  wire preset_carry__2_n_7;
  wire preset_carry__3_i_1_n_0;
  wire preset_carry__3_i_2_n_0;
  wire preset_carry__3_i_3_n_0;
  wire preset_carry__3_i_4_n_0;
  wire preset_carry__3_n_0;
  wire preset_carry__3_n_1;
  wire preset_carry__3_n_2;
  wire preset_carry__3_n_3;
  wire preset_carry__3_n_4;
  wire preset_carry__3_n_5;
  wire preset_carry__3_n_6;
  wire preset_carry__3_n_7;
  wire preset_carry__4_i_1_n_0;
  wire preset_carry__4_i_2_n_0;
  wire preset_carry__4_i_3_n_0;
  wire preset_carry__4_i_4_n_0;
  wire preset_carry__4_n_0;
  wire preset_carry__4_n_1;
  wire preset_carry__4_n_2;
  wire preset_carry__4_n_3;
  wire preset_carry__4_n_4;
  wire preset_carry__4_n_5;
  wire preset_carry__4_n_6;
  wire preset_carry__4_n_7;
  wire preset_carry__5_i_1_n_0;
  wire preset_carry__5_i_2_n_0;
  wire preset_carry__5_i_3_n_0;
  wire preset_carry__5_i_4_n_0;
  wire preset_carry__5_n_0;
  wire preset_carry__5_n_1;
  wire preset_carry__5_n_2;
  wire preset_carry__5_n_3;
  wire preset_carry__5_n_4;
  wire preset_carry__5_n_5;
  wire preset_carry__5_n_6;
  wire preset_carry__5_n_7;
  wire preset_carry__6_i_1_n_0;
  wire preset_carry__6_i_2_n_0;
  wire preset_carry__6_i_3_n_0;
  wire preset_carry__6_n_0;
  wire preset_carry__6_n_2;
  wire preset_carry__6_n_3;
  wire preset_carry__6_n_5;
  wire preset_carry__6_n_6;
  wire preset_carry__6_n_7;
  wire preset_carry_i_1_n_0;
  wire preset_carry_i_2_n_0;
  wire preset_carry_i_3_n_0;
  wire preset_carry_n_0;
  wire preset_carry_n_1;
  wire preset_carry_n_2;
  wire preset_carry_n_3;
  wire preset_carry_n_4;
  wire preset_carry_n_7;
  wire pwm;
  wire pwmaux;
  wire pwmaux0_carry__0_i_13_n_0;
  wire pwmaux0_carry__0_i_14_n_0;
  wire pwmaux0_carry__0_i_15_n_0;
  wire pwmaux0_carry__0_i_16_n_0;
  wire pwmaux0_carry__0_i_1_n_0;
  wire pwmaux0_carry__0_i_2_n_0;
  wire pwmaux0_carry__0_i_3_n_0;
  wire pwmaux0_carry__0_i_4_n_0;
  wire pwmaux0_carry__0_i_5_n_0;
  wire pwmaux0_carry__0_i_6_n_0;
  wire pwmaux0_carry__0_i_7_n_0;
  wire pwmaux0_carry__0_i_8_n_0;
  wire pwmaux0_carry__0_n_0;
  wire pwmaux0_carry__0_n_1;
  wire pwmaux0_carry__0_n_2;
  wire pwmaux0_carry__0_n_3;
  wire pwmaux0_carry__1_i_10_n_0;
  wire pwmaux0_carry__1_i_11_n_0;
  wire pwmaux0_carry__1_i_1_n_0;
  wire pwmaux0_carry__1_i_2_n_0;
  wire pwmaux0_carry__1_i_3_n_0;
  wire pwmaux0_carry__1_i_4_n_0;
  wire pwmaux0_carry__1_i_5_n_0;
  wire pwmaux0_carry__1_i_6_n_0;
  wire pwmaux0_carry__1_i_7_n_0;
  wire pwmaux0_carry__1_n_1;
  wire pwmaux0_carry__1_n_2;
  wire pwmaux0_carry__1_n_3;
  wire pwmaux0_carry_i_10_n_0;
  wire pwmaux0_carry_i_14_n_0;
  wire pwmaux0_carry_i_15_n_0;
  wire pwmaux0_carry_i_16_n_0;
  wire pwmaux0_carry_i_17_n_0;
  wire pwmaux0_carry_i_1_n_0;
  wire pwmaux0_carry_i_2_n_0;
  wire pwmaux0_carry_i_3_n_0;
  wire pwmaux0_carry_i_4_n_0;
  wire pwmaux0_carry_i_5_n_0;
  wire pwmaux0_carry_i_6_n_0;
  wire pwmaux0_carry_i_7_n_0;
  wire pwmaux0_carry_i_8_n_0;
  wire pwmaux0_carry_n_0;
  wire pwmaux0_carry_n_1;
  wire pwmaux0_carry_n_2;
  wire pwmaux0_carry_n_3;
  wire pwmaux_i_1_n_0;
  wire pwmaux_i_4_n_0;
  wire pwmaux_i_5_n_0;
  wire pwmaux_i_6_n_0;
  wire pwmaux_i_7_n_0;
  wire pwmaux_i_8_n_0;
  wire reset;
  wire resetaux;
  wire [3:2]\NLW_count_reg[23]_i_2_CO_UNCONNECTED ;
  wire [3:3]\NLW_count_reg[23]_i_2_O_UNCONNECTED ;
  wire NLW_preset0_CARRYCASCOUT_UNCONNECTED;
  wire NLW_preset0_MULTSIGNOUT_UNCONNECTED;
  wire NLW_preset0_OVERFLOW_UNCONNECTED;
  wire NLW_preset0_PATTERNBDETECT_UNCONNECTED;
  wire NLW_preset0_PATTERNDETECT_UNCONNECTED;
  wire NLW_preset0_UNDERFLOW_UNCONNECTED;
  wire [29:0]NLW_preset0_ACOUT_UNCONNECTED;
  wire [17:0]NLW_preset0_BCOUT_UNCONNECTED;
  wire [3:0]NLW_preset0_CARRYOUT_UNCONNECTED;
  wire [47:28]NLW_preset0_P_UNCONNECTED;
  wire [47:0]NLW_preset0_PCOUT_UNCONNECTED;
  wire [0:0]NLW_preset__145_carry_O_UNCONNECTED;
  wire [2:2]NLW_preset__145_carry__6_CO_UNCONNECTED;
  wire [3:3]NLW_preset__145_carry__6_O_UNCONNECTED;
  wire [0:0]NLW_preset__207_carry_O_UNCONNECTED;
  wire [3:2]NLW_preset__207_carry__6_CO_UNCONNECTED;
  wire [3:3]NLW_preset__207_carry__6_O_UNCONNECTED;
  wire [3:0]NLW_preset__268_carry_O_UNCONNECTED;
  wire [3:0]NLW_preset__268_carry__0_O_UNCONNECTED;
  wire [3:0]NLW_preset__268_carry__1_O_UNCONNECTED;
  wire [3:0]NLW_preset__268_carry__2_O_UNCONNECTED;
  wire [3:0]NLW_preset__268_carry__3_O_UNCONNECTED;
  wire [0:0]NLW_preset__268_carry__4_O_UNCONNECTED;
  wire [3:1]NLW_preset__268_carry__9_CO_UNCONNECTED;
  wire [3:2]NLW_preset__268_carry__9_O_UNCONNECTED;
  wire [3:1]NLW_preset__371_carry__4_CO_UNCONNECTED;
  wire [3:2]NLW_preset__371_carry__4_O_UNCONNECTED;
  wire [3:0]NLW_preset__430_carry_O_UNCONNECTED;
  wire [3:0]NLW_preset__430_carry__0_O_UNCONNECTED;
  wire [3:0]NLW_preset__430_carry__1_O_UNCONNECTED;
  wire [3:0]NLW_preset__430_carry__2_O_UNCONNECTED;
  wire [3:0]NLW_preset__430_carry__3_O_UNCONNECTED;
  wire [3:0]NLW_preset__430_carry__4_O_UNCONNECTED;
  wire [3:3]NLW_preset__430_carry__5_CO_UNCONNECTED;
  wire [3:0]NLW_preset__430_carry__5_O_UNCONNECTED;
  wire [3:0]NLW_preset__483_carry__4_CO_UNCONNECTED;
  wire [3:1]NLW_preset__483_carry__4_O_UNCONNECTED;
  wire [0:0]NLW_preset__83_carry_O_UNCONNECTED;
  wire [2:2]NLW_preset__83_carry__6_CO_UNCONNECTED;
  wire [3:3]NLW_preset__83_carry__6_O_UNCONNECTED;
  wire [2:1]NLW_preset_carry_O_UNCONNECTED;
  wire [2:2]NLW_preset_carry__6_CO_UNCONNECTED;
  wire [3:3]NLW_preset_carry__6_O_UNCONNECTED;
  wire [3:0]NLW_pwmaux0_carry_O_UNCONNECTED;
  wire [3:0]NLW_pwmaux0_carry__0_O_UNCONNECTED;
  wire [3:0]NLW_pwmaux0_carry__1_O_UNCONNECTED;

  LUT2 #(
    .INIT(4'h2)) 
    \count[0]_i_1 
       (.I0(pwmaux),
        .I1(count[0]),
        .O(count_0[0]));
  (* SOFT_HLUTNM = "soft_lutpair36" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \count[10]_i_1 
       (.I0(data0[10]),
        .I1(pwmaux),
        .O(count_0[10]));
  (* SOFT_HLUTNM = "soft_lutpair35" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \count[11]_i_1 
       (.I0(data0[11]),
        .I1(pwmaux),
        .O(count_0[11]));
  (* SOFT_HLUTNM = "soft_lutpair35" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \count[12]_i_1 
       (.I0(data0[12]),
        .I1(pwmaux),
        .O(count_0[12]));
  (* SOFT_HLUTNM = "soft_lutpair34" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \count[13]_i_1 
       (.I0(data0[13]),
        .I1(pwmaux),
        .O(count_0[13]));
  (* SOFT_HLUTNM = "soft_lutpair33" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \count[14]_i_1 
       (.I0(data0[14]),
        .I1(pwmaux),
        .O(count_0[14]));
  (* SOFT_HLUTNM = "soft_lutpair32" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \count[15]_i_1 
       (.I0(data0[15]),
        .I1(pwmaux),
        .O(count_0[15]));
  (* SOFT_HLUTNM = "soft_lutpair31" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \count[16]_i_1 
       (.I0(data0[16]),
        .I1(pwmaux),
        .O(count_0[16]));
  (* SOFT_HLUTNM = "soft_lutpair30" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \count[17]_i_1 
       (.I0(data0[17]),
        .I1(pwmaux),
        .O(count_0[17]));
  (* SOFT_HLUTNM = "soft_lutpair34" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \count[18]_i_1 
       (.I0(data0[18]),
        .I1(pwmaux),
        .O(count_0[18]));
  (* SOFT_HLUTNM = "soft_lutpair33" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \count[19]_i_1 
       (.I0(data0[19]),
        .I1(pwmaux),
        .O(count_0[19]));
  (* SOFT_HLUTNM = "soft_lutpair40" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \count[1]_i_1 
       (.I0(data0[1]),
        .I1(pwmaux),
        .O(count_0[1]));
  (* SOFT_HLUTNM = "soft_lutpair32" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \count[20]_i_1 
       (.I0(data0[20]),
        .I1(pwmaux),
        .O(count_0[20]));
  (* SOFT_HLUTNM = "soft_lutpair31" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \count[21]_i_1 
       (.I0(data0[21]),
        .I1(pwmaux),
        .O(count_0[21]));
  (* SOFT_HLUTNM = "soft_lutpair30" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \count[22]_i_1 
       (.I0(data0[22]),
        .I1(pwmaux),
        .O(count_0[22]));
  (* SOFT_HLUTNM = "soft_lutpair29" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \count[23]_i_1 
       (.I0(data0[23]),
        .I1(pwmaux),
        .O(count_0[23]));
  (* SOFT_HLUTNM = "soft_lutpair40" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \count[2]_i_1 
       (.I0(data0[2]),
        .I1(pwmaux),
        .O(count_0[2]));
  (* SOFT_HLUTNM = "soft_lutpair39" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \count[3]_i_1 
       (.I0(data0[3]),
        .I1(pwmaux),
        .O(count_0[3]));
  (* SOFT_HLUTNM = "soft_lutpair39" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \count[4]_i_1 
       (.I0(data0[4]),
        .I1(pwmaux),
        .O(count_0[4]));
  (* SOFT_HLUTNM = "soft_lutpair38" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \count[5]_i_1 
       (.I0(data0[5]),
        .I1(pwmaux),
        .O(count_0[5]));
  (* SOFT_HLUTNM = "soft_lutpair38" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \count[6]_i_1 
       (.I0(data0[6]),
        .I1(pwmaux),
        .O(count_0[6]));
  (* SOFT_HLUTNM = "soft_lutpair37" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \count[7]_i_1 
       (.I0(data0[7]),
        .I1(pwmaux),
        .O(count_0[7]));
  (* SOFT_HLUTNM = "soft_lutpair37" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \count[8]_i_1 
       (.I0(data0[8]),
        .I1(pwmaux),
        .O(count_0[8]));
  (* SOFT_HLUTNM = "soft_lutpair36" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \count[9]_i_1 
       (.I0(data0[9]),
        .I1(pwmaux),
        .O(count_0[9]));
  FDCE #(
    .INIT(1'b0)) 
    \count_reg[0] 
       (.C(clk),
        .CE(1'b1),
        .CLR(resetaux),
        .D(count_0[0]),
        .Q(count[0]));
  FDCE #(
    .INIT(1'b0)) 
    \count_reg[10] 
       (.C(clk),
        .CE(1'b1),
        .CLR(resetaux),
        .D(count_0[10]),
        .Q(count[10]));
  FDCE #(
    .INIT(1'b0)) 
    \count_reg[11] 
       (.C(clk),
        .CE(1'b1),
        .CLR(resetaux),
        .D(count_0[11]),
        .Q(count[11]));
  FDCE #(
    .INIT(1'b0)) 
    \count_reg[12] 
       (.C(clk),
        .CE(1'b1),
        .CLR(resetaux),
        .D(count_0[12]),
        .Q(count[12]));
  CARRY4 \count_reg[12]_i_2 
       (.CI(\count_reg[8]_i_2_n_0 ),
        .CO({\count_reg[12]_i_2_n_0 ,\count_reg[12]_i_2_n_1 ,\count_reg[12]_i_2_n_2 ,\count_reg[12]_i_2_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(data0[12:9]),
        .S(count[12:9]));
  FDCE #(
    .INIT(1'b0)) 
    \count_reg[13] 
       (.C(clk),
        .CE(1'b1),
        .CLR(resetaux),
        .D(count_0[13]),
        .Q(count[13]));
  FDCE #(
    .INIT(1'b0)) 
    \count_reg[14] 
       (.C(clk),
        .CE(1'b1),
        .CLR(resetaux),
        .D(count_0[14]),
        .Q(count[14]));
  FDCE #(
    .INIT(1'b0)) 
    \count_reg[15] 
       (.C(clk),
        .CE(1'b1),
        .CLR(resetaux),
        .D(count_0[15]),
        .Q(count[15]));
  FDCE #(
    .INIT(1'b0)) 
    \count_reg[16] 
       (.C(clk),
        .CE(1'b1),
        .CLR(resetaux),
        .D(count_0[16]),
        .Q(count[16]));
  CARRY4 \count_reg[16]_i_2 
       (.CI(\count_reg[12]_i_2_n_0 ),
        .CO({\count_reg[16]_i_2_n_0 ,\count_reg[16]_i_2_n_1 ,\count_reg[16]_i_2_n_2 ,\count_reg[16]_i_2_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(data0[16:13]),
        .S(count[16:13]));
  FDCE #(
    .INIT(1'b0)) 
    \count_reg[17] 
       (.C(clk),
        .CE(1'b1),
        .CLR(resetaux),
        .D(count_0[17]),
        .Q(count[17]));
  FDCE #(
    .INIT(1'b0)) 
    \count_reg[18] 
       (.C(clk),
        .CE(1'b1),
        .CLR(resetaux),
        .D(count_0[18]),
        .Q(count[18]));
  FDCE #(
    .INIT(1'b0)) 
    \count_reg[19] 
       (.C(clk),
        .CE(1'b1),
        .CLR(resetaux),
        .D(count_0[19]),
        .Q(count[19]));
  FDCE #(
    .INIT(1'b0)) 
    \count_reg[1] 
       (.C(clk),
        .CE(1'b1),
        .CLR(resetaux),
        .D(count_0[1]),
        .Q(count[1]));
  FDCE #(
    .INIT(1'b0)) 
    \count_reg[20] 
       (.C(clk),
        .CE(1'b1),
        .CLR(resetaux),
        .D(count_0[20]),
        .Q(count[20]));
  CARRY4 \count_reg[20]_i_2 
       (.CI(\count_reg[16]_i_2_n_0 ),
        .CO({\count_reg[20]_i_2_n_0 ,\count_reg[20]_i_2_n_1 ,\count_reg[20]_i_2_n_2 ,\count_reg[20]_i_2_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(data0[20:17]),
        .S(count[20:17]));
  FDCE #(
    .INIT(1'b0)) 
    \count_reg[21] 
       (.C(clk),
        .CE(1'b1),
        .CLR(resetaux),
        .D(count_0[21]),
        .Q(count[21]));
  FDCE #(
    .INIT(1'b0)) 
    \count_reg[22] 
       (.C(clk),
        .CE(1'b1),
        .CLR(resetaux),
        .D(count_0[22]),
        .Q(count[22]));
  FDCE #(
    .INIT(1'b0)) 
    \count_reg[23] 
       (.C(clk),
        .CE(1'b1),
        .CLR(resetaux),
        .D(count_0[23]),
        .Q(count[23]));
  CARRY4 \count_reg[23]_i_2 
       (.CI(\count_reg[20]_i_2_n_0 ),
        .CO({\NLW_count_reg[23]_i_2_CO_UNCONNECTED [3:2],\count_reg[23]_i_2_n_2 ,\count_reg[23]_i_2_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\NLW_count_reg[23]_i_2_O_UNCONNECTED [3],data0[23:21]}),
        .S({1'b0,count[23:21]}));
  FDCE #(
    .INIT(1'b0)) 
    \count_reg[2] 
       (.C(clk),
        .CE(1'b1),
        .CLR(resetaux),
        .D(count_0[2]),
        .Q(count[2]));
  FDCE #(
    .INIT(1'b0)) 
    \count_reg[3] 
       (.C(clk),
        .CE(1'b1),
        .CLR(resetaux),
        .D(count_0[3]),
        .Q(count[3]));
  FDCE #(
    .INIT(1'b0)) 
    \count_reg[4] 
       (.C(clk),
        .CE(1'b1),
        .CLR(resetaux),
        .D(count_0[4]),
        .Q(count[4]));
  CARRY4 \count_reg[4]_i_2 
       (.CI(1'b0),
        .CO({\count_reg[4]_i_2_n_0 ,\count_reg[4]_i_2_n_1 ,\count_reg[4]_i_2_n_2 ,\count_reg[4]_i_2_n_3 }),
        .CYINIT(count[0]),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(data0[4:1]),
        .S(count[4:1]));
  FDCE #(
    .INIT(1'b0)) 
    \count_reg[5] 
       (.C(clk),
        .CE(1'b1),
        .CLR(resetaux),
        .D(count_0[5]),
        .Q(count[5]));
  FDCE #(
    .INIT(1'b0)) 
    \count_reg[6] 
       (.C(clk),
        .CE(1'b1),
        .CLR(resetaux),
        .D(count_0[6]),
        .Q(count[6]));
  FDCE #(
    .INIT(1'b0)) 
    \count_reg[7] 
       (.C(clk),
        .CE(1'b1),
        .CLR(resetaux),
        .D(count_0[7]),
        .Q(count[7]));
  FDCE #(
    .INIT(1'b0)) 
    \count_reg[8] 
       (.C(clk),
        .CE(1'b1),
        .CLR(resetaux),
        .D(count_0[8]),
        .Q(count[8]));
  CARRY4 \count_reg[8]_i_2 
       (.CI(\count_reg[4]_i_2_n_0 ),
        .CO({\count_reg[8]_i_2_n_0 ,\count_reg[8]_i_2_n_1 ,\count_reg[8]_i_2_n_2 ,\count_reg[8]_i_2_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(data0[8:5]),
        .S(count[8:5]));
  FDCE #(
    .INIT(1'b0)) 
    \count_reg[9] 
       (.C(clk),
        .CE(1'b1),
        .CLR(resetaux),
        .D(count_0[9]),
        .Q(count[9]));
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-13 {cell *THIS*}}" *) 
  DSP48E1 #(
    .ACASCREG(0),
    .ADREG(1),
    .ALUMODEREG(0),
    .AREG(0),
    .AUTORESET_PATDET("NO_RESET"),
    .A_INPUT("DIRECT"),
    .BCASCREG(0),
    .BREG(0),
    .B_INPUT("DIRECT"),
    .CARRYINREG(0),
    .CARRYINSELREG(0),
    .CREG(1),
    .DREG(1),
    .INMODEREG(0),
    .MASK(48'h3FFFFFFFFFFF),
    .MREG(0),
    .OPMODEREG(0),
    .PATTERN(48'h000000000000),
    .PREG(0),
    .SEL_MASK("MASK"),
    .SEL_PATTERN("PATTERN"),
    .USE_DPORT("FALSE"),
    .USE_MULT("MULTIPLY"),
    .USE_PATTERN_DETECT("NO_PATDET"),
    .USE_SIMD("ONE48")) 
    preset0
       (.A({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b1,1'b1,1'b1,1'b1,1'b0,1'b1,1'b0,1'b0,1'b0,1'b0,1'b1,1'b0,1'b0,1'b1,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .ACIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .ACOUT(NLW_preset0_ACOUT_UNCONNECTED[29:0]),
        .ALUMODE({1'b0,1'b0,1'b0,1'b0}),
        .B({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,dc}),
        .BCIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .BCOUT(NLW_preset0_BCOUT_UNCONNECTED[17:0]),
        .C({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .CARRYCASCIN(1'b0),
        .CARRYCASCOUT(NLW_preset0_CARRYCASCOUT_UNCONNECTED),
        .CARRYIN(1'b0),
        .CARRYINSEL({1'b0,1'b0,1'b0}),
        .CARRYOUT(NLW_preset0_CARRYOUT_UNCONNECTED[3:0]),
        .CEA1(1'b0),
        .CEA2(1'b0),
        .CEAD(1'b0),
        .CEALUMODE(1'b0),
        .CEB1(1'b0),
        .CEB2(1'b0),
        .CEC(1'b0),
        .CECARRYIN(1'b0),
        .CECTRL(1'b0),
        .CED(1'b0),
        .CEINMODE(1'b0),
        .CEM(1'b0),
        .CEP(1'b0),
        .CLK(1'b0),
        .D({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .INMODE({1'b0,1'b0,1'b0,1'b0,1'b0}),
        .MULTSIGNIN(1'b0),
        .MULTSIGNOUT(NLW_preset0_MULTSIGNOUT_UNCONNECTED),
        .OPMODE({1'b0,1'b0,1'b0,1'b0,1'b1,1'b0,1'b1}),
        .OVERFLOW(NLW_preset0_OVERFLOW_UNCONNECTED),
        .P({NLW_preset0_P_UNCONNECTED[47:28],preset0_n_78,preset0_n_79,preset0_n_80,preset0_n_81,preset0_n_82,preset0_n_83,preset0_n_84,preset0_n_85,preset0_n_86,preset0_n_87,preset0_n_88,preset0_n_89,preset0_n_90,preset0_n_91,preset0_n_92,preset0_n_93,preset0_n_94,preset0_n_95,preset0_n_96,preset0_n_97,preset0_n_98,preset0_n_99,preset0_n_100,preset0_n_101,preset0_n_102,preset0_n_103,preset0_n_104,preset0_n_105}),
        .PATTERNBDETECT(NLW_preset0_PATTERNBDETECT_UNCONNECTED),
        .PATTERNDETECT(NLW_preset0_PATTERNDETECT_UNCONNECTED),
        .PCIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .PCOUT(NLW_preset0_PCOUT_UNCONNECTED[47:0]),
        .RSTA(1'b0),
        .RSTALLCARRYIN(1'b0),
        .RSTALUMODE(1'b0),
        .RSTB(1'b0),
        .RSTC(1'b0),
        .RSTCTRL(1'b0),
        .RSTD(1'b0),
        .RSTINMODE(1'b0),
        .RSTM(1'b0),
        .RSTP(1'b0),
        .UNDERFLOW(NLW_preset0_UNDERFLOW_UNCONNECTED));
  CARRY4 preset__145_carry
       (.CI(1'b0),
        .CO({preset__145_carry_n_0,preset__145_carry_n_1,preset__145_carry_n_2,preset__145_carry_n_3}),
        .CYINIT(1'b0),
        .DI({preset0_n_105,1'b0,1'b0,1'b1}),
        .O({preset__145_carry_n_4,preset__145_carry_n_5,preset__145_carry_n_6,NLW_preset__145_carry_O_UNCONNECTED[0]}),
        .S({preset__145_carry_i_1_n_0,preset__145_carry_i_2_n_0,preset__145_carry_i_3_n_0,preset0_n_105}));
  CARRY4 preset__145_carry__0
       (.CI(preset__145_carry_n_0),
        .CO({preset__145_carry__0_n_0,preset__145_carry__0_n_1,preset__145_carry__0_n_2,preset__145_carry__0_n_3}),
        .CYINIT(1'b0),
        .DI({preset0_n_101,preset0_n_102,preset0_n_103,preset0_n_104}),
        .O({preset__145_carry__0_n_4,preset__145_carry__0_n_5,preset__145_carry__0_n_6,preset__145_carry__0_n_7}),
        .S({preset__145_carry__0_i_1_n_0,preset__145_carry__0_i_2_n_0,preset__145_carry__0_i_3_n_0,preset__145_carry__0_i_4_n_0}));
  LUT2 #(
    .INIT(4'h9)) 
    preset__145_carry__0_i_1
       (.I0(preset0_n_101),
        .I1(preset0_n_98),
        .O(preset__145_carry__0_i_1_n_0));
  LUT2 #(
    .INIT(4'h9)) 
    preset__145_carry__0_i_2
       (.I0(preset0_n_102),
        .I1(preset0_n_99),
        .O(preset__145_carry__0_i_2_n_0));
  LUT2 #(
    .INIT(4'h9)) 
    preset__145_carry__0_i_3
       (.I0(preset0_n_103),
        .I1(preset0_n_100),
        .O(preset__145_carry__0_i_3_n_0));
  LUT2 #(
    .INIT(4'h9)) 
    preset__145_carry__0_i_4
       (.I0(preset0_n_104),
        .I1(preset0_n_101),
        .O(preset__145_carry__0_i_4_n_0));
  CARRY4 preset__145_carry__1
       (.CI(preset__145_carry__0_n_0),
        .CO({preset__145_carry__1_n_0,preset__145_carry__1_n_1,preset__145_carry__1_n_2,preset__145_carry__1_n_3}),
        .CYINIT(1'b0),
        .DI({preset0_n_97,preset0_n_98,preset0_n_99,preset0_n_100}),
        .O({preset__145_carry__1_n_4,preset__145_carry__1_n_5,preset__145_carry__1_n_6,preset__145_carry__1_n_7}),
        .S({preset__145_carry__1_i_1_n_0,preset__145_carry__1_i_2_n_0,preset__145_carry__1_i_3_n_0,preset__145_carry__1_i_4_n_0}));
  LUT2 #(
    .INIT(4'h9)) 
    preset__145_carry__1_i_1
       (.I0(preset0_n_97),
        .I1(preset0_n_94),
        .O(preset__145_carry__1_i_1_n_0));
  LUT2 #(
    .INIT(4'h9)) 
    preset__145_carry__1_i_2
       (.I0(preset0_n_98),
        .I1(preset0_n_95),
        .O(preset__145_carry__1_i_2_n_0));
  LUT2 #(
    .INIT(4'h9)) 
    preset__145_carry__1_i_3
       (.I0(preset0_n_99),
        .I1(preset0_n_96),
        .O(preset__145_carry__1_i_3_n_0));
  LUT2 #(
    .INIT(4'h9)) 
    preset__145_carry__1_i_4
       (.I0(preset0_n_100),
        .I1(preset0_n_97),
        .O(preset__145_carry__1_i_4_n_0));
  CARRY4 preset__145_carry__2
       (.CI(preset__145_carry__1_n_0),
        .CO({preset__145_carry__2_n_0,preset__145_carry__2_n_1,preset__145_carry__2_n_2,preset__145_carry__2_n_3}),
        .CYINIT(1'b0),
        .DI({preset0_n_93,preset0_n_94,preset0_n_95,preset0_n_96}),
        .O({preset__145_carry__2_n_4,preset__145_carry__2_n_5,preset__145_carry__2_n_6,preset__145_carry__2_n_7}),
        .S({preset__145_carry__2_i_1_n_0,preset__145_carry__2_i_2_n_0,preset__145_carry__2_i_3_n_0,preset__145_carry__2_i_4_n_0}));
  LUT2 #(
    .INIT(4'h9)) 
    preset__145_carry__2_i_1
       (.I0(preset0_n_93),
        .I1(preset0_n_90),
        .O(preset__145_carry__2_i_1_n_0));
  LUT2 #(
    .INIT(4'h9)) 
    preset__145_carry__2_i_2
       (.I0(preset0_n_94),
        .I1(preset0_n_91),
        .O(preset__145_carry__2_i_2_n_0));
  LUT2 #(
    .INIT(4'h9)) 
    preset__145_carry__2_i_3
       (.I0(preset0_n_95),
        .I1(preset0_n_92),
        .O(preset__145_carry__2_i_3_n_0));
  LUT2 #(
    .INIT(4'h9)) 
    preset__145_carry__2_i_4
       (.I0(preset0_n_96),
        .I1(preset0_n_93),
        .O(preset__145_carry__2_i_4_n_0));
  CARRY4 preset__145_carry__3
       (.CI(preset__145_carry__2_n_0),
        .CO({preset__145_carry__3_n_0,preset__145_carry__3_n_1,preset__145_carry__3_n_2,preset__145_carry__3_n_3}),
        .CYINIT(1'b0),
        .DI({preset0_n_89,preset0_n_90,preset0_n_91,preset0_n_92}),
        .O({preset__145_carry__3_n_4,preset__145_carry__3_n_5,preset__145_carry__3_n_6,preset__145_carry__3_n_7}),
        .S({preset__145_carry__3_i_1_n_0,preset__145_carry__3_i_2_n_0,preset__145_carry__3_i_3_n_0,preset__145_carry__3_i_4_n_0}));
  LUT2 #(
    .INIT(4'h9)) 
    preset__145_carry__3_i_1
       (.I0(preset0_n_89),
        .I1(preset0_n_86),
        .O(preset__145_carry__3_i_1_n_0));
  LUT2 #(
    .INIT(4'h9)) 
    preset__145_carry__3_i_2
       (.I0(preset0_n_90),
        .I1(preset0_n_87),
        .O(preset__145_carry__3_i_2_n_0));
  LUT2 #(
    .INIT(4'h9)) 
    preset__145_carry__3_i_3
       (.I0(preset0_n_91),
        .I1(preset0_n_88),
        .O(preset__145_carry__3_i_3_n_0));
  LUT2 #(
    .INIT(4'h9)) 
    preset__145_carry__3_i_4
       (.I0(preset0_n_92),
        .I1(preset0_n_89),
        .O(preset__145_carry__3_i_4_n_0));
  CARRY4 preset__145_carry__4
       (.CI(preset__145_carry__3_n_0),
        .CO({preset__145_carry__4_n_0,preset__145_carry__4_n_1,preset__145_carry__4_n_2,preset__145_carry__4_n_3}),
        .CYINIT(1'b0),
        .DI({preset0_n_85,preset0_n_86,preset0_n_87,preset0_n_88}),
        .O({preset__145_carry__4_n_4,preset__145_carry__4_n_5,preset__145_carry__4_n_6,preset__145_carry__4_n_7}),
        .S({preset__145_carry__4_i_1_n_0,preset__145_carry__4_i_2_n_0,preset__145_carry__4_i_3_n_0,preset__145_carry__4_i_4_n_0}));
  LUT2 #(
    .INIT(4'h9)) 
    preset__145_carry__4_i_1
       (.I0(preset0_n_85),
        .I1(preset0_n_82),
        .O(preset__145_carry__4_i_1_n_0));
  LUT2 #(
    .INIT(4'h9)) 
    preset__145_carry__4_i_2
       (.I0(preset0_n_86),
        .I1(preset0_n_83),
        .O(preset__145_carry__4_i_2_n_0));
  LUT2 #(
    .INIT(4'h9)) 
    preset__145_carry__4_i_3
       (.I0(preset0_n_87),
        .I1(preset0_n_84),
        .O(preset__145_carry__4_i_3_n_0));
  LUT2 #(
    .INIT(4'h9)) 
    preset__145_carry__4_i_4
       (.I0(preset0_n_88),
        .I1(preset0_n_85),
        .O(preset__145_carry__4_i_4_n_0));
  CARRY4 preset__145_carry__5
       (.CI(preset__145_carry__4_n_0),
        .CO({preset__145_carry__5_n_0,preset__145_carry__5_n_1,preset__145_carry__5_n_2,preset__145_carry__5_n_3}),
        .CYINIT(1'b0),
        .DI({preset0_n_81,preset0_n_82,preset0_n_83,preset0_n_84}),
        .O({preset__145_carry__5_n_4,preset__145_carry__5_n_5,preset__145_carry__5_n_6,preset__145_carry__5_n_7}),
        .S({preset__145_carry__5_i_1_n_0,preset__145_carry__5_i_2_n_0,preset__145_carry__5_i_3_n_0,preset__145_carry__5_i_4_n_0}));
  LUT2 #(
    .INIT(4'h9)) 
    preset__145_carry__5_i_1
       (.I0(preset0_n_81),
        .I1(preset0_n_78),
        .O(preset__145_carry__5_i_1_n_0));
  LUT2 #(
    .INIT(4'h9)) 
    preset__145_carry__5_i_2
       (.I0(preset0_n_82),
        .I1(preset0_n_79),
        .O(preset__145_carry__5_i_2_n_0));
  LUT2 #(
    .INIT(4'h9)) 
    preset__145_carry__5_i_3
       (.I0(preset0_n_83),
        .I1(preset0_n_80),
        .O(preset__145_carry__5_i_3_n_0));
  LUT2 #(
    .INIT(4'h9)) 
    preset__145_carry__5_i_4
       (.I0(preset0_n_84),
        .I1(preset0_n_81),
        .O(preset__145_carry__5_i_4_n_0));
  CARRY4 preset__145_carry__6
       (.CI(preset__145_carry__5_n_0),
        .CO({preset__145_carry__6_n_0,NLW_preset__145_carry__6_CO_UNCONNECTED[2],preset__145_carry__6_n_2,preset__145_carry__6_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,preset0_n_78,preset0_n_79,preset0_n_80}),
        .O({NLW_preset__145_carry__6_O_UNCONNECTED[3],preset__145_carry__6_n_5,preset__145_carry__6_n_6,preset__145_carry__6_n_7}),
        .S({1'b1,preset__145_carry__6_i_1_n_0,preset__145_carry__6_i_2_n_0,preset__145_carry__6_i_3_n_0}));
  LUT1 #(
    .INIT(2'h1)) 
    preset__145_carry__6_i_1
       (.I0(preset0_n_78),
        .O(preset__145_carry__6_i_1_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    preset__145_carry__6_i_2
       (.I0(preset0_n_79),
        .O(preset__145_carry__6_i_2_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    preset__145_carry__6_i_3
       (.I0(preset0_n_80),
        .O(preset__145_carry__6_i_3_n_0));
  LUT2 #(
    .INIT(4'h9)) 
    preset__145_carry_i_1
       (.I0(preset0_n_105),
        .I1(preset0_n_102),
        .O(preset__145_carry_i_1_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    preset__145_carry_i_2
       (.I0(preset0_n_103),
        .O(preset__145_carry_i_2_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    preset__145_carry_i_3
       (.I0(preset0_n_104),
        .O(preset__145_carry_i_3_n_0));
  CARRY4 preset__207_carry
       (.CI(1'b0),
        .CO({preset__207_carry_n_0,preset__207_carry_n_1,preset__207_carry_n_2,preset__207_carry_n_3}),
        .CYINIT(1'b0),
        .DI({preset0_n_105,1'b0,1'b0,1'b1}),
        .O({preset__207_carry_n_4,preset__207_carry_n_5,preset__207_carry_n_6,NLW_preset__207_carry_O_UNCONNECTED[0]}),
        .S({preset__207_carry_i_1_n_0,preset__207_carry_i_2_n_0,preset__207_carry_i_3_n_0,preset0_n_105}));
  CARRY4 preset__207_carry__0
       (.CI(preset__207_carry_n_0),
        .CO({preset__207_carry__0_n_0,preset__207_carry__0_n_1,preset__207_carry__0_n_2,preset__207_carry__0_n_3}),
        .CYINIT(1'b0),
        .DI({preset0_n_101,preset0_n_102,preset0_n_103,preset0_n_104}),
        .O({preset__207_carry__0_n_4,preset__207_carry__0_n_5,preset__207_carry__0_n_6,preset__207_carry__0_n_7}),
        .S({preset__207_carry__0_i_1_n_0,preset__207_carry__0_i_2_n_0,preset__207_carry__0_i_3_n_0,preset__207_carry__0_i_4_n_0}));
  LUT2 #(
    .INIT(4'h9)) 
    preset__207_carry__0_i_1
       (.I0(preset0_n_101),
        .I1(preset0_n_98),
        .O(preset__207_carry__0_i_1_n_0));
  LUT2 #(
    .INIT(4'h9)) 
    preset__207_carry__0_i_2
       (.I0(preset0_n_102),
        .I1(preset0_n_99),
        .O(preset__207_carry__0_i_2_n_0));
  LUT2 #(
    .INIT(4'h9)) 
    preset__207_carry__0_i_3
       (.I0(preset0_n_103),
        .I1(preset0_n_100),
        .O(preset__207_carry__0_i_3_n_0));
  LUT2 #(
    .INIT(4'h9)) 
    preset__207_carry__0_i_4
       (.I0(preset0_n_104),
        .I1(preset0_n_101),
        .O(preset__207_carry__0_i_4_n_0));
  CARRY4 preset__207_carry__1
       (.CI(preset__207_carry__0_n_0),
        .CO({preset__207_carry__1_n_0,preset__207_carry__1_n_1,preset__207_carry__1_n_2,preset__207_carry__1_n_3}),
        .CYINIT(1'b0),
        .DI({preset0_n_97,preset0_n_98,preset0_n_99,preset0_n_100}),
        .O({preset__207_carry__1_n_4,preset__207_carry__1_n_5,preset__207_carry__1_n_6,preset__207_carry__1_n_7}),
        .S({preset__207_carry__1_i_1_n_0,preset__207_carry__1_i_2_n_0,preset__207_carry__1_i_3_n_0,preset__207_carry__1_i_4_n_0}));
  LUT2 #(
    .INIT(4'h9)) 
    preset__207_carry__1_i_1
       (.I0(preset0_n_97),
        .I1(preset0_n_94),
        .O(preset__207_carry__1_i_1_n_0));
  LUT2 #(
    .INIT(4'h9)) 
    preset__207_carry__1_i_2
       (.I0(preset0_n_98),
        .I1(preset0_n_95),
        .O(preset__207_carry__1_i_2_n_0));
  LUT2 #(
    .INIT(4'h9)) 
    preset__207_carry__1_i_3
       (.I0(preset0_n_99),
        .I1(preset0_n_96),
        .O(preset__207_carry__1_i_3_n_0));
  LUT2 #(
    .INIT(4'h9)) 
    preset__207_carry__1_i_4
       (.I0(preset0_n_100),
        .I1(preset0_n_97),
        .O(preset__207_carry__1_i_4_n_0));
  CARRY4 preset__207_carry__2
       (.CI(preset__207_carry__1_n_0),
        .CO({preset__207_carry__2_n_0,preset__207_carry__2_n_1,preset__207_carry__2_n_2,preset__207_carry__2_n_3}),
        .CYINIT(1'b0),
        .DI({preset0_n_93,preset0_n_94,preset0_n_95,preset0_n_96}),
        .O({preset__207_carry__2_n_4,preset__207_carry__2_n_5,preset__207_carry__2_n_6,preset__207_carry__2_n_7}),
        .S({preset__207_carry__2_i_1_n_0,preset__207_carry__2_i_2_n_0,preset__207_carry__2_i_3_n_0,preset__207_carry__2_i_4_n_0}));
  LUT2 #(
    .INIT(4'h9)) 
    preset__207_carry__2_i_1
       (.I0(preset0_n_93),
        .I1(preset0_n_90),
        .O(preset__207_carry__2_i_1_n_0));
  LUT2 #(
    .INIT(4'h9)) 
    preset__207_carry__2_i_2
       (.I0(preset0_n_94),
        .I1(preset0_n_91),
        .O(preset__207_carry__2_i_2_n_0));
  LUT2 #(
    .INIT(4'h9)) 
    preset__207_carry__2_i_3
       (.I0(preset0_n_95),
        .I1(preset0_n_92),
        .O(preset__207_carry__2_i_3_n_0));
  LUT2 #(
    .INIT(4'h9)) 
    preset__207_carry__2_i_4
       (.I0(preset0_n_96),
        .I1(preset0_n_93),
        .O(preset__207_carry__2_i_4_n_0));
  CARRY4 preset__207_carry__3
       (.CI(preset__207_carry__2_n_0),
        .CO({preset__207_carry__3_n_0,preset__207_carry__3_n_1,preset__207_carry__3_n_2,preset__207_carry__3_n_3}),
        .CYINIT(1'b0),
        .DI({preset0_n_89,preset0_n_90,preset0_n_91,preset0_n_92}),
        .O({preset__207_carry__3_n_4,preset__207_carry__3_n_5,preset__207_carry__3_n_6,preset__207_carry__3_n_7}),
        .S({preset__207_carry__3_i_1_n_0,preset__207_carry__3_i_2_n_0,preset__207_carry__3_i_3_n_0,preset__207_carry__3_i_4_n_0}));
  LUT2 #(
    .INIT(4'h9)) 
    preset__207_carry__3_i_1
       (.I0(preset0_n_89),
        .I1(preset0_n_86),
        .O(preset__207_carry__3_i_1_n_0));
  LUT2 #(
    .INIT(4'h9)) 
    preset__207_carry__3_i_2
       (.I0(preset0_n_90),
        .I1(preset0_n_87),
        .O(preset__207_carry__3_i_2_n_0));
  LUT2 #(
    .INIT(4'h9)) 
    preset__207_carry__3_i_3
       (.I0(preset0_n_91),
        .I1(preset0_n_88),
        .O(preset__207_carry__3_i_3_n_0));
  LUT2 #(
    .INIT(4'h9)) 
    preset__207_carry__3_i_4
       (.I0(preset0_n_92),
        .I1(preset0_n_89),
        .O(preset__207_carry__3_i_4_n_0));
  CARRY4 preset__207_carry__4
       (.CI(preset__207_carry__3_n_0),
        .CO({preset__207_carry__4_n_0,preset__207_carry__4_n_1,preset__207_carry__4_n_2,preset__207_carry__4_n_3}),
        .CYINIT(1'b0),
        .DI({preset0_n_85,preset0_n_86,preset0_n_87,preset0_n_88}),
        .O({preset__207_carry__4_n_4,preset__207_carry__4_n_5,preset__207_carry__4_n_6,preset__207_carry__4_n_7}),
        .S({preset__207_carry__4_i_1_n_0,preset__207_carry__4_i_2_n_0,preset__207_carry__4_i_3_n_0,preset__207_carry__4_i_4_n_0}));
  LUT2 #(
    .INIT(4'h9)) 
    preset__207_carry__4_i_1
       (.I0(preset0_n_85),
        .I1(preset0_n_82),
        .O(preset__207_carry__4_i_1_n_0));
  LUT2 #(
    .INIT(4'h9)) 
    preset__207_carry__4_i_2
       (.I0(preset0_n_86),
        .I1(preset0_n_83),
        .O(preset__207_carry__4_i_2_n_0));
  LUT2 #(
    .INIT(4'h9)) 
    preset__207_carry__4_i_3
       (.I0(preset0_n_87),
        .I1(preset0_n_84),
        .O(preset__207_carry__4_i_3_n_0));
  LUT2 #(
    .INIT(4'h9)) 
    preset__207_carry__4_i_4
       (.I0(preset0_n_88),
        .I1(preset0_n_85),
        .O(preset__207_carry__4_i_4_n_0));
  CARRY4 preset__207_carry__5
       (.CI(preset__207_carry__4_n_0),
        .CO({preset__207_carry__5_n_0,preset__207_carry__5_n_1,preset__207_carry__5_n_2,preset__207_carry__5_n_3}),
        .CYINIT(1'b0),
        .DI({preset0_n_81,preset0_n_82,preset0_n_83,preset0_n_84}),
        .O({preset__207_carry__5_n_4,preset__207_carry__5_n_5,preset__207_carry__5_n_6,preset__207_carry__5_n_7}),
        .S({preset__207_carry__5_i_1_n_0,preset__207_carry__5_i_2_n_0,preset__207_carry__5_i_3_n_0,preset__207_carry__5_i_4_n_0}));
  LUT2 #(
    .INIT(4'h9)) 
    preset__207_carry__5_i_1
       (.I0(preset0_n_81),
        .I1(preset0_n_78),
        .O(preset__207_carry__5_i_1_n_0));
  LUT2 #(
    .INIT(4'h9)) 
    preset__207_carry__5_i_2
       (.I0(preset0_n_82),
        .I1(preset0_n_79),
        .O(preset__207_carry__5_i_2_n_0));
  LUT2 #(
    .INIT(4'h9)) 
    preset__207_carry__5_i_3
       (.I0(preset0_n_83),
        .I1(preset0_n_80),
        .O(preset__207_carry__5_i_3_n_0));
  LUT2 #(
    .INIT(4'h9)) 
    preset__207_carry__5_i_4
       (.I0(preset0_n_84),
        .I1(preset0_n_81),
        .O(preset__207_carry__5_i_4_n_0));
  CARRY4 preset__207_carry__6
       (.CI(preset__207_carry__5_n_0),
        .CO({NLW_preset__207_carry__6_CO_UNCONNECTED[3:2],preset__207_carry__6_n_2,preset__207_carry__6_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,preset0_n_79,preset0_n_80}),
        .O({NLW_preset__207_carry__6_O_UNCONNECTED[3],preset__207_carry__6_n_5,preset__207_carry__6_n_6,preset__207_carry__6_n_7}),
        .S({1'b0,preset__207_carry__6_i_1_n_0,preset__207_carry__6_i_2_n_0,preset__207_carry__6_i_3_n_0}));
  LUT1 #(
    .INIT(2'h1)) 
    preset__207_carry__6_i_1
       (.I0(preset0_n_78),
        .O(preset__207_carry__6_i_1_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    preset__207_carry__6_i_2
       (.I0(preset0_n_79),
        .O(preset__207_carry__6_i_2_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    preset__207_carry__6_i_3
       (.I0(preset0_n_80),
        .O(preset__207_carry__6_i_3_n_0));
  LUT2 #(
    .INIT(4'h9)) 
    preset__207_carry_i_1
       (.I0(preset0_n_105),
        .I1(preset0_n_102),
        .O(preset__207_carry_i_1_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    preset__207_carry_i_2
       (.I0(preset0_n_103),
        .O(preset__207_carry_i_2_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    preset__207_carry_i_3
       (.I0(preset0_n_104),
        .O(preset__207_carry_i_3_n_0));
  CARRY4 preset__268_carry
       (.CI(1'b0),
        .CO({preset__268_carry_n_0,preset__268_carry_n_1,preset__268_carry_n_2,preset__268_carry_n_3}),
        .CYINIT(1'b0),
        .DI({preset__268_carry_i_1_n_0,preset__268_carry_i_2_n_0,preset__268_carry_i_3_n_0,preset__268_carry_i_4_n_0}),
        .O(NLW_preset__268_carry_O_UNCONNECTED[3:0]),
        .S({preset__268_carry_i_5_n_0,preset__268_carry_i_6_n_0,preset__268_carry_i_7_n_0,preset__268_carry_i_8_n_0}));
  CARRY4 preset__268_carry__0
       (.CI(preset__268_carry_n_0),
        .CO({preset__268_carry__0_n_0,preset__268_carry__0_n_1,preset__268_carry__0_n_2,preset__268_carry__0_n_3}),
        .CYINIT(1'b0),
        .DI({preset__268_carry__0_i_1_n_0,preset__268_carry__0_i_2_n_0,preset__268_carry__0_i_3_n_0,preset__268_carry__0_i_4_n_0}),
        .O(NLW_preset__268_carry__0_O_UNCONNECTED[3:0]),
        .S({preset__268_carry__0_i_5_n_0,preset__268_carry__0_i_6_n_0,preset__268_carry__0_i_7_n_0,preset__268_carry__0_i_8_n_0}));
  LUT2 #(
    .INIT(4'h8)) 
    preset__268_carry__0_i_1
       (.I0(preset_carry__1_n_5),
        .I1(preset__83_carry__0_n_4),
        .O(preset__268_carry__0_i_1_n_0));
  LUT2 #(
    .INIT(4'h8)) 
    preset__268_carry__0_i_2
       (.I0(preset_carry__1_n_6),
        .I1(preset__83_carry__0_n_5),
        .O(preset__268_carry__0_i_2_n_0));
  LUT2 #(
    .INIT(4'h8)) 
    preset__268_carry__0_i_3
       (.I0(preset_carry__1_n_7),
        .I1(preset__83_carry__0_n_6),
        .O(preset__268_carry__0_i_3_n_0));
  LUT2 #(
    .INIT(4'h8)) 
    preset__268_carry__0_i_4
       (.I0(preset_carry__0_n_4),
        .I1(preset__83_carry__0_n_7),
        .O(preset__268_carry__0_i_4_n_0));
  (* HLUTNM = "lutpair0" *) 
  LUT4 #(
    .INIT(16'h9666)) 
    preset__268_carry__0_i_5
       (.I0(preset_carry__1_n_4),
        .I1(preset__83_carry__1_n_7),
        .I2(preset__83_carry__0_n_4),
        .I3(preset_carry__1_n_5),
        .O(preset__268_carry__0_i_5_n_0));
  LUT4 #(
    .INIT(16'h8778)) 
    preset__268_carry__0_i_6
       (.I0(preset__83_carry__0_n_5),
        .I1(preset_carry__1_n_6),
        .I2(preset_carry__1_n_5),
        .I3(preset__83_carry__0_n_4),
        .O(preset__268_carry__0_i_6_n_0));
  LUT4 #(
    .INIT(16'h8778)) 
    preset__268_carry__0_i_7
       (.I0(preset__83_carry__0_n_6),
        .I1(preset_carry__1_n_7),
        .I2(preset_carry__1_n_6),
        .I3(preset__83_carry__0_n_5),
        .O(preset__268_carry__0_i_7_n_0));
  LUT4 #(
    .INIT(16'h8778)) 
    preset__268_carry__0_i_8
       (.I0(preset__83_carry__0_n_7),
        .I1(preset_carry__0_n_4),
        .I2(preset_carry__1_n_7),
        .I3(preset__83_carry__0_n_6),
        .O(preset__268_carry__0_i_8_n_0));
  CARRY4 preset__268_carry__1
       (.CI(preset__268_carry__0_n_0),
        .CO({preset__268_carry__1_n_0,preset__268_carry__1_n_1,preset__268_carry__1_n_2,preset__268_carry__1_n_3}),
        .CYINIT(1'b0),
        .DI({preset__268_carry__1_i_1_n_0,preset__268_carry__1_i_2_n_0,preset__268_carry__1_i_3_n_0,preset__268_carry__1_i_4_n_0}),
        .O(NLW_preset__268_carry__1_O_UNCONNECTED[3:0]),
        .S({preset__268_carry__1_i_5_n_0,preset__268_carry__1_i_6_n_0,preset__268_carry__1_i_7_n_0,preset__268_carry__1_i_8_n_0}));
  LUT5 #(
    .INIT(32'h80EAEA80)) 
    preset__268_carry__1_i_1
       (.I0(preset_carry__2_n_5),
        .I1(preset__83_carry__1_n_5),
        .I2(preset__145_carry_n_6),
        .I3(preset__83_carry__1_n_4),
        .I4(preset__145_carry_n_5),
        .O(preset__268_carry__1_i_1_n_0));
  (* SOFT_HLUTNM = "soft_lutpair28" *) 
  LUT3 #(
    .INIT(8'h96)) 
    preset__268_carry__1_i_10
       (.I0(preset__83_carry__2_n_7),
        .I1(preset_carry_n_7),
        .I2(preset__145_carry_n_4),
        .O(preset__268_carry__1_i_10_n_0));
  LUT5 #(
    .INIT(32'h80EAEA80)) 
    preset__268_carry__1_i_2
       (.I0(preset_carry__2_n_6),
        .I1(preset__83_carry__1_n_6),
        .I2(preset0_n_105),
        .I3(preset__83_carry__1_n_5),
        .I4(preset__145_carry_n_6),
        .O(preset__268_carry__1_i_2_n_0));
  LUT3 #(
    .INIT(8'h28)) 
    preset__268_carry__1_i_3
       (.I0(preset_carry__2_n_7),
        .I1(preset0_n_105),
        .I2(preset__83_carry__1_n_6),
        .O(preset__268_carry__1_i_3_n_0));
  (* HLUTNM = "lutpair0" *) 
  LUT2 #(
    .INIT(4'h8)) 
    preset__268_carry__1_i_4
       (.I0(preset_carry__1_n_4),
        .I1(preset__83_carry__1_n_7),
        .O(preset__268_carry__1_i_4_n_0));
  LUT6 #(
    .INIT(64'hB44BD22DD22D4BB4)) 
    preset__268_carry__1_i_5
       (.I0(preset__268_carry__1_i_9_n_0),
        .I1(preset_carry__2_n_5),
        .I2(preset__268_carry__1_i_10_n_0),
        .I3(preset_carry__2_n_4),
        .I4(preset__83_carry__1_n_4),
        .I5(preset__145_carry_n_5),
        .O(preset__268_carry__1_i_5_n_0));
  LUT6 #(
    .INIT(64'h9669699669966996)) 
    preset__268_carry__1_i_6
       (.I0(preset__268_carry__1_i_2_n_0),
        .I1(preset__145_carry_n_5),
        .I2(preset__83_carry__1_n_4),
        .I3(preset_carry__2_n_5),
        .I4(preset__83_carry__1_n_5),
        .I5(preset__145_carry_n_6),
        .O(preset__268_carry__1_i_6_n_0));
  LUT6 #(
    .INIT(64'h3CC369966996C33C)) 
    preset__268_carry__1_i_7
       (.I0(preset_carry__2_n_7),
        .I1(preset__145_carry_n_6),
        .I2(preset__83_carry__1_n_5),
        .I3(preset_carry__2_n_6),
        .I4(preset__83_carry__1_n_6),
        .I5(preset0_n_105),
        .O(preset__268_carry__1_i_7_n_0));
  LUT4 #(
    .INIT(16'h6996)) 
    preset__268_carry__1_i_8
       (.I0(preset__268_carry__1_i_4_n_0),
        .I1(preset_carry__2_n_7),
        .I2(preset__83_carry__1_n_6),
        .I3(preset0_n_105),
        .O(preset__268_carry__1_i_8_n_0));
  LUT2 #(
    .INIT(4'h7)) 
    preset__268_carry__1_i_9
       (.I0(preset__83_carry__1_n_5),
        .I1(preset__145_carry_n_6),
        .O(preset__268_carry__1_i_9_n_0));
  CARRY4 preset__268_carry__2
       (.CI(preset__268_carry__1_n_0),
        .CO({preset__268_carry__2_n_0,preset__268_carry__2_n_1,preset__268_carry__2_n_2,preset__268_carry__2_n_3}),
        .CYINIT(1'b0),
        .DI({preset__268_carry__2_i_1_n_0,preset__268_carry__2_i_2_n_0,preset__268_carry__2_i_3_n_0,preset__268_carry__2_i_4_n_0}),
        .O(NLW_preset__268_carry__2_O_UNCONNECTED[3:0]),
        .S({preset__268_carry__2_i_5_n_0,preset__268_carry__2_i_6_n_0,preset__268_carry__2_i_7_n_0,preset__268_carry__2_i_8_n_0}));
  LUT5 #(
    .INIT(32'hFFE8E800)) 
    preset__268_carry__2_i_1
       (.I0(preset__83_carry__2_n_5),
        .I1(preset__145_carry__0_n_6),
        .I2(preset__207_carry_n_5),
        .I3(preset__268_carry__2_i_9_n_0),
        .I4(preset_carry__3_n_5),
        .O(preset__268_carry__2_i_1_n_0));
  (* SOFT_HLUTNM = "soft_lutpair26" *) 
  LUT3 #(
    .INIT(8'h96)) 
    preset__268_carry__2_i_10
       (.I0(preset__83_carry__2_n_5),
        .I1(preset__207_carry_n_5),
        .I2(preset__145_carry__0_n_6),
        .O(preset__268_carry__2_i_10_n_0));
  (* SOFT_HLUTNM = "soft_lutpair27" *) 
  LUT3 #(
    .INIT(8'h96)) 
    preset__268_carry__2_i_11
       (.I0(preset__83_carry__2_n_6),
        .I1(preset__207_carry_n_6),
        .I2(preset__145_carry__0_n_7),
        .O(preset__268_carry__2_i_11_n_0));
  (* SOFT_HLUTNM = "soft_lutpair26" *) 
  LUT3 #(
    .INIT(8'hE8)) 
    preset__268_carry__2_i_12
       (.I0(preset__83_carry__2_n_5),
        .I1(preset__145_carry__0_n_6),
        .I2(preset__207_carry_n_5),
        .O(preset__268_carry__2_i_12_n_0));
  (* SOFT_HLUTNM = "soft_lutpair24" *) 
  LUT3 #(
    .INIT(8'h96)) 
    preset__268_carry__2_i_13
       (.I0(preset__83_carry__3_n_7),
        .I1(preset__207_carry__0_n_7),
        .I2(preset__145_carry__0_n_4),
        .O(preset__268_carry__2_i_13_n_0));
  (* SOFT_HLUTNM = "soft_lutpair25" *) 
  LUT3 #(
    .INIT(8'hE8)) 
    preset__268_carry__2_i_14
       (.I0(preset__83_carry__2_n_4),
        .I1(preset__145_carry__0_n_5),
        .I2(preset__207_carry_n_4),
        .O(preset__268_carry__2_i_14_n_0));
  (* SOFT_HLUTNM = "soft_lutpair27" *) 
  LUT3 #(
    .INIT(8'hE8)) 
    preset__268_carry__2_i_15
       (.I0(preset__83_carry__2_n_6),
        .I1(preset__145_carry__0_n_7),
        .I2(preset__207_carry_n_6),
        .O(preset__268_carry__2_i_15_n_0));
  (* SOFT_HLUTNM = "soft_lutpair28" *) 
  LUT3 #(
    .INIT(8'hE8)) 
    preset__268_carry__2_i_16
       (.I0(preset__83_carry__2_n_7),
        .I1(preset__145_carry_n_4),
        .I2(preset_carry_n_7),
        .O(preset__268_carry__2_i_16_n_0));
  LUT5 #(
    .INIT(32'hFFE8E800)) 
    preset__268_carry__2_i_2
       (.I0(preset__83_carry__2_n_6),
        .I1(preset__145_carry__0_n_7),
        .I2(preset__207_carry_n_6),
        .I3(preset__268_carry__2_i_10_n_0),
        .I4(preset_carry__3_n_6),
        .O(preset__268_carry__2_i_2_n_0));
  LUT5 #(
    .INIT(32'hFFE8E800)) 
    preset__268_carry__2_i_3
       (.I0(preset__83_carry__2_n_7),
        .I1(preset__145_carry_n_4),
        .I2(preset_carry_n_7),
        .I3(preset__268_carry__2_i_11_n_0),
        .I4(preset_carry__3_n_7),
        .O(preset__268_carry__2_i_3_n_0));
  LUT6 #(
    .INIT(64'hEA8080EA80EAEA80)) 
    preset__268_carry__2_i_4
       (.I0(preset_carry__2_n_4),
        .I1(preset__83_carry__1_n_4),
        .I2(preset__145_carry_n_5),
        .I3(preset__83_carry__2_n_7),
        .I4(preset_carry_n_7),
        .I5(preset__145_carry_n_4),
        .O(preset__268_carry__2_i_4_n_0));
  LUT6 #(
    .INIT(64'h17E8E817E81717E8)) 
    preset__268_carry__2_i_5
       (.I0(preset_carry__3_n_5),
        .I1(preset__268_carry__2_i_9_n_0),
        .I2(preset__268_carry__2_i_12_n_0),
        .I3(preset__268_carry__2_i_13_n_0),
        .I4(preset_carry__3_n_4),
        .I5(preset__268_carry__2_i_14_n_0),
        .O(preset__268_carry__2_i_5_n_0));
  LUT6 #(
    .INIT(64'h17E8E817E81717E8)) 
    preset__268_carry__2_i_6
       (.I0(preset_carry__3_n_6),
        .I1(preset__268_carry__2_i_10_n_0),
        .I2(preset__268_carry__2_i_15_n_0),
        .I3(preset__268_carry__2_i_9_n_0),
        .I4(preset_carry__3_n_5),
        .I5(preset__268_carry__2_i_12_n_0),
        .O(preset__268_carry__2_i_6_n_0));
  LUT6 #(
    .INIT(64'h17E8E817E81717E8)) 
    preset__268_carry__2_i_7
       (.I0(preset_carry__3_n_7),
        .I1(preset__268_carry__2_i_11_n_0),
        .I2(preset__268_carry__2_i_16_n_0),
        .I3(preset__268_carry__2_i_10_n_0),
        .I4(preset_carry__3_n_6),
        .I5(preset__268_carry__2_i_15_n_0),
        .O(preset__268_carry__2_i_7_n_0));
  LUT6 #(
    .INIT(64'h6969699669969696)) 
    preset__268_carry__2_i_8
       (.I0(preset__268_carry__2_i_4_n_0),
        .I1(preset__268_carry__2_i_11_n_0),
        .I2(preset_carry__3_n_7),
        .I3(preset__83_carry__2_n_7),
        .I4(preset__145_carry_n_4),
        .I5(preset_carry_n_7),
        .O(preset__268_carry__2_i_8_n_0));
  (* SOFT_HLUTNM = "soft_lutpair25" *) 
  LUT3 #(
    .INIT(8'h96)) 
    preset__268_carry__2_i_9
       (.I0(preset__83_carry__2_n_4),
        .I1(preset__207_carry_n_4),
        .I2(preset__145_carry__0_n_5),
        .O(preset__268_carry__2_i_9_n_0));
  CARRY4 preset__268_carry__3
       (.CI(preset__268_carry__2_n_0),
        .CO({preset__268_carry__3_n_0,preset__268_carry__3_n_1,preset__268_carry__3_n_2,preset__268_carry__3_n_3}),
        .CYINIT(1'b0),
        .DI({preset__268_carry__3_i_1_n_0,preset__268_carry__3_i_2_n_0,preset__268_carry__3_i_3_n_0,preset__268_carry__3_i_4_n_0}),
        .O(NLW_preset__268_carry__3_O_UNCONNECTED[3:0]),
        .S({preset__268_carry__3_i_5_n_0,preset__268_carry__3_i_6_n_0,preset__268_carry__3_i_7_n_0,preset__268_carry__3_i_8_n_0}));
  LUT5 #(
    .INIT(32'hFFE8E800)) 
    preset__268_carry__3_i_1
       (.I0(preset__83_carry__3_n_5),
        .I1(preset__145_carry__1_n_6),
        .I2(preset__207_carry__0_n_5),
        .I3(preset__268_carry__3_i_9_n_0),
        .I4(preset_carry__4_n_5),
        .O(preset__268_carry__3_i_1_n_0));
  (* SOFT_HLUTNM = "soft_lutpair22" *) 
  LUT3 #(
    .INIT(8'h96)) 
    preset__268_carry__3_i_10
       (.I0(preset__83_carry__3_n_5),
        .I1(preset__207_carry__0_n_5),
        .I2(preset__145_carry__1_n_6),
        .O(preset__268_carry__3_i_10_n_0));
  (* SOFT_HLUTNM = "soft_lutpair23" *) 
  LUT3 #(
    .INIT(8'h96)) 
    preset__268_carry__3_i_11
       (.I0(preset__83_carry__3_n_6),
        .I1(preset__207_carry__0_n_6),
        .I2(preset__145_carry__1_n_7),
        .O(preset__268_carry__3_i_11_n_0));
  (* SOFT_HLUTNM = "soft_lutpair22" *) 
  LUT3 #(
    .INIT(8'hE8)) 
    preset__268_carry__3_i_12
       (.I0(preset__83_carry__3_n_5),
        .I1(preset__145_carry__1_n_6),
        .I2(preset__207_carry__0_n_5),
        .O(preset__268_carry__3_i_12_n_0));
  (* SOFT_HLUTNM = "soft_lutpair20" *) 
  LUT3 #(
    .INIT(8'h96)) 
    preset__268_carry__3_i_13
       (.I0(preset__83_carry__4_n_7),
        .I1(preset__207_carry__1_n_7),
        .I2(preset__145_carry__1_n_4),
        .O(preset__268_carry__3_i_13_n_0));
  (* SOFT_HLUTNM = "soft_lutpair21" *) 
  LUT3 #(
    .INIT(8'hE8)) 
    preset__268_carry__3_i_14
       (.I0(preset__83_carry__3_n_4),
        .I1(preset__145_carry__1_n_5),
        .I2(preset__207_carry__0_n_4),
        .O(preset__268_carry__3_i_14_n_0));
  (* SOFT_HLUTNM = "soft_lutpair23" *) 
  LUT3 #(
    .INIT(8'hE8)) 
    preset__268_carry__3_i_15
       (.I0(preset__83_carry__3_n_6),
        .I1(preset__145_carry__1_n_7),
        .I2(preset__207_carry__0_n_6),
        .O(preset__268_carry__3_i_15_n_0));
  (* SOFT_HLUTNM = "soft_lutpair24" *) 
  LUT3 #(
    .INIT(8'hE8)) 
    preset__268_carry__3_i_16
       (.I0(preset__83_carry__3_n_7),
        .I1(preset__145_carry__0_n_4),
        .I2(preset__207_carry__0_n_7),
        .O(preset__268_carry__3_i_16_n_0));
  LUT5 #(
    .INIT(32'hFFE8E800)) 
    preset__268_carry__3_i_2
       (.I0(preset__83_carry__3_n_6),
        .I1(preset__145_carry__1_n_7),
        .I2(preset__207_carry__0_n_6),
        .I3(preset__268_carry__3_i_10_n_0),
        .I4(preset_carry__4_n_6),
        .O(preset__268_carry__3_i_2_n_0));
  LUT5 #(
    .INIT(32'hFFE8E800)) 
    preset__268_carry__3_i_3
       (.I0(preset__83_carry__3_n_7),
        .I1(preset__145_carry__0_n_4),
        .I2(preset__207_carry__0_n_7),
        .I3(preset__268_carry__3_i_11_n_0),
        .I4(preset_carry__4_n_7),
        .O(preset__268_carry__3_i_3_n_0));
  LUT5 #(
    .INIT(32'hFFE8E800)) 
    preset__268_carry__3_i_4
       (.I0(preset__83_carry__2_n_4),
        .I1(preset__145_carry__0_n_5),
        .I2(preset__207_carry_n_4),
        .I3(preset__268_carry__2_i_13_n_0),
        .I4(preset_carry__3_n_4),
        .O(preset__268_carry__3_i_4_n_0));
  LUT6 #(
    .INIT(64'h17E8E817E81717E8)) 
    preset__268_carry__3_i_5
       (.I0(preset_carry__4_n_5),
        .I1(preset__268_carry__3_i_9_n_0),
        .I2(preset__268_carry__3_i_12_n_0),
        .I3(preset__268_carry__3_i_13_n_0),
        .I4(preset_carry__4_n_4),
        .I5(preset__268_carry__3_i_14_n_0),
        .O(preset__268_carry__3_i_5_n_0));
  LUT6 #(
    .INIT(64'h17E8E817E81717E8)) 
    preset__268_carry__3_i_6
       (.I0(preset_carry__4_n_6),
        .I1(preset__268_carry__3_i_10_n_0),
        .I2(preset__268_carry__3_i_15_n_0),
        .I3(preset__268_carry__3_i_9_n_0),
        .I4(preset_carry__4_n_5),
        .I5(preset__268_carry__3_i_12_n_0),
        .O(preset__268_carry__3_i_6_n_0));
  LUT6 #(
    .INIT(64'h17E8E817E81717E8)) 
    preset__268_carry__3_i_7
       (.I0(preset_carry__4_n_7),
        .I1(preset__268_carry__3_i_11_n_0),
        .I2(preset__268_carry__3_i_16_n_0),
        .I3(preset__268_carry__3_i_10_n_0),
        .I4(preset_carry__4_n_6),
        .I5(preset__268_carry__3_i_15_n_0),
        .O(preset__268_carry__3_i_7_n_0));
  LUT6 #(
    .INIT(64'h17E8E817E81717E8)) 
    preset__268_carry__3_i_8
       (.I0(preset_carry__3_n_4),
        .I1(preset__268_carry__2_i_13_n_0),
        .I2(preset__268_carry__2_i_14_n_0),
        .I3(preset__268_carry__3_i_11_n_0),
        .I4(preset_carry__4_n_7),
        .I5(preset__268_carry__3_i_16_n_0),
        .O(preset__268_carry__3_i_8_n_0));
  (* SOFT_HLUTNM = "soft_lutpair21" *) 
  LUT3 #(
    .INIT(8'h96)) 
    preset__268_carry__3_i_9
       (.I0(preset__83_carry__3_n_4),
        .I1(preset__207_carry__0_n_4),
        .I2(preset__145_carry__1_n_5),
        .O(preset__268_carry__3_i_9_n_0));
  CARRY4 preset__268_carry__4
       (.CI(preset__268_carry__3_n_0),
        .CO({preset__268_carry__4_n_0,preset__268_carry__4_n_1,preset__268_carry__4_n_2,preset__268_carry__4_n_3}),
        .CYINIT(1'b0),
        .DI({preset__268_carry__4_i_1_n_0,preset__268_carry__4_i_2_n_0,preset__268_carry__4_i_3_n_0,preset__268_carry__4_i_4_n_0}),
        .O({preset__268_carry__4_n_4,preset__268_carry__4_n_5,preset__268_carry__4_n_6,NLW_preset__268_carry__4_O_UNCONNECTED[0]}),
        .S({preset__268_carry__4_i_5_n_0,preset__268_carry__4_i_6_n_0,preset__268_carry__4_i_7_n_0,preset__268_carry__4_i_8_n_0}));
  LUT5 #(
    .INIT(32'hFFE8E800)) 
    preset__268_carry__4_i_1
       (.I0(preset__83_carry__4_n_5),
        .I1(preset__145_carry__2_n_6),
        .I2(preset__207_carry__1_n_5),
        .I3(preset__268_carry__4_i_9_n_0),
        .I4(preset_carry__5_n_5),
        .O(preset__268_carry__4_i_1_n_0));
  (* SOFT_HLUTNM = "soft_lutpair18" *) 
  LUT3 #(
    .INIT(8'h96)) 
    preset__268_carry__4_i_10
       (.I0(preset__83_carry__4_n_5),
        .I1(preset__207_carry__1_n_5),
        .I2(preset__145_carry__2_n_6),
        .O(preset__268_carry__4_i_10_n_0));
  (* SOFT_HLUTNM = "soft_lutpair19" *) 
  LUT3 #(
    .INIT(8'h96)) 
    preset__268_carry__4_i_11
       (.I0(preset__83_carry__4_n_6),
        .I1(preset__207_carry__1_n_6),
        .I2(preset__145_carry__2_n_7),
        .O(preset__268_carry__4_i_11_n_0));
  (* SOFT_HLUTNM = "soft_lutpair18" *) 
  LUT3 #(
    .INIT(8'hE8)) 
    preset__268_carry__4_i_12
       (.I0(preset__83_carry__4_n_5),
        .I1(preset__145_carry__2_n_6),
        .I2(preset__207_carry__1_n_5),
        .O(preset__268_carry__4_i_12_n_0));
  (* SOFT_HLUTNM = "soft_lutpair1" *) 
  LUT3 #(
    .INIT(8'h96)) 
    preset__268_carry__4_i_13
       (.I0(preset__83_carry__5_n_7),
        .I1(preset__207_carry__2_n_7),
        .I2(preset__145_carry__2_n_4),
        .O(preset__268_carry__4_i_13_n_0));
  (* SOFT_HLUTNM = "soft_lutpair9" *) 
  LUT3 #(
    .INIT(8'hE8)) 
    preset__268_carry__4_i_14
       (.I0(preset__83_carry__4_n_4),
        .I1(preset__145_carry__2_n_5),
        .I2(preset__207_carry__1_n_4),
        .O(preset__268_carry__4_i_14_n_0));
  (* SOFT_HLUTNM = "soft_lutpair19" *) 
  LUT3 #(
    .INIT(8'hE8)) 
    preset__268_carry__4_i_15
       (.I0(preset__83_carry__4_n_6),
        .I1(preset__145_carry__2_n_7),
        .I2(preset__207_carry__1_n_6),
        .O(preset__268_carry__4_i_15_n_0));
  (* SOFT_HLUTNM = "soft_lutpair20" *) 
  LUT3 #(
    .INIT(8'hE8)) 
    preset__268_carry__4_i_16
       (.I0(preset__83_carry__4_n_7),
        .I1(preset__145_carry__1_n_4),
        .I2(preset__207_carry__1_n_7),
        .O(preset__268_carry__4_i_16_n_0));
  LUT5 #(
    .INIT(32'hFFE8E800)) 
    preset__268_carry__4_i_2
       (.I0(preset__83_carry__4_n_6),
        .I1(preset__145_carry__2_n_7),
        .I2(preset__207_carry__1_n_6),
        .I3(preset__268_carry__4_i_10_n_0),
        .I4(preset_carry__5_n_6),
        .O(preset__268_carry__4_i_2_n_0));
  LUT5 #(
    .INIT(32'hFFE8E800)) 
    preset__268_carry__4_i_3
       (.I0(preset__83_carry__4_n_7),
        .I1(preset__145_carry__1_n_4),
        .I2(preset__207_carry__1_n_7),
        .I3(preset__268_carry__4_i_11_n_0),
        .I4(preset_carry__5_n_7),
        .O(preset__268_carry__4_i_3_n_0));
  LUT5 #(
    .INIT(32'hFFE8E800)) 
    preset__268_carry__4_i_4
       (.I0(preset__83_carry__3_n_4),
        .I1(preset__145_carry__1_n_5),
        .I2(preset__207_carry__0_n_4),
        .I3(preset__268_carry__3_i_13_n_0),
        .I4(preset_carry__4_n_4),
        .O(preset__268_carry__4_i_4_n_0));
  LUT6 #(
    .INIT(64'h17E8E817E81717E8)) 
    preset__268_carry__4_i_5
       (.I0(preset_carry__5_n_5),
        .I1(preset__268_carry__4_i_9_n_0),
        .I2(preset__268_carry__4_i_12_n_0),
        .I3(preset__268_carry__4_i_13_n_0),
        .I4(preset_carry__5_n_4),
        .I5(preset__268_carry__4_i_14_n_0),
        .O(preset__268_carry__4_i_5_n_0));
  LUT6 #(
    .INIT(64'h17E8E817E81717E8)) 
    preset__268_carry__4_i_6
       (.I0(preset_carry__5_n_6),
        .I1(preset__268_carry__4_i_10_n_0),
        .I2(preset__268_carry__4_i_15_n_0),
        .I3(preset__268_carry__4_i_9_n_0),
        .I4(preset_carry__5_n_5),
        .I5(preset__268_carry__4_i_12_n_0),
        .O(preset__268_carry__4_i_6_n_0));
  LUT6 #(
    .INIT(64'h17E8E817E81717E8)) 
    preset__268_carry__4_i_7
       (.I0(preset_carry__5_n_7),
        .I1(preset__268_carry__4_i_11_n_0),
        .I2(preset__268_carry__4_i_16_n_0),
        .I3(preset__268_carry__4_i_10_n_0),
        .I4(preset_carry__5_n_6),
        .I5(preset__268_carry__4_i_15_n_0),
        .O(preset__268_carry__4_i_7_n_0));
  LUT6 #(
    .INIT(64'h17E8E817E81717E8)) 
    preset__268_carry__4_i_8
       (.I0(preset_carry__4_n_4),
        .I1(preset__268_carry__3_i_13_n_0),
        .I2(preset__268_carry__3_i_14_n_0),
        .I3(preset__268_carry__4_i_11_n_0),
        .I4(preset_carry__5_n_7),
        .I5(preset__268_carry__4_i_16_n_0),
        .O(preset__268_carry__4_i_8_n_0));
  (* SOFT_HLUTNM = "soft_lutpair9" *) 
  LUT3 #(
    .INIT(8'h96)) 
    preset__268_carry__4_i_9
       (.I0(preset__83_carry__4_n_4),
        .I1(preset__207_carry__1_n_4),
        .I2(preset__145_carry__2_n_5),
        .O(preset__268_carry__4_i_9_n_0));
  CARRY4 preset__268_carry__5
       (.CI(preset__268_carry__4_n_0),
        .CO({preset__268_carry__5_n_0,preset__268_carry__5_n_1,preset__268_carry__5_n_2,preset__268_carry__5_n_3}),
        .CYINIT(1'b0),
        .DI({preset__268_carry__5_i_1_n_0,preset__268_carry__5_i_2_n_0,preset__268_carry__5_i_3_n_0,preset__268_carry__5_i_4_n_0}),
        .O({preset__268_carry__5_n_4,preset__268_carry__5_n_5,preset__268_carry__5_n_6,preset__268_carry__5_n_7}),
        .S({preset__268_carry__5_i_5_n_0,preset__268_carry__5_i_6_n_0,preset__268_carry__5_i_7_n_0,preset__268_carry__5_i_8_n_0}));
  LUT5 #(
    .INIT(32'hFFE8E800)) 
    preset__268_carry__5_i_1
       (.I0(preset__83_carry__5_n_5),
        .I1(preset__145_carry__3_n_6),
        .I2(preset__207_carry__2_n_5),
        .I3(preset__268_carry__5_i_9_n_0),
        .I4(preset_carry__6_n_5),
        .O(preset__268_carry__5_i_1_n_0));
  (* SOFT_HLUTNM = "soft_lutpair3" *) 
  LUT3 #(
    .INIT(8'h96)) 
    preset__268_carry__5_i_10
       (.I0(preset__83_carry__5_n_5),
        .I1(preset__207_carry__2_n_5),
        .I2(preset__145_carry__3_n_6),
        .O(preset__268_carry__5_i_10_n_0));
  (* SOFT_HLUTNM = "soft_lutpair2" *) 
  LUT3 #(
    .INIT(8'h96)) 
    preset__268_carry__5_i_11
       (.I0(preset__83_carry__5_n_6),
        .I1(preset__207_carry__2_n_6),
        .I2(preset__145_carry__3_n_7),
        .O(preset__268_carry__5_i_11_n_0));
  (* SOFT_HLUTNM = "soft_lutpair3" *) 
  LUT3 #(
    .INIT(8'hE8)) 
    preset__268_carry__5_i_12
       (.I0(preset__83_carry__5_n_5),
        .I1(preset__145_carry__3_n_6),
        .I2(preset__207_carry__2_n_5),
        .O(preset__268_carry__5_i_12_n_0));
  (* SOFT_HLUTNM = "soft_lutpair5" *) 
  LUT3 #(
    .INIT(8'h96)) 
    preset__268_carry__5_i_13
       (.I0(preset__83_carry__6_n_7),
        .I1(preset__207_carry__3_n_7),
        .I2(preset__145_carry__3_n_4),
        .O(preset__268_carry__5_i_13_n_0));
  (* SOFT_HLUTNM = "soft_lutpair4" *) 
  LUT3 #(
    .INIT(8'hE8)) 
    preset__268_carry__5_i_14
       (.I0(preset__83_carry__5_n_4),
        .I1(preset__145_carry__3_n_5),
        .I2(preset__207_carry__2_n_4),
        .O(preset__268_carry__5_i_14_n_0));
  (* SOFT_HLUTNM = "soft_lutpair2" *) 
  LUT3 #(
    .INIT(8'hE8)) 
    preset__268_carry__5_i_15
       (.I0(preset__83_carry__5_n_6),
        .I1(preset__145_carry__3_n_7),
        .I2(preset__207_carry__2_n_6),
        .O(preset__268_carry__5_i_15_n_0));
  (* SOFT_HLUTNM = "soft_lutpair1" *) 
  LUT3 #(
    .INIT(8'hE8)) 
    preset__268_carry__5_i_16
       (.I0(preset__83_carry__5_n_7),
        .I1(preset__145_carry__2_n_4),
        .I2(preset__207_carry__2_n_7),
        .O(preset__268_carry__5_i_16_n_0));
  LUT5 #(
    .INIT(32'hFFE8E800)) 
    preset__268_carry__5_i_2
       (.I0(preset__83_carry__5_n_6),
        .I1(preset__145_carry__3_n_7),
        .I2(preset__207_carry__2_n_6),
        .I3(preset__268_carry__5_i_10_n_0),
        .I4(preset_carry__6_n_6),
        .O(preset__268_carry__5_i_2_n_0));
  LUT5 #(
    .INIT(32'hFFE8E800)) 
    preset__268_carry__5_i_3
       (.I0(preset__83_carry__5_n_7),
        .I1(preset__145_carry__2_n_4),
        .I2(preset__207_carry__2_n_7),
        .I3(preset__268_carry__5_i_11_n_0),
        .I4(preset_carry__6_n_7),
        .O(preset__268_carry__5_i_3_n_0));
  LUT5 #(
    .INIT(32'hFFE8E800)) 
    preset__268_carry__5_i_4
       (.I0(preset__83_carry__4_n_4),
        .I1(preset__145_carry__2_n_5),
        .I2(preset__207_carry__1_n_4),
        .I3(preset__268_carry__4_i_13_n_0),
        .I4(preset_carry__5_n_4),
        .O(preset__268_carry__5_i_4_n_0));
  LUT6 #(
    .INIT(64'hE81717E817E8E817)) 
    preset__268_carry__5_i_5
       (.I0(preset_carry__6_n_5),
        .I1(preset__268_carry__5_i_9_n_0),
        .I2(preset__268_carry__5_i_12_n_0),
        .I3(preset__268_carry__5_i_13_n_0),
        .I4(preset__268_carry__5_i_14_n_0),
        .I5(preset_carry__6_n_0),
        .O(preset__268_carry__5_i_5_n_0));
  LUT6 #(
    .INIT(64'h17E8E817E81717E8)) 
    preset__268_carry__5_i_6
       (.I0(preset_carry__6_n_6),
        .I1(preset__268_carry__5_i_10_n_0),
        .I2(preset__268_carry__5_i_15_n_0),
        .I3(preset__268_carry__5_i_9_n_0),
        .I4(preset_carry__6_n_5),
        .I5(preset__268_carry__5_i_12_n_0),
        .O(preset__268_carry__5_i_6_n_0));
  LUT6 #(
    .INIT(64'h17E8E817E81717E8)) 
    preset__268_carry__5_i_7
       (.I0(preset_carry__6_n_7),
        .I1(preset__268_carry__5_i_11_n_0),
        .I2(preset__268_carry__5_i_16_n_0),
        .I3(preset__268_carry__5_i_10_n_0),
        .I4(preset_carry__6_n_6),
        .I5(preset__268_carry__5_i_15_n_0),
        .O(preset__268_carry__5_i_7_n_0));
  LUT6 #(
    .INIT(64'h17E8E817E81717E8)) 
    preset__268_carry__5_i_8
       (.I0(preset_carry__5_n_4),
        .I1(preset__268_carry__4_i_13_n_0),
        .I2(preset__268_carry__4_i_14_n_0),
        .I3(preset__268_carry__5_i_11_n_0),
        .I4(preset_carry__6_n_7),
        .I5(preset__268_carry__5_i_16_n_0),
        .O(preset__268_carry__5_i_8_n_0));
  (* SOFT_HLUTNM = "soft_lutpair4" *) 
  LUT3 #(
    .INIT(8'h96)) 
    preset__268_carry__5_i_9
       (.I0(preset__83_carry__5_n_4),
        .I1(preset__207_carry__2_n_4),
        .I2(preset__145_carry__3_n_5),
        .O(preset__268_carry__5_i_9_n_0));
  CARRY4 preset__268_carry__6
       (.CI(preset__268_carry__5_n_0),
        .CO({preset__268_carry__6_n_0,preset__268_carry__6_n_1,preset__268_carry__6_n_2,preset__268_carry__6_n_3}),
        .CYINIT(1'b0),
        .DI({preset__268_carry__6_i_1_n_0,preset__268_carry__6_i_2_n_0,preset__268_carry__6_i_3_n_0,preset__268_carry__6_i_4_n_0}),
        .O({preset__268_carry__6_n_4,preset__268_carry__6_n_5,preset__268_carry__6_n_6,preset__268_carry__6_n_7}),
        .S({preset__268_carry__6_i_5_n_0,preset__268_carry__6_i_6_n_0,preset__268_carry__6_i_7_n_0,preset__268_carry__6_i_8_n_0}));
  LUT5 #(
    .INIT(32'hE800FFE8)) 
    preset__268_carry__6_i_1
       (.I0(preset__83_carry__6_n_5),
        .I1(preset__145_carry__4_n_6),
        .I2(preset__207_carry__3_n_5),
        .I3(preset__268_carry__6_i_9_n_0),
        .I4(preset_carry__6_n_0),
        .O(preset__268_carry__6_i_1_n_0));
  (* SOFT_HLUTNM = "soft_lutpair7" *) 
  LUT3 #(
    .INIT(8'h96)) 
    preset__268_carry__6_i_10
       (.I0(preset__83_carry__6_n_5),
        .I1(preset__207_carry__3_n_5),
        .I2(preset__145_carry__4_n_6),
        .O(preset__268_carry__6_i_10_n_0));
  (* SOFT_HLUTNM = "soft_lutpair6" *) 
  LUT3 #(
    .INIT(8'h96)) 
    preset__268_carry__6_i_11
       (.I0(preset__83_carry__6_n_6),
        .I1(preset__207_carry__3_n_6),
        .I2(preset__145_carry__4_n_7),
        .O(preset__268_carry__6_i_11_n_0));
  (* SOFT_HLUTNM = "soft_lutpair7" *) 
  LUT3 #(
    .INIT(8'hE8)) 
    preset__268_carry__6_i_12
       (.I0(preset__83_carry__6_n_5),
        .I1(preset__145_carry__4_n_6),
        .I2(preset__207_carry__3_n_5),
        .O(preset__268_carry__6_i_12_n_0));
  (* SOFT_HLUTNM = "soft_lutpair10" *) 
  LUT3 #(
    .INIT(8'h69)) 
    preset__268_carry__6_i_13
       (.I0(preset__207_carry__4_n_7),
        .I1(preset__83_carry__6_n_0),
        .I2(preset__145_carry__4_n_4),
        .O(preset__268_carry__6_i_13_n_0));
  (* SOFT_HLUTNM = "soft_lutpair6" *) 
  LUT3 #(
    .INIT(8'hE8)) 
    preset__268_carry__6_i_14
       (.I0(preset__83_carry__6_n_6),
        .I1(preset__145_carry__4_n_7),
        .I2(preset__207_carry__3_n_6),
        .O(preset__268_carry__6_i_14_n_0));
  (* SOFT_HLUTNM = "soft_lutpair5" *) 
  LUT3 #(
    .INIT(8'hE8)) 
    preset__268_carry__6_i_15
       (.I0(preset__83_carry__6_n_7),
        .I1(preset__145_carry__3_n_4),
        .I2(preset__207_carry__3_n_7),
        .O(preset__268_carry__6_i_15_n_0));
  LUT5 #(
    .INIT(32'hE800FFE8)) 
    preset__268_carry__6_i_2
       (.I0(preset__83_carry__6_n_6),
        .I1(preset__145_carry__4_n_7),
        .I2(preset__207_carry__3_n_6),
        .I3(preset__268_carry__6_i_10_n_0),
        .I4(preset_carry__6_n_0),
        .O(preset__268_carry__6_i_2_n_0));
  LUT5 #(
    .INIT(32'hE800FFE8)) 
    preset__268_carry__6_i_3
       (.I0(preset__83_carry__6_n_7),
        .I1(preset__145_carry__3_n_4),
        .I2(preset__207_carry__3_n_7),
        .I3(preset__268_carry__6_i_11_n_0),
        .I4(preset_carry__6_n_0),
        .O(preset__268_carry__6_i_3_n_0));
  LUT5 #(
    .INIT(32'hE800FFE8)) 
    preset__268_carry__6_i_4
       (.I0(preset__83_carry__5_n_4),
        .I1(preset__145_carry__3_n_5),
        .I2(preset__207_carry__2_n_4),
        .I3(preset__268_carry__5_i_13_n_0),
        .I4(preset_carry__6_n_0),
        .O(preset__268_carry__6_i_4_n_0));
  LUT6 #(
    .INIT(64'h366C93366CC9366C)) 
    preset__268_carry__6_i_5
       (.I0(preset__268_carry__6_i_12_n_0),
        .I1(preset__268_carry__6_i_13_n_0),
        .I2(preset__207_carry__3_n_4),
        .I3(preset__145_carry__4_n_5),
        .I4(preset__83_carry__6_n_0),
        .I5(preset_carry__6_n_0),
        .O(preset__268_carry__6_i_5_n_0));
  LUT6 #(
    .INIT(64'h9336366C366C6CC9)) 
    preset__268_carry__6_i_6
       (.I0(preset__268_carry__6_i_14_n_0),
        .I1(preset__268_carry__6_i_9_n_0),
        .I2(preset__207_carry__3_n_5),
        .I3(preset__145_carry__4_n_6),
        .I4(preset__83_carry__6_n_5),
        .I5(preset_carry__6_n_0),
        .O(preset__268_carry__6_i_6_n_0));
  LUT6 #(
    .INIT(64'h9336366C366C6CC9)) 
    preset__268_carry__6_i_7
       (.I0(preset__268_carry__6_i_15_n_0),
        .I1(preset__268_carry__6_i_10_n_0),
        .I2(preset__207_carry__3_n_6),
        .I3(preset__145_carry__4_n_7),
        .I4(preset__83_carry__6_n_6),
        .I5(preset_carry__6_n_0),
        .O(preset__268_carry__6_i_7_n_0));
  LUT6 #(
    .INIT(64'h9336366C366C6CC9)) 
    preset__268_carry__6_i_8
       (.I0(preset__268_carry__5_i_14_n_0),
        .I1(preset__268_carry__6_i_11_n_0),
        .I2(preset__207_carry__3_n_7),
        .I3(preset__145_carry__3_n_4),
        .I4(preset__83_carry__6_n_7),
        .I5(preset_carry__6_n_0),
        .O(preset__268_carry__6_i_8_n_0));
  (* SOFT_HLUTNM = "soft_lutpair8" *) 
  LUT3 #(
    .INIT(8'h69)) 
    preset__268_carry__6_i_9
       (.I0(preset__207_carry__3_n_4),
        .I1(preset__145_carry__4_n_5),
        .I2(preset__83_carry__6_n_0),
        .O(preset__268_carry__6_i_9_n_0));
  CARRY4 preset__268_carry__7
       (.CI(preset__268_carry__6_n_0),
        .CO({preset__268_carry__7_n_0,preset__268_carry__7_n_1,preset__268_carry__7_n_2,preset__268_carry__7_n_3}),
        .CYINIT(1'b0),
        .DI({preset__268_carry__7_i_1_n_0,preset__268_carry__7_i_2_n_0,preset__268_carry__7_i_3_n_0,preset__268_carry__7_i_4_n_0}),
        .O({preset__268_carry__7_n_4,preset__268_carry__7_n_5,preset__268_carry__7_n_6,preset__268_carry__7_n_7}),
        .S({preset__268_carry__7_i_5_n_0,preset__268_carry__7_i_6_n_0,preset__268_carry__7_i_7_n_0,preset__268_carry__7_i_8_n_0}));
  LUT6 #(
    .INIT(64'h32808032B3FEFEB3)) 
    preset__268_carry__7_i_1
       (.I0(preset__145_carry__5_n_6),
        .I1(preset__83_carry__6_n_0),
        .I2(preset__207_carry__4_n_5),
        .I3(preset__145_carry__5_n_5),
        .I4(preset__207_carry__4_n_4),
        .I5(preset_carry__6_n_0),
        .O(preset__268_carry__7_i_1_n_0));
  (* SOFT_HLUTNM = "soft_lutpair14" *) 
  LUT3 #(
    .INIT(8'h69)) 
    preset__268_carry__7_i_10
       (.I0(preset__207_carry__5_n_7),
        .I1(preset__83_carry__6_n_0),
        .I2(preset__145_carry__5_n_4),
        .O(preset__268_carry__7_i_10_n_0));
  (* SOFT_HLUTNM = "soft_lutpair11" *) 
  LUT3 #(
    .INIT(8'hB2)) 
    preset__268_carry__7_i_11
       (.I0(preset__145_carry__5_n_7),
        .I1(preset__83_carry__6_n_0),
        .I2(preset__207_carry__4_n_6),
        .O(preset__268_carry__7_i_11_n_0));
  (* SOFT_HLUTNM = "soft_lutpair13" *) 
  LUT3 #(
    .INIT(8'h69)) 
    preset__268_carry__7_i_12
       (.I0(preset__207_carry__4_n_4),
        .I1(preset__83_carry__6_n_0),
        .I2(preset__145_carry__5_n_5),
        .O(preset__268_carry__7_i_12_n_0));
  (* SOFT_HLUTNM = "soft_lutpair10" *) 
  LUT3 #(
    .INIT(8'hB2)) 
    preset__268_carry__7_i_13
       (.I0(preset__145_carry__4_n_4),
        .I1(preset__83_carry__6_n_0),
        .I2(preset__207_carry__4_n_7),
        .O(preset__268_carry__7_i_13_n_0));
  (* SOFT_HLUTNM = "soft_lutpair12" *) 
  LUT3 #(
    .INIT(8'h69)) 
    preset__268_carry__7_i_14
       (.I0(preset__207_carry__4_n_5),
        .I1(preset__83_carry__6_n_0),
        .I2(preset__145_carry__5_n_6),
        .O(preset__268_carry__7_i_14_n_0));
  (* SOFT_HLUTNM = "soft_lutpair8" *) 
  LUT3 #(
    .INIT(8'hD4)) 
    preset__268_carry__7_i_15
       (.I0(preset__83_carry__6_n_0),
        .I1(preset__145_carry__4_n_5),
        .I2(preset__207_carry__3_n_4),
        .O(preset__268_carry__7_i_15_n_0));
  (* SOFT_HLUTNM = "soft_lutpair11" *) 
  LUT3 #(
    .INIT(8'h69)) 
    preset__268_carry__7_i_16
       (.I0(preset__207_carry__4_n_6),
        .I1(preset__83_carry__6_n_0),
        .I2(preset__145_carry__5_n_7),
        .O(preset__268_carry__7_i_16_n_0));
  LUT6 #(
    .INIT(64'h32808032B3FEFEB3)) 
    preset__268_carry__7_i_2
       (.I0(preset__145_carry__5_n_7),
        .I1(preset__83_carry__6_n_0),
        .I2(preset__207_carry__4_n_6),
        .I3(preset__145_carry__5_n_6),
        .I4(preset__207_carry__4_n_5),
        .I5(preset_carry__6_n_0),
        .O(preset__268_carry__7_i_2_n_0));
  LUT6 #(
    .INIT(64'h32808032B3FEFEB3)) 
    preset__268_carry__7_i_3
       (.I0(preset__145_carry__4_n_4),
        .I1(preset__83_carry__6_n_0),
        .I2(preset__207_carry__4_n_7),
        .I3(preset__145_carry__5_n_7),
        .I4(preset__207_carry__4_n_6),
        .I5(preset_carry__6_n_0),
        .O(preset__268_carry__7_i_3_n_0));
  LUT6 #(
    .INIT(64'h54808054D5FEFED5)) 
    preset__268_carry__7_i_4
       (.I0(preset__83_carry__6_n_0),
        .I1(preset__145_carry__4_n_5),
        .I2(preset__207_carry__3_n_4),
        .I3(preset__145_carry__4_n_4),
        .I4(preset__207_carry__4_n_7),
        .I5(preset_carry__6_n_0),
        .O(preset__268_carry__7_i_4_n_0));
  LUT6 #(
    .INIT(64'h36936C366C36C96C)) 
    preset__268_carry__7_i_5
       (.I0(preset__268_carry__7_i_9_n_0),
        .I1(preset__268_carry__7_i_10_n_0),
        .I2(preset__207_carry__4_n_4),
        .I3(preset__83_carry__6_n_0),
        .I4(preset__145_carry__5_n_5),
        .I5(preset_carry__6_n_0),
        .O(preset__268_carry__7_i_5_n_0));
  LUT6 #(
    .INIT(64'h36936C366C36C96C)) 
    preset__268_carry__7_i_6
       (.I0(preset__268_carry__7_i_11_n_0),
        .I1(preset__268_carry__7_i_12_n_0),
        .I2(preset__207_carry__4_n_5),
        .I3(preset__83_carry__6_n_0),
        .I4(preset__145_carry__5_n_6),
        .I5(preset_carry__6_n_0),
        .O(preset__268_carry__7_i_6_n_0));
  LUT6 #(
    .INIT(64'h36936C366C36C96C)) 
    preset__268_carry__7_i_7
       (.I0(preset__268_carry__7_i_13_n_0),
        .I1(preset__268_carry__7_i_14_n_0),
        .I2(preset__207_carry__4_n_6),
        .I3(preset__83_carry__6_n_0),
        .I4(preset__145_carry__5_n_7),
        .I5(preset_carry__6_n_0),
        .O(preset__268_carry__7_i_7_n_0));
  LUT6 #(
    .INIT(64'h36936C366C36C96C)) 
    preset__268_carry__7_i_8
       (.I0(preset__268_carry__7_i_15_n_0),
        .I1(preset__268_carry__7_i_16_n_0),
        .I2(preset__207_carry__4_n_7),
        .I3(preset__83_carry__6_n_0),
        .I4(preset__145_carry__4_n_4),
        .I5(preset_carry__6_n_0),
        .O(preset__268_carry__7_i_8_n_0));
  (* SOFT_HLUTNM = "soft_lutpair12" *) 
  LUT3 #(
    .INIT(8'hB2)) 
    preset__268_carry__7_i_9
       (.I0(preset__145_carry__5_n_6),
        .I1(preset__83_carry__6_n_0),
        .I2(preset__207_carry__4_n_5),
        .O(preset__268_carry__7_i_9_n_0));
  CARRY4 preset__268_carry__8
       (.CI(preset__268_carry__7_n_0),
        .CO({preset__268_carry__8_n_0,preset__268_carry__8_n_1,preset__268_carry__8_n_2,preset__268_carry__8_n_3}),
        .CYINIT(1'b0),
        .DI({preset__268_carry__8_i_1_n_0,preset__268_carry__8_i_2_n_0,preset__268_carry__8_i_3_n_0,preset__268_carry__8_i_4_n_0}),
        .O({preset__268_carry__8_n_4,preset__268_carry__8_n_5,preset__268_carry__8_n_6,preset__268_carry__8_n_7}),
        .S({preset__268_carry__8_i_5_n_0,preset__268_carry__8_i_6_n_0,preset__268_carry__8_i_7_n_0,preset__268_carry__8_i_8_n_0}));
  LUT6 #(
    .INIT(64'h32808032B3FEFEB3)) 
    preset__268_carry__8_i_1
       (.I0(preset__145_carry__6_n_6),
        .I1(preset__83_carry__6_n_0),
        .I2(preset__207_carry__5_n_5),
        .I3(preset__145_carry__6_n_5),
        .I4(preset__207_carry__5_n_4),
        .I5(preset_carry__6_n_0),
        .O(preset__268_carry__8_i_1_n_0));
  LUT3 #(
    .INIT(8'h96)) 
    preset__268_carry__8_i_10
       (.I0(preset__207_carry__6_n_7),
        .I1(preset__83_carry__6_n_0),
        .I2(preset__145_carry__6_n_0),
        .O(preset__268_carry__8_i_10_n_0));
  (* SOFT_HLUTNM = "soft_lutpair15" *) 
  LUT3 #(
    .INIT(8'hB2)) 
    preset__268_carry__8_i_11
       (.I0(preset__145_carry__6_n_7),
        .I1(preset__83_carry__6_n_0),
        .I2(preset__207_carry__5_n_6),
        .O(preset__268_carry__8_i_11_n_0));
  (* SOFT_HLUTNM = "soft_lutpair17" *) 
  LUT3 #(
    .INIT(8'h69)) 
    preset__268_carry__8_i_12
       (.I0(preset__207_carry__5_n_4),
        .I1(preset__83_carry__6_n_0),
        .I2(preset__145_carry__6_n_5),
        .O(preset__268_carry__8_i_12_n_0));
  (* SOFT_HLUTNM = "soft_lutpair14" *) 
  LUT3 #(
    .INIT(8'hB2)) 
    preset__268_carry__8_i_13
       (.I0(preset__145_carry__5_n_4),
        .I1(preset__83_carry__6_n_0),
        .I2(preset__207_carry__5_n_7),
        .O(preset__268_carry__8_i_13_n_0));
  (* SOFT_HLUTNM = "soft_lutpair16" *) 
  LUT3 #(
    .INIT(8'h69)) 
    preset__268_carry__8_i_14
       (.I0(preset__207_carry__5_n_5),
        .I1(preset__83_carry__6_n_0),
        .I2(preset__145_carry__6_n_6),
        .O(preset__268_carry__8_i_14_n_0));
  (* SOFT_HLUTNM = "soft_lutpair13" *) 
  LUT3 #(
    .INIT(8'hB2)) 
    preset__268_carry__8_i_15
       (.I0(preset__145_carry__5_n_5),
        .I1(preset__83_carry__6_n_0),
        .I2(preset__207_carry__4_n_4),
        .O(preset__268_carry__8_i_15_n_0));
  (* SOFT_HLUTNM = "soft_lutpair15" *) 
  LUT3 #(
    .INIT(8'h69)) 
    preset__268_carry__8_i_16
       (.I0(preset__207_carry__5_n_6),
        .I1(preset__83_carry__6_n_0),
        .I2(preset__145_carry__6_n_7),
        .O(preset__268_carry__8_i_16_n_0));
  LUT6 #(
    .INIT(64'h32808032B3FEFEB3)) 
    preset__268_carry__8_i_2
       (.I0(preset__145_carry__6_n_7),
        .I1(preset__83_carry__6_n_0),
        .I2(preset__207_carry__5_n_6),
        .I3(preset__145_carry__6_n_6),
        .I4(preset__207_carry__5_n_5),
        .I5(preset_carry__6_n_0),
        .O(preset__268_carry__8_i_2_n_0));
  LUT6 #(
    .INIT(64'h32808032B3FEFEB3)) 
    preset__268_carry__8_i_3
       (.I0(preset__145_carry__5_n_4),
        .I1(preset__83_carry__6_n_0),
        .I2(preset__207_carry__5_n_7),
        .I3(preset__145_carry__6_n_7),
        .I4(preset__207_carry__5_n_6),
        .I5(preset_carry__6_n_0),
        .O(preset__268_carry__8_i_3_n_0));
  LUT6 #(
    .INIT(64'h32808032B3FEFEB3)) 
    preset__268_carry__8_i_4
       (.I0(preset__145_carry__5_n_5),
        .I1(preset__83_carry__6_n_0),
        .I2(preset__207_carry__4_n_4),
        .I3(preset__145_carry__5_n_4),
        .I4(preset__207_carry__5_n_7),
        .I5(preset_carry__6_n_0),
        .O(preset__268_carry__8_i_4_n_0));
  LUT6 #(
    .INIT(64'h1871E78E71E78E18)) 
    preset__268_carry__8_i_5
       (.I0(preset__268_carry__8_i_9_n_0),
        .I1(preset__145_carry__6_n_5),
        .I2(preset__83_carry__6_n_0),
        .I3(preset__207_carry__5_n_4),
        .I4(preset__268_carry__8_i_10_n_0),
        .I5(preset_carry__6_n_0),
        .O(preset__268_carry__8_i_5_n_0));
  LUT6 #(
    .INIT(64'h36936C366C36C96C)) 
    preset__268_carry__8_i_6
       (.I0(preset__268_carry__8_i_11_n_0),
        .I1(preset__268_carry__8_i_12_n_0),
        .I2(preset__207_carry__5_n_5),
        .I3(preset__83_carry__6_n_0),
        .I4(preset__145_carry__6_n_6),
        .I5(preset_carry__6_n_0),
        .O(preset__268_carry__8_i_6_n_0));
  LUT6 #(
    .INIT(64'h36936C366C36C96C)) 
    preset__268_carry__8_i_7
       (.I0(preset__268_carry__8_i_13_n_0),
        .I1(preset__268_carry__8_i_14_n_0),
        .I2(preset__207_carry__5_n_6),
        .I3(preset__83_carry__6_n_0),
        .I4(preset__145_carry__6_n_7),
        .I5(preset_carry__6_n_0),
        .O(preset__268_carry__8_i_7_n_0));
  LUT6 #(
    .INIT(64'h36936C366C36C96C)) 
    preset__268_carry__8_i_8
       (.I0(preset__268_carry__8_i_15_n_0),
        .I1(preset__268_carry__8_i_16_n_0),
        .I2(preset__207_carry__5_n_7),
        .I3(preset__83_carry__6_n_0),
        .I4(preset__145_carry__5_n_4),
        .I5(preset_carry__6_n_0),
        .O(preset__268_carry__8_i_8_n_0));
  (* SOFT_HLUTNM = "soft_lutpair16" *) 
  LUT3 #(
    .INIT(8'hB2)) 
    preset__268_carry__8_i_9
       (.I0(preset__145_carry__6_n_6),
        .I1(preset__83_carry__6_n_0),
        .I2(preset__207_carry__5_n_5),
        .O(preset__268_carry__8_i_9_n_0));
  CARRY4 preset__268_carry__9
       (.CI(preset__268_carry__8_n_0),
        .CO({NLW_preset__268_carry__9_CO_UNCONNECTED[3:1],preset__268_carry__9_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,preset__268_carry__9_i_1_n_0}),
        .O({NLW_preset__268_carry__9_O_UNCONNECTED[3:2],preset__268_carry__9_n_6,preset__268_carry__9_n_7}),
        .S({1'b0,1'b0,preset__268_carry__9_i_2_n_0,preset__268_carry__9_i_3_n_0}));
  LUT6 #(
    .INIT(64'h80323280FEB3B3FE)) 
    preset__268_carry__9_i_1
       (.I0(preset__145_carry__6_n_5),
        .I1(preset__83_carry__6_n_0),
        .I2(preset__207_carry__5_n_4),
        .I3(preset__145_carry__6_n_0),
        .I4(preset__207_carry__6_n_7),
        .I5(preset_carry__6_n_0),
        .O(preset__268_carry__9_i_1_n_0));
  LUT6 #(
    .INIT(64'hFEEC0113C880377F)) 
    preset__268_carry__9_i_2
       (.I0(preset__207_carry__6_n_7),
        .I1(preset__207_carry__6_n_6),
        .I2(preset__145_carry__6_n_0),
        .I3(preset__83_carry__6_n_0),
        .I4(preset__207_carry__6_n_5),
        .I5(preset_carry__6_n_0),
        .O(preset__268_carry__9_i_2_n_0));
  LUT6 #(
    .INIT(64'h7EE88117E881177E)) 
    preset__268_carry__9_i_3
       (.I0(preset__268_carry__9_i_4_n_0),
        .I1(preset__207_carry__6_n_7),
        .I2(preset__145_carry__6_n_0),
        .I3(preset__83_carry__6_n_0),
        .I4(preset__207_carry__6_n_6),
        .I5(preset_carry__6_n_0),
        .O(preset__268_carry__9_i_3_n_0));
  (* SOFT_HLUTNM = "soft_lutpair17" *) 
  LUT3 #(
    .INIT(8'hB2)) 
    preset__268_carry__9_i_4
       (.I0(preset__145_carry__6_n_5),
        .I1(preset__83_carry__6_n_0),
        .I2(preset__207_carry__5_n_4),
        .O(preset__268_carry__9_i_4_n_0));
  LUT2 #(
    .INIT(4'h8)) 
    preset__268_carry_i_1
       (.I0(preset_carry__0_n_5),
        .I1(preset__83_carry_n_4),
        .O(preset__268_carry_i_1_n_0));
  LUT2 #(
    .INIT(4'h8)) 
    preset__268_carry_i_2
       (.I0(preset_carry__0_n_6),
        .I1(preset__83_carry_n_5),
        .O(preset__268_carry_i_2_n_0));
  LUT2 #(
    .INIT(4'h8)) 
    preset__268_carry_i_3
       (.I0(preset_carry__0_n_7),
        .I1(preset__83_carry_n_6),
        .O(preset__268_carry_i_3_n_0));
  LUT2 #(
    .INIT(4'h8)) 
    preset__268_carry_i_4
       (.I0(preset_carry_n_4),
        .I1(preset0_n_105),
        .O(preset__268_carry_i_4_n_0));
  LUT4 #(
    .INIT(16'h8778)) 
    preset__268_carry_i_5
       (.I0(preset__83_carry_n_4),
        .I1(preset_carry__0_n_5),
        .I2(preset_carry__0_n_4),
        .I3(preset__83_carry__0_n_7),
        .O(preset__268_carry_i_5_n_0));
  LUT4 #(
    .INIT(16'h8778)) 
    preset__268_carry_i_6
       (.I0(preset__83_carry_n_5),
        .I1(preset_carry__0_n_6),
        .I2(preset_carry__0_n_5),
        .I3(preset__83_carry_n_4),
        .O(preset__268_carry_i_6_n_0));
  LUT4 #(
    .INIT(16'h8778)) 
    preset__268_carry_i_7
       (.I0(preset__83_carry_n_6),
        .I1(preset_carry__0_n_7),
        .I2(preset_carry__0_n_6),
        .I3(preset__83_carry_n_5),
        .O(preset__268_carry_i_7_n_0));
  LUT4 #(
    .INIT(16'h8778)) 
    preset__268_carry_i_8
       (.I0(preset0_n_105),
        .I1(preset_carry_n_4),
        .I2(preset_carry__0_n_7),
        .I3(preset__83_carry_n_6),
        .O(preset__268_carry_i_8_n_0));
  CARRY4 preset__371_carry
       (.CI(1'b0),
        .CO({preset__371_carry_n_0,preset__371_carry_n_1,preset__371_carry_n_2,preset__371_carry_n_3}),
        .CYINIT(1'b0),
        .DI({preset__268_carry__6_n_6,preset__268_carry__6_n_7,preset__268_carry__5_n_4,1'b0}),
        .O({preset__371_carry_n_4,preset__371_carry_n_5,preset__371_carry_n_6,preset__371_carry_n_7}),
        .S({preset__371_carry_i_1_n_0,preset__371_carry_i_2_n_0,preset__371_carry_i_3_n_0,preset__268_carry__5_n_5}));
  CARRY4 preset__371_carry__0
       (.CI(preset__371_carry_n_0),
        .CO({preset__371_carry__0_n_0,preset__371_carry__0_n_1,preset__371_carry__0_n_2,preset__371_carry__0_n_3}),
        .CYINIT(1'b0),
        .DI({preset__268_carry__7_n_6,preset__268_carry__7_n_7,preset__268_carry__6_n_4,preset__268_carry__6_n_5}),
        .O({preset__371_carry__0_n_4,preset__371_carry__0_n_5,preset__371_carry__0_n_6,preset__371_carry__0_n_7}),
        .S({preset__371_carry__0_i_1_n_0,preset__371_carry__0_i_2_n_0,preset__371_carry__0_i_3_n_0,preset__371_carry__0_i_4_n_0}));
  LUT2 #(
    .INIT(4'h6)) 
    preset__371_carry__0_i_1
       (.I0(preset__268_carry__7_n_6),
        .I1(preset__268_carry__5_n_4),
        .O(preset__371_carry__0_i_1_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    preset__371_carry__0_i_2
       (.I0(preset__268_carry__7_n_7),
        .I1(preset__268_carry__5_n_5),
        .O(preset__371_carry__0_i_2_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    preset__371_carry__0_i_3
       (.I0(preset__268_carry__6_n_4),
        .I1(preset__268_carry__5_n_6),
        .O(preset__371_carry__0_i_3_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    preset__371_carry__0_i_4
       (.I0(preset__268_carry__6_n_5),
        .I1(preset__268_carry__5_n_7),
        .O(preset__371_carry__0_i_4_n_0));
  CARRY4 preset__371_carry__1
       (.CI(preset__371_carry__0_n_0),
        .CO({preset__371_carry__1_n_0,preset__371_carry__1_n_1,preset__371_carry__1_n_2,preset__371_carry__1_n_3}),
        .CYINIT(1'b0),
        .DI({preset__268_carry__8_n_6,preset__268_carry__8_n_7,preset__268_carry__7_n_4,preset__268_carry__7_n_5}),
        .O({preset__371_carry__1_n_4,preset__371_carry__1_n_5,preset__371_carry__1_n_6,preset__371_carry__1_n_7}),
        .S({preset__371_carry__1_i_1_n_0,preset__371_carry__1_i_2_n_0,preset__371_carry__1_i_3_n_0,preset__371_carry__1_i_4_n_0}));
  LUT2 #(
    .INIT(4'h6)) 
    preset__371_carry__1_i_1
       (.I0(preset__268_carry__8_n_6),
        .I1(preset__268_carry__6_n_4),
        .O(preset__371_carry__1_i_1_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    preset__371_carry__1_i_2
       (.I0(preset__268_carry__8_n_7),
        .I1(preset__268_carry__6_n_5),
        .O(preset__371_carry__1_i_2_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    preset__371_carry__1_i_3
       (.I0(preset__268_carry__7_n_4),
        .I1(preset__268_carry__6_n_6),
        .O(preset__371_carry__1_i_3_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    preset__371_carry__1_i_4
       (.I0(preset__268_carry__7_n_5),
        .I1(preset__268_carry__6_n_7),
        .O(preset__371_carry__1_i_4_n_0));
  CARRY4 preset__371_carry__2
       (.CI(preset__371_carry__1_n_0),
        .CO({preset__371_carry__2_n_0,preset__371_carry__2_n_1,preset__371_carry__2_n_2,preset__371_carry__2_n_3}),
        .CYINIT(1'b0),
        .DI({preset__268_carry__9_n_6,preset__268_carry__9_n_7,preset__268_carry__8_n_4,preset__268_carry__8_n_5}),
        .O({preset__371_carry__2_n_4,preset__371_carry__2_n_5,preset__371_carry__2_n_6,preset__371_carry__2_n_7}),
        .S({preset__371_carry__2_i_1_n_0,preset__371_carry__2_i_2_n_0,preset__371_carry__2_i_3_n_0,preset__371_carry__2_i_4_n_0}));
  LUT2 #(
    .INIT(4'h6)) 
    preset__371_carry__2_i_1
       (.I0(preset__268_carry__9_n_6),
        .I1(preset__268_carry__7_n_4),
        .O(preset__371_carry__2_i_1_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    preset__371_carry__2_i_2
       (.I0(preset__268_carry__9_n_7),
        .I1(preset__268_carry__7_n_5),
        .O(preset__371_carry__2_i_2_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    preset__371_carry__2_i_3
       (.I0(preset__268_carry__8_n_4),
        .I1(preset__268_carry__7_n_6),
        .O(preset__371_carry__2_i_3_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    preset__371_carry__2_i_4
       (.I0(preset__268_carry__8_n_5),
        .I1(preset__268_carry__7_n_7),
        .O(preset__371_carry__2_i_4_n_0));
  CARRY4 preset__371_carry__3
       (.CI(preset__371_carry__2_n_0),
        .CO({preset__371_carry__3_n_0,preset__371_carry__3_n_1,preset__371_carry__3_n_2,preset__371_carry__3_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({preset__371_carry__3_n_4,preset__371_carry__3_n_5,preset__371_carry__3_n_6,preset__371_carry__3_n_7}),
        .S({preset__268_carry__8_n_4,preset__268_carry__8_n_5,preset__268_carry__8_n_6,preset__268_carry__8_n_7}));
  CARRY4 preset__371_carry__4
       (.CI(preset__371_carry__3_n_0),
        .CO({NLW_preset__371_carry__4_CO_UNCONNECTED[3:1],preset__371_carry__4_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({NLW_preset__371_carry__4_O_UNCONNECTED[3:2],preset__371_carry__4_n_6,preset__371_carry__4_n_7}),
        .S({1'b0,1'b0,preset__268_carry__9_n_6,preset__268_carry__9_n_7}));
  LUT2 #(
    .INIT(4'h6)) 
    preset__371_carry_i_1
       (.I0(preset__268_carry__6_n_6),
        .I1(preset__268_carry__4_n_4),
        .O(preset__371_carry_i_1_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    preset__371_carry_i_2
       (.I0(preset__268_carry__6_n_7),
        .I1(preset__268_carry__4_n_5),
        .O(preset__371_carry_i_2_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    preset__371_carry_i_3
       (.I0(preset__268_carry__5_n_4),
        .I1(preset__268_carry__4_n_6),
        .O(preset__371_carry_i_3_n_0));
  CARRY4 preset__430_carry
       (.CI(1'b0),
        .CO({preset__430_carry_n_0,preset__430_carry_n_1,preset__430_carry_n_2,preset__430_carry_n_3}),
        .CYINIT(1'b0),
        .DI({preset__430_carry_i_1_n_0,preset__430_carry_i_2_n_0,preset__430_carry_i_3_n_0,1'b0}),
        .O(NLW_preset__430_carry_O_UNCONNECTED[3:0]),
        .S({preset__430_carry_i_4_n_0,preset__430_carry_i_5_n_0,preset__430_carry_i_6_n_0,preset__430_carry_i_7_n_0}));
  CARRY4 preset__430_carry__0
       (.CI(preset__430_carry_n_0),
        .CO({preset__430_carry__0_n_0,preset__430_carry__0_n_1,preset__430_carry__0_n_2,preset__430_carry__0_n_3}),
        .CYINIT(1'b0),
        .DI({preset__430_carry__0_i_1_n_0,preset__430_carry__0_i_2_n_0,preset__430_carry__0_i_3_n_0,preset__430_carry__0_i_4_n_0}),
        .O(NLW_preset__430_carry__0_O_UNCONNECTED[3:0]),
        .S({preset__430_carry__0_i_5_n_0,preset__430_carry__0_i_6_n_0,preset__430_carry__0_i_7_n_0,preset__430_carry__0_i_8_n_0}));
  LUT2 #(
    .INIT(4'hB)) 
    preset__430_carry__0_i_1
       (.I0(preset__371_carry_n_6),
        .I1(preset0_n_98),
        .O(preset__430_carry__0_i_1_n_0));
  LUT2 #(
    .INIT(4'h2)) 
    preset__430_carry__0_i_2
       (.I0(preset__371_carry_n_7),
        .I1(preset0_n_99),
        .O(preset__430_carry__0_i_2_n_0));
  LUT2 #(
    .INIT(4'h2)) 
    preset__430_carry__0_i_3
       (.I0(preset__268_carry__5_n_6),
        .I1(preset0_n_100),
        .O(preset__430_carry__0_i_3_n_0));
  LUT2 #(
    .INIT(4'h2)) 
    preset__430_carry__0_i_4
       (.I0(preset__268_carry__5_n_7),
        .I1(preset0_n_101),
        .O(preset__430_carry__0_i_4_n_0));
  LUT4 #(
    .INIT(16'h2DD2)) 
    preset__430_carry__0_i_5
       (.I0(preset0_n_98),
        .I1(preset__371_carry_n_6),
        .I2(preset__371_carry_n_5),
        .I3(preset0_n_97),
        .O(preset__430_carry__0_i_5_n_0));
  LUT4 #(
    .INIT(16'h4BB4)) 
    preset__430_carry__0_i_6
       (.I0(preset0_n_99),
        .I1(preset__371_carry_n_7),
        .I2(preset__371_carry_n_6),
        .I3(preset0_n_98),
        .O(preset__430_carry__0_i_6_n_0));
  LUT4 #(
    .INIT(16'hB44B)) 
    preset__430_carry__0_i_7
       (.I0(preset0_n_100),
        .I1(preset__268_carry__5_n_6),
        .I2(preset__371_carry_n_7),
        .I3(preset0_n_99),
        .O(preset__430_carry__0_i_7_n_0));
  LUT4 #(
    .INIT(16'hB44B)) 
    preset__430_carry__0_i_8
       (.I0(preset0_n_101),
        .I1(preset__268_carry__5_n_7),
        .I2(preset__268_carry__5_n_6),
        .I3(preset0_n_100),
        .O(preset__430_carry__0_i_8_n_0));
  CARRY4 preset__430_carry__1
       (.CI(preset__430_carry__0_n_0),
        .CO({preset__430_carry__1_n_0,preset__430_carry__1_n_1,preset__430_carry__1_n_2,preset__430_carry__1_n_3}),
        .CYINIT(1'b0),
        .DI({preset__430_carry__1_i_1_n_0,preset__430_carry__1_i_2_n_0,preset__430_carry__1_i_3_n_0,preset__430_carry__1_i_4_n_0}),
        .O(NLW_preset__430_carry__1_O_UNCONNECTED[3:0]),
        .S({preset__430_carry__1_i_5_n_0,preset__430_carry__1_i_6_n_0,preset__430_carry__1_i_7_n_0,preset__430_carry__1_i_8_n_0}));
  LUT2 #(
    .INIT(4'h2)) 
    preset__430_carry__1_i_1
       (.I0(preset__371_carry__0_n_6),
        .I1(preset0_n_94),
        .O(preset__430_carry__1_i_1_n_0));
  LUT2 #(
    .INIT(4'h2)) 
    preset__430_carry__1_i_2
       (.I0(preset__371_carry__0_n_7),
        .I1(preset0_n_95),
        .O(preset__430_carry__1_i_2_n_0));
  LUT2 #(
    .INIT(4'h2)) 
    preset__430_carry__1_i_3
       (.I0(preset__371_carry_n_4),
        .I1(preset0_n_96),
        .O(preset__430_carry__1_i_3_n_0));
  LUT2 #(
    .INIT(4'h2)) 
    preset__430_carry__1_i_4
       (.I0(preset__371_carry_n_5),
        .I1(preset0_n_97),
        .O(preset__430_carry__1_i_4_n_0));
  LUT4 #(
    .INIT(16'hB44B)) 
    preset__430_carry__1_i_5
       (.I0(preset0_n_94),
        .I1(preset__371_carry__0_n_6),
        .I2(preset__371_carry__0_n_5),
        .I3(preset0_n_93),
        .O(preset__430_carry__1_i_5_n_0));
  LUT4 #(
    .INIT(16'hB44B)) 
    preset__430_carry__1_i_6
       (.I0(preset0_n_95),
        .I1(preset__371_carry__0_n_7),
        .I2(preset__371_carry__0_n_6),
        .I3(preset0_n_94),
        .O(preset__430_carry__1_i_6_n_0));
  LUT4 #(
    .INIT(16'hB44B)) 
    preset__430_carry__1_i_7
       (.I0(preset0_n_96),
        .I1(preset__371_carry_n_4),
        .I2(preset__371_carry__0_n_7),
        .I3(preset0_n_95),
        .O(preset__430_carry__1_i_7_n_0));
  LUT4 #(
    .INIT(16'hB44B)) 
    preset__430_carry__1_i_8
       (.I0(preset0_n_97),
        .I1(preset__371_carry_n_5),
        .I2(preset__371_carry_n_4),
        .I3(preset0_n_96),
        .O(preset__430_carry__1_i_8_n_0));
  CARRY4 preset__430_carry__2
       (.CI(preset__430_carry__1_n_0),
        .CO({preset__430_carry__2_n_0,preset__430_carry__2_n_1,preset__430_carry__2_n_2,preset__430_carry__2_n_3}),
        .CYINIT(1'b0),
        .DI({preset__430_carry__2_i_1_n_0,preset__430_carry__2_i_2_n_0,preset__430_carry__2_i_3_n_0,preset__430_carry__2_i_4_n_0}),
        .O(NLW_preset__430_carry__2_O_UNCONNECTED[3:0]),
        .S({preset__430_carry__2_i_5_n_0,preset__430_carry__2_i_6_n_0,preset__430_carry__2_i_7_n_0,preset__430_carry__2_i_8_n_0}));
  LUT2 #(
    .INIT(4'h2)) 
    preset__430_carry__2_i_1
       (.I0(preset__371_carry__1_n_6),
        .I1(preset0_n_90),
        .O(preset__430_carry__2_i_1_n_0));
  LUT2 #(
    .INIT(4'h2)) 
    preset__430_carry__2_i_2
       (.I0(preset__371_carry__1_n_7),
        .I1(preset0_n_91),
        .O(preset__430_carry__2_i_2_n_0));
  LUT2 #(
    .INIT(4'h2)) 
    preset__430_carry__2_i_3
       (.I0(preset__371_carry__0_n_4),
        .I1(preset0_n_92),
        .O(preset__430_carry__2_i_3_n_0));
  LUT2 #(
    .INIT(4'h2)) 
    preset__430_carry__2_i_4
       (.I0(preset__371_carry__0_n_5),
        .I1(preset0_n_93),
        .O(preset__430_carry__2_i_4_n_0));
  LUT4 #(
    .INIT(16'hB44B)) 
    preset__430_carry__2_i_5
       (.I0(preset0_n_90),
        .I1(preset__371_carry__1_n_6),
        .I2(preset__371_carry__1_n_5),
        .I3(preset0_n_89),
        .O(preset__430_carry__2_i_5_n_0));
  LUT4 #(
    .INIT(16'hB44B)) 
    preset__430_carry__2_i_6
       (.I0(preset0_n_91),
        .I1(preset__371_carry__1_n_7),
        .I2(preset__371_carry__1_n_6),
        .I3(preset0_n_90),
        .O(preset__430_carry__2_i_6_n_0));
  LUT4 #(
    .INIT(16'hB44B)) 
    preset__430_carry__2_i_7
       (.I0(preset0_n_92),
        .I1(preset__371_carry__0_n_4),
        .I2(preset__371_carry__1_n_7),
        .I3(preset0_n_91),
        .O(preset__430_carry__2_i_7_n_0));
  LUT4 #(
    .INIT(16'hB44B)) 
    preset__430_carry__2_i_8
       (.I0(preset0_n_93),
        .I1(preset__371_carry__0_n_5),
        .I2(preset__371_carry__0_n_4),
        .I3(preset0_n_92),
        .O(preset__430_carry__2_i_8_n_0));
  CARRY4 preset__430_carry__3
       (.CI(preset__430_carry__2_n_0),
        .CO({preset__430_carry__3_n_0,preset__430_carry__3_n_1,preset__430_carry__3_n_2,preset__430_carry__3_n_3}),
        .CYINIT(1'b0),
        .DI({preset__430_carry__3_i_1_n_0,preset__430_carry__3_i_2_n_0,preset__430_carry__3_i_3_n_0,preset__430_carry__3_i_4_n_0}),
        .O(NLW_preset__430_carry__3_O_UNCONNECTED[3:0]),
        .S({preset__430_carry__3_i_5_n_0,preset__430_carry__3_i_6_n_0,preset__430_carry__3_i_7_n_0,preset__430_carry__3_i_8_n_0}));
  LUT2 #(
    .INIT(4'h2)) 
    preset__430_carry__3_i_1
       (.I0(preset__371_carry__2_n_6),
        .I1(preset0_n_86),
        .O(preset__430_carry__3_i_1_n_0));
  LUT2 #(
    .INIT(4'h2)) 
    preset__430_carry__3_i_2
       (.I0(preset__371_carry__2_n_7),
        .I1(preset0_n_87),
        .O(preset__430_carry__3_i_2_n_0));
  LUT2 #(
    .INIT(4'h2)) 
    preset__430_carry__3_i_3
       (.I0(preset__371_carry__1_n_4),
        .I1(preset0_n_88),
        .O(preset__430_carry__3_i_3_n_0));
  LUT2 #(
    .INIT(4'h2)) 
    preset__430_carry__3_i_4
       (.I0(preset__371_carry__1_n_5),
        .I1(preset0_n_89),
        .O(preset__430_carry__3_i_4_n_0));
  LUT4 #(
    .INIT(16'hB44B)) 
    preset__430_carry__3_i_5
       (.I0(preset0_n_86),
        .I1(preset__371_carry__2_n_6),
        .I2(preset__371_carry__2_n_5),
        .I3(preset0_n_85),
        .O(preset__430_carry__3_i_5_n_0));
  LUT4 #(
    .INIT(16'hB44B)) 
    preset__430_carry__3_i_6
       (.I0(preset0_n_87),
        .I1(preset__371_carry__2_n_7),
        .I2(preset__371_carry__2_n_6),
        .I3(preset0_n_86),
        .O(preset__430_carry__3_i_6_n_0));
  LUT4 #(
    .INIT(16'hB44B)) 
    preset__430_carry__3_i_7
       (.I0(preset0_n_88),
        .I1(preset__371_carry__1_n_4),
        .I2(preset__371_carry__2_n_7),
        .I3(preset0_n_87),
        .O(preset__430_carry__3_i_7_n_0));
  LUT4 #(
    .INIT(16'hB44B)) 
    preset__430_carry__3_i_8
       (.I0(preset0_n_89),
        .I1(preset__371_carry__1_n_5),
        .I2(preset__371_carry__1_n_4),
        .I3(preset0_n_88),
        .O(preset__430_carry__3_i_8_n_0));
  CARRY4 preset__430_carry__4
       (.CI(preset__430_carry__3_n_0),
        .CO({preset__430_carry__4_n_0,preset__430_carry__4_n_1,preset__430_carry__4_n_2,preset__430_carry__4_n_3}),
        .CYINIT(1'b0),
        .DI({preset__430_carry__4_i_1_n_0,preset__430_carry__4_i_2_n_0,preset__430_carry__4_i_3_n_0,preset__430_carry__4_i_4_n_0}),
        .O(NLW_preset__430_carry__4_O_UNCONNECTED[3:0]),
        .S({preset__430_carry__4_i_5_n_0,preset__430_carry__4_i_6_n_0,preset__430_carry__4_i_7_n_0,preset__430_carry__4_i_8_n_0}));
  LUT2 #(
    .INIT(4'h2)) 
    preset__430_carry__4_i_1
       (.I0(preset__371_carry__3_n_6),
        .I1(preset0_n_82),
        .O(preset__430_carry__4_i_1_n_0));
  LUT2 #(
    .INIT(4'h2)) 
    preset__430_carry__4_i_2
       (.I0(preset__371_carry__3_n_7),
        .I1(preset0_n_83),
        .O(preset__430_carry__4_i_2_n_0));
  LUT2 #(
    .INIT(4'h2)) 
    preset__430_carry__4_i_3
       (.I0(preset__371_carry__2_n_4),
        .I1(preset0_n_84),
        .O(preset__430_carry__4_i_3_n_0));
  LUT2 #(
    .INIT(4'h2)) 
    preset__430_carry__4_i_4
       (.I0(preset__371_carry__2_n_5),
        .I1(preset0_n_85),
        .O(preset__430_carry__4_i_4_n_0));
  LUT4 #(
    .INIT(16'hB44B)) 
    preset__430_carry__4_i_5
       (.I0(preset0_n_82),
        .I1(preset__371_carry__3_n_6),
        .I2(preset__371_carry__3_n_5),
        .I3(preset0_n_81),
        .O(preset__430_carry__4_i_5_n_0));
  LUT4 #(
    .INIT(16'hB44B)) 
    preset__430_carry__4_i_6
       (.I0(preset0_n_83),
        .I1(preset__371_carry__3_n_7),
        .I2(preset__371_carry__3_n_6),
        .I3(preset0_n_82),
        .O(preset__430_carry__4_i_6_n_0));
  LUT4 #(
    .INIT(16'hB44B)) 
    preset__430_carry__4_i_7
       (.I0(preset0_n_84),
        .I1(preset__371_carry__2_n_4),
        .I2(preset__371_carry__3_n_7),
        .I3(preset0_n_83),
        .O(preset__430_carry__4_i_7_n_0));
  LUT4 #(
    .INIT(16'hB44B)) 
    preset__430_carry__4_i_8
       (.I0(preset0_n_85),
        .I1(preset__371_carry__2_n_5),
        .I2(preset__371_carry__2_n_4),
        .I3(preset0_n_84),
        .O(preset__430_carry__4_i_8_n_0));
  CARRY4 preset__430_carry__5
       (.CI(preset__430_carry__4_n_0),
        .CO({NLW_preset__430_carry__5_CO_UNCONNECTED[3],preset__430_carry__5_n_1,preset__430_carry__5_n_2,preset__430_carry__5_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,preset__430_carry__5_i_1_n_0,preset__430_carry__5_i_2_n_0,preset__430_carry__5_i_3_n_0}),
        .O(NLW_preset__430_carry__5_O_UNCONNECTED[3:0]),
        .S({1'b0,preset__430_carry__5_i_4_n_0,preset__430_carry__5_i_5_n_0,preset__430_carry__5_i_6_n_0}));
  LUT2 #(
    .INIT(4'h2)) 
    preset__430_carry__5_i_1
       (.I0(preset__371_carry__4_n_7),
        .I1(preset0_n_79),
        .O(preset__430_carry__5_i_1_n_0));
  LUT2 #(
    .INIT(4'h2)) 
    preset__430_carry__5_i_2
       (.I0(preset__371_carry__3_n_4),
        .I1(preset0_n_80),
        .O(preset__430_carry__5_i_2_n_0));
  LUT2 #(
    .INIT(4'h2)) 
    preset__430_carry__5_i_3
       (.I0(preset__371_carry__3_n_5),
        .I1(preset0_n_81),
        .O(preset__430_carry__5_i_3_n_0));
  LUT4 #(
    .INIT(16'hB44B)) 
    preset__430_carry__5_i_4
       (.I0(preset0_n_79),
        .I1(preset__371_carry__4_n_7),
        .I2(preset__371_carry__4_n_6),
        .I3(preset0_n_78),
        .O(preset__430_carry__5_i_4_n_0));
  LUT4 #(
    .INIT(16'hB44B)) 
    preset__430_carry__5_i_5
       (.I0(preset0_n_80),
        .I1(preset__371_carry__3_n_4),
        .I2(preset__371_carry__4_n_7),
        .I3(preset0_n_79),
        .O(preset__430_carry__5_i_5_n_0));
  LUT4 #(
    .INIT(16'hB44B)) 
    preset__430_carry__5_i_6
       (.I0(preset0_n_81),
        .I1(preset__371_carry__3_n_5),
        .I2(preset__371_carry__3_n_4),
        .I3(preset0_n_80),
        .O(preset__430_carry__5_i_6_n_0));
  LUT2 #(
    .INIT(4'h2)) 
    preset__430_carry_i_1
       (.I0(preset__268_carry__4_n_4),
        .I1(preset0_n_102),
        .O(preset__430_carry_i_1_n_0));
  LUT2 #(
    .INIT(4'h2)) 
    preset__430_carry_i_2
       (.I0(preset__268_carry__4_n_5),
        .I1(preset0_n_103),
        .O(preset__430_carry_i_2_n_0));
  LUT2 #(
    .INIT(4'hB)) 
    preset__430_carry_i_3
       (.I0(preset__268_carry__4_n_6),
        .I1(preset0_n_104),
        .O(preset__430_carry_i_3_n_0));
  LUT4 #(
    .INIT(16'hB44B)) 
    preset__430_carry_i_4
       (.I0(preset0_n_102),
        .I1(preset__268_carry__4_n_4),
        .I2(preset__268_carry__5_n_7),
        .I3(preset0_n_101),
        .O(preset__430_carry_i_4_n_0));
  LUT4 #(
    .INIT(16'hB44B)) 
    preset__430_carry_i_5
       (.I0(preset0_n_103),
        .I1(preset__268_carry__4_n_5),
        .I2(preset__268_carry__4_n_4),
        .I3(preset0_n_102),
        .O(preset__430_carry_i_5_n_0));
  LUT4 #(
    .INIT(16'h2DD2)) 
    preset__430_carry_i_6
       (.I0(preset0_n_104),
        .I1(preset__268_carry__4_n_6),
        .I2(preset__268_carry__4_n_5),
        .I3(preset0_n_103),
        .O(preset__430_carry_i_6_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    preset__430_carry_i_7
       (.I0(preset0_n_104),
        .I1(preset__268_carry__4_n_6),
        .O(preset__430_carry_i_7_n_0));
  CARRY4 preset__483_carry
       (.CI(1'b0),
        .CO({preset__483_carry_n_0,preset__483_carry_n_1,preset__483_carry_n_2,preset__483_carry_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b1}),
        .O({preset__483_carry_n_4,preset__483_carry_n_5,preset__483_carry_n_6,preset__483_carry_n_7}),
        .S({preset__268_carry__5_n_7,preset__268_carry__4_n_4,preset__268_carry__4_n_5,preset__483_carry_i_1_n_0}));
  CARRY4 preset__483_carry__0
       (.CI(preset__483_carry_n_0),
        .CO({preset__483_carry__0_n_0,preset__483_carry__0_n_1,preset__483_carry__0_n_2,preset__483_carry__0_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({preset__483_carry__0_n_4,preset__483_carry__0_n_5,preset__483_carry__0_n_6,preset__483_carry__0_n_7}),
        .S({preset__268_carry__6_n_7,preset__268_carry__5_n_4,preset__268_carry__5_n_5,preset__268_carry__5_n_6}));
  CARRY4 preset__483_carry__1
       (.CI(preset__483_carry__0_n_0),
        .CO({preset__483_carry__1_n_0,preset__483_carry__1_n_1,preset__483_carry__1_n_2,preset__483_carry__1_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({preset__483_carry__1_n_4,preset__483_carry__1_n_5,preset__483_carry__1_n_6,preset__483_carry__1_n_7}),
        .S({preset__268_carry__7_n_7,preset__268_carry__6_n_4,preset__268_carry__6_n_5,preset__268_carry__6_n_6}));
  CARRY4 preset__483_carry__2
       (.CI(preset__483_carry__1_n_0),
        .CO({preset__483_carry__2_n_0,preset__483_carry__2_n_1,preset__483_carry__2_n_2,preset__483_carry__2_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({preset__483_carry__2_n_4,preset__483_carry__2_n_5,preset__483_carry__2_n_6,preset__483_carry__2_n_7}),
        .S({preset__268_carry__8_n_7,preset__268_carry__7_n_4,preset__268_carry__7_n_5,preset__268_carry__7_n_6}));
  CARRY4 preset__483_carry__3
       (.CI(preset__483_carry__2_n_0),
        .CO({preset__483_carry__3_n_0,preset__483_carry__3_n_1,preset__483_carry__3_n_2,preset__483_carry__3_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({preset__483_carry__3_n_4,preset__483_carry__3_n_5,preset__483_carry__3_n_6,preset__483_carry__3_n_7}),
        .S({preset__268_carry__9_n_7,preset__268_carry__8_n_4,preset__268_carry__8_n_5,preset__268_carry__8_n_6}));
  CARRY4 preset__483_carry__4
       (.CI(preset__483_carry__3_n_0),
        .CO(NLW_preset__483_carry__4_CO_UNCONNECTED[3:0]),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({NLW_preset__483_carry__4_O_UNCONNECTED[3:1],preset__483_carry__4_n_7}),
        .S({1'b0,1'b0,1'b0,preset__268_carry__9_n_6}));
  LUT1 #(
    .INIT(2'h1)) 
    preset__483_carry_i_1
       (.I0(preset__268_carry__4_n_6),
        .O(preset__483_carry_i_1_n_0));
  CARRY4 preset__83_carry
       (.CI(1'b0),
        .CO({preset__83_carry_n_0,preset__83_carry_n_1,preset__83_carry_n_2,preset__83_carry_n_3}),
        .CYINIT(1'b0),
        .DI({preset0_n_105,1'b0,1'b0,1'b1}),
        .O({preset__83_carry_n_4,preset__83_carry_n_5,preset__83_carry_n_6,NLW_preset__83_carry_O_UNCONNECTED[0]}),
        .S({preset__83_carry_i_1_n_0,preset__83_carry_i_2_n_0,preset__83_carry_i_3_n_0,preset0_n_105}));
  CARRY4 preset__83_carry__0
       (.CI(preset__83_carry_n_0),
        .CO({preset__83_carry__0_n_0,preset__83_carry__0_n_1,preset__83_carry__0_n_2,preset__83_carry__0_n_3}),
        .CYINIT(1'b0),
        .DI({preset0_n_101,preset0_n_102,preset0_n_103,preset0_n_104}),
        .O({preset__83_carry__0_n_4,preset__83_carry__0_n_5,preset__83_carry__0_n_6,preset__83_carry__0_n_7}),
        .S({preset__83_carry__0_i_1_n_0,preset__83_carry__0_i_2_n_0,preset__83_carry__0_i_3_n_0,preset__83_carry__0_i_4_n_0}));
  LUT2 #(
    .INIT(4'h9)) 
    preset__83_carry__0_i_1
       (.I0(preset0_n_101),
        .I1(preset0_n_98),
        .O(preset__83_carry__0_i_1_n_0));
  LUT2 #(
    .INIT(4'h9)) 
    preset__83_carry__0_i_2
       (.I0(preset0_n_102),
        .I1(preset0_n_99),
        .O(preset__83_carry__0_i_2_n_0));
  LUT2 #(
    .INIT(4'h9)) 
    preset__83_carry__0_i_3
       (.I0(preset0_n_103),
        .I1(preset0_n_100),
        .O(preset__83_carry__0_i_3_n_0));
  LUT2 #(
    .INIT(4'h9)) 
    preset__83_carry__0_i_4
       (.I0(preset0_n_104),
        .I1(preset0_n_101),
        .O(preset__83_carry__0_i_4_n_0));
  CARRY4 preset__83_carry__1
       (.CI(preset__83_carry__0_n_0),
        .CO({preset__83_carry__1_n_0,preset__83_carry__1_n_1,preset__83_carry__1_n_2,preset__83_carry__1_n_3}),
        .CYINIT(1'b0),
        .DI({preset0_n_97,preset0_n_98,preset0_n_99,preset0_n_100}),
        .O({preset__83_carry__1_n_4,preset__83_carry__1_n_5,preset__83_carry__1_n_6,preset__83_carry__1_n_7}),
        .S({preset__83_carry__1_i_1_n_0,preset__83_carry__1_i_2_n_0,preset__83_carry__1_i_3_n_0,preset__83_carry__1_i_4_n_0}));
  LUT2 #(
    .INIT(4'h9)) 
    preset__83_carry__1_i_1
       (.I0(preset0_n_97),
        .I1(preset0_n_94),
        .O(preset__83_carry__1_i_1_n_0));
  LUT2 #(
    .INIT(4'h9)) 
    preset__83_carry__1_i_2
       (.I0(preset0_n_98),
        .I1(preset0_n_95),
        .O(preset__83_carry__1_i_2_n_0));
  LUT2 #(
    .INIT(4'h9)) 
    preset__83_carry__1_i_3
       (.I0(preset0_n_99),
        .I1(preset0_n_96),
        .O(preset__83_carry__1_i_3_n_0));
  LUT2 #(
    .INIT(4'h9)) 
    preset__83_carry__1_i_4
       (.I0(preset0_n_100),
        .I1(preset0_n_97),
        .O(preset__83_carry__1_i_4_n_0));
  CARRY4 preset__83_carry__2
       (.CI(preset__83_carry__1_n_0),
        .CO({preset__83_carry__2_n_0,preset__83_carry__2_n_1,preset__83_carry__2_n_2,preset__83_carry__2_n_3}),
        .CYINIT(1'b0),
        .DI({preset0_n_93,preset0_n_94,preset0_n_95,preset0_n_96}),
        .O({preset__83_carry__2_n_4,preset__83_carry__2_n_5,preset__83_carry__2_n_6,preset__83_carry__2_n_7}),
        .S({preset__83_carry__2_i_1_n_0,preset__83_carry__2_i_2_n_0,preset__83_carry__2_i_3_n_0,preset__83_carry__2_i_4_n_0}));
  LUT2 #(
    .INIT(4'h9)) 
    preset__83_carry__2_i_1
       (.I0(preset0_n_93),
        .I1(preset0_n_90),
        .O(preset__83_carry__2_i_1_n_0));
  LUT2 #(
    .INIT(4'h9)) 
    preset__83_carry__2_i_2
       (.I0(preset0_n_94),
        .I1(preset0_n_91),
        .O(preset__83_carry__2_i_2_n_0));
  LUT2 #(
    .INIT(4'h9)) 
    preset__83_carry__2_i_3
       (.I0(preset0_n_95),
        .I1(preset0_n_92),
        .O(preset__83_carry__2_i_3_n_0));
  LUT2 #(
    .INIT(4'h9)) 
    preset__83_carry__2_i_4
       (.I0(preset0_n_96),
        .I1(preset0_n_93),
        .O(preset__83_carry__2_i_4_n_0));
  CARRY4 preset__83_carry__3
       (.CI(preset__83_carry__2_n_0),
        .CO({preset__83_carry__3_n_0,preset__83_carry__3_n_1,preset__83_carry__3_n_2,preset__83_carry__3_n_3}),
        .CYINIT(1'b0),
        .DI({preset0_n_89,preset0_n_90,preset0_n_91,preset0_n_92}),
        .O({preset__83_carry__3_n_4,preset__83_carry__3_n_5,preset__83_carry__3_n_6,preset__83_carry__3_n_7}),
        .S({preset__83_carry__3_i_1_n_0,preset__83_carry__3_i_2_n_0,preset__83_carry__3_i_3_n_0,preset__83_carry__3_i_4_n_0}));
  LUT2 #(
    .INIT(4'h9)) 
    preset__83_carry__3_i_1
       (.I0(preset0_n_89),
        .I1(preset0_n_86),
        .O(preset__83_carry__3_i_1_n_0));
  LUT2 #(
    .INIT(4'h9)) 
    preset__83_carry__3_i_2
       (.I0(preset0_n_90),
        .I1(preset0_n_87),
        .O(preset__83_carry__3_i_2_n_0));
  LUT2 #(
    .INIT(4'h9)) 
    preset__83_carry__3_i_3
       (.I0(preset0_n_91),
        .I1(preset0_n_88),
        .O(preset__83_carry__3_i_3_n_0));
  LUT2 #(
    .INIT(4'h9)) 
    preset__83_carry__3_i_4
       (.I0(preset0_n_92),
        .I1(preset0_n_89),
        .O(preset__83_carry__3_i_4_n_0));
  CARRY4 preset__83_carry__4
       (.CI(preset__83_carry__3_n_0),
        .CO({preset__83_carry__4_n_0,preset__83_carry__4_n_1,preset__83_carry__4_n_2,preset__83_carry__4_n_3}),
        .CYINIT(1'b0),
        .DI({preset0_n_85,preset0_n_86,preset0_n_87,preset0_n_88}),
        .O({preset__83_carry__4_n_4,preset__83_carry__4_n_5,preset__83_carry__4_n_6,preset__83_carry__4_n_7}),
        .S({preset__83_carry__4_i_1_n_0,preset__83_carry__4_i_2_n_0,preset__83_carry__4_i_3_n_0,preset__83_carry__4_i_4_n_0}));
  LUT2 #(
    .INIT(4'h9)) 
    preset__83_carry__4_i_1
       (.I0(preset0_n_85),
        .I1(preset0_n_82),
        .O(preset__83_carry__4_i_1_n_0));
  LUT2 #(
    .INIT(4'h9)) 
    preset__83_carry__4_i_2
       (.I0(preset0_n_86),
        .I1(preset0_n_83),
        .O(preset__83_carry__4_i_2_n_0));
  LUT2 #(
    .INIT(4'h9)) 
    preset__83_carry__4_i_3
       (.I0(preset0_n_87),
        .I1(preset0_n_84),
        .O(preset__83_carry__4_i_3_n_0));
  LUT2 #(
    .INIT(4'h9)) 
    preset__83_carry__4_i_4
       (.I0(preset0_n_88),
        .I1(preset0_n_85),
        .O(preset__83_carry__4_i_4_n_0));
  CARRY4 preset__83_carry__5
       (.CI(preset__83_carry__4_n_0),
        .CO({preset__83_carry__5_n_0,preset__83_carry__5_n_1,preset__83_carry__5_n_2,preset__83_carry__5_n_3}),
        .CYINIT(1'b0),
        .DI({preset0_n_81,preset0_n_82,preset0_n_83,preset0_n_84}),
        .O({preset__83_carry__5_n_4,preset__83_carry__5_n_5,preset__83_carry__5_n_6,preset__83_carry__5_n_7}),
        .S({preset__83_carry__5_i_1_n_0,preset__83_carry__5_i_2_n_0,preset__83_carry__5_i_3_n_0,preset__83_carry__5_i_4_n_0}));
  LUT2 #(
    .INIT(4'h9)) 
    preset__83_carry__5_i_1
       (.I0(preset0_n_81),
        .I1(preset0_n_78),
        .O(preset__83_carry__5_i_1_n_0));
  LUT2 #(
    .INIT(4'h9)) 
    preset__83_carry__5_i_2
       (.I0(preset0_n_82),
        .I1(preset0_n_79),
        .O(preset__83_carry__5_i_2_n_0));
  LUT2 #(
    .INIT(4'h9)) 
    preset__83_carry__5_i_3
       (.I0(preset0_n_83),
        .I1(preset0_n_80),
        .O(preset__83_carry__5_i_3_n_0));
  LUT2 #(
    .INIT(4'h9)) 
    preset__83_carry__5_i_4
       (.I0(preset0_n_84),
        .I1(preset0_n_81),
        .O(preset__83_carry__5_i_4_n_0));
  CARRY4 preset__83_carry__6
       (.CI(preset__83_carry__5_n_0),
        .CO({preset__83_carry__6_n_0,NLW_preset__83_carry__6_CO_UNCONNECTED[2],preset__83_carry__6_n_2,preset__83_carry__6_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,preset0_n_78,preset0_n_79,preset0_n_80}),
        .O({NLW_preset__83_carry__6_O_UNCONNECTED[3],preset__83_carry__6_n_5,preset__83_carry__6_n_6,preset__83_carry__6_n_7}),
        .S({1'b1,preset__83_carry__6_i_1_n_0,preset__83_carry__6_i_2_n_0,preset__83_carry__6_i_3_n_0}));
  LUT1 #(
    .INIT(2'h1)) 
    preset__83_carry__6_i_1
       (.I0(preset0_n_78),
        .O(preset__83_carry__6_i_1_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    preset__83_carry__6_i_2
       (.I0(preset0_n_79),
        .O(preset__83_carry__6_i_2_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    preset__83_carry__6_i_3
       (.I0(preset0_n_80),
        .O(preset__83_carry__6_i_3_n_0));
  LUT2 #(
    .INIT(4'h9)) 
    preset__83_carry_i_1
       (.I0(preset0_n_105),
        .I1(preset0_n_102),
        .O(preset__83_carry_i_1_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    preset__83_carry_i_2
       (.I0(preset0_n_103),
        .O(preset__83_carry_i_2_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    preset__83_carry_i_3
       (.I0(preset0_n_104),
        .O(preset__83_carry_i_3_n_0));
  CARRY4 preset_carry
       (.CI(1'b0),
        .CO({preset_carry_n_0,preset_carry_n_1,preset_carry_n_2,preset_carry_n_3}),
        .CYINIT(1'b0),
        .DI({preset0_n_105,1'b0,1'b0,1'b1}),
        .O({preset_carry_n_4,NLW_preset_carry_O_UNCONNECTED[2:1],preset_carry_n_7}),
        .S({preset_carry_i_1_n_0,preset_carry_i_2_n_0,preset_carry_i_3_n_0,preset0_n_105}));
  CARRY4 preset_carry__0
       (.CI(preset_carry_n_0),
        .CO({preset_carry__0_n_0,preset_carry__0_n_1,preset_carry__0_n_2,preset_carry__0_n_3}),
        .CYINIT(1'b0),
        .DI({preset0_n_101,preset0_n_102,preset0_n_103,preset0_n_104}),
        .O({preset_carry__0_n_4,preset_carry__0_n_5,preset_carry__0_n_6,preset_carry__0_n_7}),
        .S({preset_carry__0_i_1_n_0,preset_carry__0_i_2_n_0,preset_carry__0_i_3_n_0,preset_carry__0_i_4_n_0}));
  LUT2 #(
    .INIT(4'h9)) 
    preset_carry__0_i_1
       (.I0(preset0_n_101),
        .I1(preset0_n_98),
        .O(preset_carry__0_i_1_n_0));
  LUT2 #(
    .INIT(4'h9)) 
    preset_carry__0_i_2
       (.I0(preset0_n_102),
        .I1(preset0_n_99),
        .O(preset_carry__0_i_2_n_0));
  LUT2 #(
    .INIT(4'h9)) 
    preset_carry__0_i_3
       (.I0(preset0_n_103),
        .I1(preset0_n_100),
        .O(preset_carry__0_i_3_n_0));
  LUT2 #(
    .INIT(4'h9)) 
    preset_carry__0_i_4
       (.I0(preset0_n_104),
        .I1(preset0_n_101),
        .O(preset_carry__0_i_4_n_0));
  CARRY4 preset_carry__1
       (.CI(preset_carry__0_n_0),
        .CO({preset_carry__1_n_0,preset_carry__1_n_1,preset_carry__1_n_2,preset_carry__1_n_3}),
        .CYINIT(1'b0),
        .DI({preset0_n_97,preset0_n_98,preset0_n_99,preset0_n_100}),
        .O({preset_carry__1_n_4,preset_carry__1_n_5,preset_carry__1_n_6,preset_carry__1_n_7}),
        .S({preset_carry__1_i_1_n_0,preset_carry__1_i_2_n_0,preset_carry__1_i_3_n_0,preset_carry__1_i_4_n_0}));
  LUT2 #(
    .INIT(4'h9)) 
    preset_carry__1_i_1
       (.I0(preset0_n_97),
        .I1(preset0_n_94),
        .O(preset_carry__1_i_1_n_0));
  LUT2 #(
    .INIT(4'h9)) 
    preset_carry__1_i_2
       (.I0(preset0_n_98),
        .I1(preset0_n_95),
        .O(preset_carry__1_i_2_n_0));
  LUT2 #(
    .INIT(4'h9)) 
    preset_carry__1_i_3
       (.I0(preset0_n_99),
        .I1(preset0_n_96),
        .O(preset_carry__1_i_3_n_0));
  LUT2 #(
    .INIT(4'h9)) 
    preset_carry__1_i_4
       (.I0(preset0_n_100),
        .I1(preset0_n_97),
        .O(preset_carry__1_i_4_n_0));
  CARRY4 preset_carry__2
       (.CI(preset_carry__1_n_0),
        .CO({preset_carry__2_n_0,preset_carry__2_n_1,preset_carry__2_n_2,preset_carry__2_n_3}),
        .CYINIT(1'b0),
        .DI({preset0_n_93,preset0_n_94,preset0_n_95,preset0_n_96}),
        .O({preset_carry__2_n_4,preset_carry__2_n_5,preset_carry__2_n_6,preset_carry__2_n_7}),
        .S({preset_carry__2_i_1_n_0,preset_carry__2_i_2_n_0,preset_carry__2_i_3_n_0,preset_carry__2_i_4_n_0}));
  LUT2 #(
    .INIT(4'h9)) 
    preset_carry__2_i_1
       (.I0(preset0_n_93),
        .I1(preset0_n_90),
        .O(preset_carry__2_i_1_n_0));
  LUT2 #(
    .INIT(4'h9)) 
    preset_carry__2_i_2
       (.I0(preset0_n_94),
        .I1(preset0_n_91),
        .O(preset_carry__2_i_2_n_0));
  LUT2 #(
    .INIT(4'h9)) 
    preset_carry__2_i_3
       (.I0(preset0_n_95),
        .I1(preset0_n_92),
        .O(preset_carry__2_i_3_n_0));
  LUT2 #(
    .INIT(4'h9)) 
    preset_carry__2_i_4
       (.I0(preset0_n_96),
        .I1(preset0_n_93),
        .O(preset_carry__2_i_4_n_0));
  CARRY4 preset_carry__3
       (.CI(preset_carry__2_n_0),
        .CO({preset_carry__3_n_0,preset_carry__3_n_1,preset_carry__3_n_2,preset_carry__3_n_3}),
        .CYINIT(1'b0),
        .DI({preset0_n_89,preset0_n_90,preset0_n_91,preset0_n_92}),
        .O({preset_carry__3_n_4,preset_carry__3_n_5,preset_carry__3_n_6,preset_carry__3_n_7}),
        .S({preset_carry__3_i_1_n_0,preset_carry__3_i_2_n_0,preset_carry__3_i_3_n_0,preset_carry__3_i_4_n_0}));
  LUT2 #(
    .INIT(4'h9)) 
    preset_carry__3_i_1
       (.I0(preset0_n_89),
        .I1(preset0_n_86),
        .O(preset_carry__3_i_1_n_0));
  LUT2 #(
    .INIT(4'h9)) 
    preset_carry__3_i_2
       (.I0(preset0_n_90),
        .I1(preset0_n_87),
        .O(preset_carry__3_i_2_n_0));
  LUT2 #(
    .INIT(4'h9)) 
    preset_carry__3_i_3
       (.I0(preset0_n_91),
        .I1(preset0_n_88),
        .O(preset_carry__3_i_3_n_0));
  LUT2 #(
    .INIT(4'h9)) 
    preset_carry__3_i_4
       (.I0(preset0_n_92),
        .I1(preset0_n_89),
        .O(preset_carry__3_i_4_n_0));
  CARRY4 preset_carry__4
       (.CI(preset_carry__3_n_0),
        .CO({preset_carry__4_n_0,preset_carry__4_n_1,preset_carry__4_n_2,preset_carry__4_n_3}),
        .CYINIT(1'b0),
        .DI({preset0_n_85,preset0_n_86,preset0_n_87,preset0_n_88}),
        .O({preset_carry__4_n_4,preset_carry__4_n_5,preset_carry__4_n_6,preset_carry__4_n_7}),
        .S({preset_carry__4_i_1_n_0,preset_carry__4_i_2_n_0,preset_carry__4_i_3_n_0,preset_carry__4_i_4_n_0}));
  LUT2 #(
    .INIT(4'h9)) 
    preset_carry__4_i_1
       (.I0(preset0_n_85),
        .I1(preset0_n_82),
        .O(preset_carry__4_i_1_n_0));
  LUT2 #(
    .INIT(4'h9)) 
    preset_carry__4_i_2
       (.I0(preset0_n_86),
        .I1(preset0_n_83),
        .O(preset_carry__4_i_2_n_0));
  LUT2 #(
    .INIT(4'h9)) 
    preset_carry__4_i_3
       (.I0(preset0_n_87),
        .I1(preset0_n_84),
        .O(preset_carry__4_i_3_n_0));
  LUT2 #(
    .INIT(4'h9)) 
    preset_carry__4_i_4
       (.I0(preset0_n_88),
        .I1(preset0_n_85),
        .O(preset_carry__4_i_4_n_0));
  CARRY4 preset_carry__5
       (.CI(preset_carry__4_n_0),
        .CO({preset_carry__5_n_0,preset_carry__5_n_1,preset_carry__5_n_2,preset_carry__5_n_3}),
        .CYINIT(1'b0),
        .DI({preset0_n_81,preset0_n_82,preset0_n_83,preset0_n_84}),
        .O({preset_carry__5_n_4,preset_carry__5_n_5,preset_carry__5_n_6,preset_carry__5_n_7}),
        .S({preset_carry__5_i_1_n_0,preset_carry__5_i_2_n_0,preset_carry__5_i_3_n_0,preset_carry__5_i_4_n_0}));
  LUT2 #(
    .INIT(4'h9)) 
    preset_carry__5_i_1
       (.I0(preset0_n_81),
        .I1(preset0_n_78),
        .O(preset_carry__5_i_1_n_0));
  LUT2 #(
    .INIT(4'h9)) 
    preset_carry__5_i_2
       (.I0(preset0_n_82),
        .I1(preset0_n_79),
        .O(preset_carry__5_i_2_n_0));
  LUT2 #(
    .INIT(4'h9)) 
    preset_carry__5_i_3
       (.I0(preset0_n_83),
        .I1(preset0_n_80),
        .O(preset_carry__5_i_3_n_0));
  LUT2 #(
    .INIT(4'h9)) 
    preset_carry__5_i_4
       (.I0(preset0_n_84),
        .I1(preset0_n_81),
        .O(preset_carry__5_i_4_n_0));
  CARRY4 preset_carry__6
       (.CI(preset_carry__5_n_0),
        .CO({preset_carry__6_n_0,NLW_preset_carry__6_CO_UNCONNECTED[2],preset_carry__6_n_2,preset_carry__6_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,preset0_n_78,preset0_n_79,preset0_n_80}),
        .O({NLW_preset_carry__6_O_UNCONNECTED[3],preset_carry__6_n_5,preset_carry__6_n_6,preset_carry__6_n_7}),
        .S({1'b1,preset_carry__6_i_1_n_0,preset_carry__6_i_2_n_0,preset_carry__6_i_3_n_0}));
  LUT1 #(
    .INIT(2'h1)) 
    preset_carry__6_i_1
       (.I0(preset0_n_78),
        .O(preset_carry__6_i_1_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    preset_carry__6_i_2
       (.I0(preset0_n_79),
        .O(preset_carry__6_i_2_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    preset_carry__6_i_3
       (.I0(preset0_n_80),
        .O(preset_carry__6_i_3_n_0));
  LUT2 #(
    .INIT(4'h9)) 
    preset_carry_i_1
       (.I0(preset0_n_105),
        .I1(preset0_n_102),
        .O(preset_carry_i_1_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    preset_carry_i_2
       (.I0(preset0_n_103),
        .O(preset_carry_i_2_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    preset_carry_i_3
       (.I0(preset0_n_104),
        .O(preset_carry_i_3_n_0));
  CARRY4 pwmaux0_carry
       (.CI(1'b0),
        .CO({pwmaux0_carry_n_0,pwmaux0_carry_n_1,pwmaux0_carry_n_2,pwmaux0_carry_n_3}),
        .CYINIT(1'b1),
        .DI({pwmaux0_carry_i_1_n_0,pwmaux0_carry_i_2_n_0,pwmaux0_carry_i_3_n_0,pwmaux0_carry_i_4_n_0}),
        .O(NLW_pwmaux0_carry_O_UNCONNECTED[3:0]),
        .S({pwmaux0_carry_i_5_n_0,pwmaux0_carry_i_6_n_0,pwmaux0_carry_i_7_n_0,pwmaux0_carry_i_8_n_0}));
  CARRY4 pwmaux0_carry__0
       (.CI(pwmaux0_carry_n_0),
        .CO({pwmaux0_carry__0_n_0,pwmaux0_carry__0_n_1,pwmaux0_carry__0_n_2,pwmaux0_carry__0_n_3}),
        .CYINIT(1'b0),
        .DI({pwmaux0_carry__0_i_1_n_0,pwmaux0_carry__0_i_2_n_0,pwmaux0_carry__0_i_3_n_0,pwmaux0_carry__0_i_4_n_0}),
        .O(NLW_pwmaux0_carry__0_O_UNCONNECTED[3:0]),
        .S({pwmaux0_carry__0_i_5_n_0,pwmaux0_carry__0_i_6_n_0,pwmaux0_carry__0_i_7_n_0,pwmaux0_carry__0_i_8_n_0}));
  LUT6 #(
    .INIT(64'h2F022F2F2F020202)) 
    pwmaux0_carry__0_i_1
       (.I0(preset[14]),
        .I1(count[14]),
        .I2(count[15]),
        .I3(preset__268_carry__8_n_7),
        .I4(pwmaux0_carry_i_10_n_0),
        .I5(preset__483_carry__2_n_4),
        .O(pwmaux0_carry__0_i_1_n_0));
  LUT5 #(
    .INIT(32'hAAEFAA20)) 
    pwmaux0_carry__0_i_10
       (.I0(preset__268_carry__7_n_6),
        .I1(preset0_n_78),
        .I2(preset__371_carry__4_n_6),
        .I3(preset__430_carry__5_n_1),
        .I4(preset__483_carry__2_n_7),
        .O(preset[12]));
  LUT5 #(
    .INIT(32'hAAEFAA20)) 
    pwmaux0_carry__0_i_11
       (.I0(preset__268_carry__6_n_4),
        .I1(preset0_n_78),
        .I2(preset__371_carry__4_n_6),
        .I3(preset__430_carry__5_n_1),
        .I4(preset__483_carry__1_n_5),
        .O(preset[10]));
  LUT5 #(
    .INIT(32'hAAEFAA20)) 
    pwmaux0_carry__0_i_12
       (.I0(preset__268_carry__6_n_6),
        .I1(preset0_n_78),
        .I2(preset__371_carry__4_n_6),
        .I3(preset__430_carry__5_n_1),
        .I4(preset__483_carry__1_n_7),
        .O(preset[8]));
  LUT6 #(
    .INIT(64'hA9A9AAA959595559)) 
    pwmaux0_carry__0_i_13
       (.I0(count[15]),
        .I1(preset__483_carry__2_n_4),
        .I2(preset__430_carry__5_n_1),
        .I3(preset__371_carry__4_n_6),
        .I4(preset0_n_78),
        .I5(preset__268_carry__8_n_7),
        .O(pwmaux0_carry__0_i_13_n_0));
  LUT6 #(
    .INIT(64'hA9A9AAA959595559)) 
    pwmaux0_carry__0_i_14
       (.I0(count[13]),
        .I1(preset__483_carry__2_n_6),
        .I2(preset__430_carry__5_n_1),
        .I3(preset__371_carry__4_n_6),
        .I4(preset0_n_78),
        .I5(preset__268_carry__7_n_5),
        .O(pwmaux0_carry__0_i_14_n_0));
  LUT6 #(
    .INIT(64'hA9A9AAA959595559)) 
    pwmaux0_carry__0_i_15
       (.I0(count[11]),
        .I1(preset__483_carry__1_n_4),
        .I2(preset__430_carry__5_n_1),
        .I3(preset__371_carry__4_n_6),
        .I4(preset0_n_78),
        .I5(preset__268_carry__7_n_7),
        .O(pwmaux0_carry__0_i_15_n_0));
  LUT6 #(
    .INIT(64'hA9A9AAA959595559)) 
    pwmaux0_carry__0_i_16
       (.I0(count[9]),
        .I1(preset__483_carry__1_n_6),
        .I2(preset__430_carry__5_n_1),
        .I3(preset__371_carry__4_n_6),
        .I4(preset0_n_78),
        .I5(preset__268_carry__6_n_5),
        .O(pwmaux0_carry__0_i_16_n_0));
  LUT6 #(
    .INIT(64'h2F022F2F2F020202)) 
    pwmaux0_carry__0_i_2
       (.I0(preset[12]),
        .I1(count[12]),
        .I2(count[13]),
        .I3(preset__268_carry__7_n_5),
        .I4(pwmaux0_carry_i_10_n_0),
        .I5(preset__483_carry__2_n_6),
        .O(pwmaux0_carry__0_i_2_n_0));
  LUT6 #(
    .INIT(64'h2F022F2F2F020202)) 
    pwmaux0_carry__0_i_3
       (.I0(preset[10]),
        .I1(count[10]),
        .I2(count[11]),
        .I3(preset__268_carry__7_n_7),
        .I4(pwmaux0_carry_i_10_n_0),
        .I5(preset__483_carry__1_n_4),
        .O(pwmaux0_carry__0_i_3_n_0));
  LUT6 #(
    .INIT(64'h2F022F2F2F020202)) 
    pwmaux0_carry__0_i_4
       (.I0(preset[8]),
        .I1(count[8]),
        .I2(count[9]),
        .I3(preset__268_carry__6_n_5),
        .I4(pwmaux0_carry_i_10_n_0),
        .I5(preset__483_carry__1_n_6),
        .O(pwmaux0_carry__0_i_4_n_0));
  LUT5 #(
    .INIT(32'hA80802A2)) 
    pwmaux0_carry__0_i_5
       (.I0(pwmaux0_carry__0_i_13_n_0),
        .I1(preset__483_carry__2_n_5),
        .I2(pwmaux0_carry_i_10_n_0),
        .I3(preset__268_carry__7_n_4),
        .I4(count[14]),
        .O(pwmaux0_carry__0_i_5_n_0));
  LUT5 #(
    .INIT(32'hA80802A2)) 
    pwmaux0_carry__0_i_6
       (.I0(pwmaux0_carry__0_i_14_n_0),
        .I1(preset__483_carry__2_n_7),
        .I2(pwmaux0_carry_i_10_n_0),
        .I3(preset__268_carry__7_n_6),
        .I4(count[12]),
        .O(pwmaux0_carry__0_i_6_n_0));
  LUT5 #(
    .INIT(32'hA80802A2)) 
    pwmaux0_carry__0_i_7
       (.I0(pwmaux0_carry__0_i_15_n_0),
        .I1(preset__483_carry__1_n_5),
        .I2(pwmaux0_carry_i_10_n_0),
        .I3(preset__268_carry__6_n_4),
        .I4(count[10]),
        .O(pwmaux0_carry__0_i_7_n_0));
  LUT5 #(
    .INIT(32'hA80802A2)) 
    pwmaux0_carry__0_i_8
       (.I0(pwmaux0_carry__0_i_16_n_0),
        .I1(preset__483_carry__1_n_7),
        .I2(pwmaux0_carry_i_10_n_0),
        .I3(preset__268_carry__6_n_6),
        .I4(count[8]),
        .O(pwmaux0_carry__0_i_8_n_0));
  LUT5 #(
    .INIT(32'hAAEFAA20)) 
    pwmaux0_carry__0_i_9
       (.I0(preset__268_carry__7_n_4),
        .I1(preset0_n_78),
        .I2(preset__371_carry__4_n_6),
        .I3(preset__430_carry__5_n_1),
        .I4(preset__483_carry__2_n_5),
        .O(preset[14]));
  CARRY4 pwmaux0_carry__1
       (.CI(pwmaux0_carry__0_n_0),
        .CO({p_0_in,pwmaux0_carry__1_n_1,pwmaux0_carry__1_n_2,pwmaux0_carry__1_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,pwmaux0_carry__1_i_1_n_0,pwmaux0_carry__1_i_2_n_0,pwmaux0_carry__1_i_3_n_0}),
        .O(NLW_pwmaux0_carry__1_O_UNCONNECTED[3:0]),
        .S({pwmaux0_carry__1_i_4_n_0,pwmaux0_carry__1_i_5_n_0,pwmaux0_carry__1_i_6_n_0,pwmaux0_carry__1_i_7_n_0}));
  LUT5 #(
    .INIT(32'h000000E2)) 
    pwmaux0_carry__1_i_1
       (.I0(preset__483_carry__4_n_7),
        .I1(pwmaux0_carry_i_10_n_0),
        .I2(preset__268_carry__9_n_6),
        .I3(count[20]),
        .I4(count[21]),
        .O(pwmaux0_carry__1_i_1_n_0));
  LUT6 #(
    .INIT(64'hA9A9AAA959595559)) 
    pwmaux0_carry__1_i_10
       (.I0(count[19]),
        .I1(preset__483_carry__3_n_4),
        .I2(preset__430_carry__5_n_1),
        .I3(preset__371_carry__4_n_6),
        .I4(preset0_n_78),
        .I5(preset__268_carry__9_n_7),
        .O(pwmaux0_carry__1_i_10_n_0));
  LUT6 #(
    .INIT(64'hA9A9AAA959595559)) 
    pwmaux0_carry__1_i_11
       (.I0(count[17]),
        .I1(preset__483_carry__3_n_6),
        .I2(preset__430_carry__5_n_1),
        .I3(preset__371_carry__4_n_6),
        .I4(preset0_n_78),
        .I5(preset__268_carry__8_n_5),
        .O(pwmaux0_carry__1_i_11_n_0));
  LUT6 #(
    .INIT(64'h2F022F2F2F020202)) 
    pwmaux0_carry__1_i_2
       (.I0(preset[18]),
        .I1(count[18]),
        .I2(count[19]),
        .I3(preset__268_carry__9_n_7),
        .I4(pwmaux0_carry_i_10_n_0),
        .I5(preset__483_carry__3_n_4),
        .O(pwmaux0_carry__1_i_2_n_0));
  LUT6 #(
    .INIT(64'h2F022F2F2F020202)) 
    pwmaux0_carry__1_i_3
       (.I0(preset[16]),
        .I1(count[16]),
        .I2(count[17]),
        .I3(preset__268_carry__8_n_5),
        .I4(pwmaux0_carry_i_10_n_0),
        .I5(preset__483_carry__3_n_6),
        .O(pwmaux0_carry__1_i_3_n_0));
  LUT2 #(
    .INIT(4'h1)) 
    pwmaux0_carry__1_i_4
       (.I0(count[23]),
        .I1(count[22]),
        .O(pwmaux0_carry__1_i_4_n_0));
  LUT5 #(
    .INIT(32'h00B80047)) 
    pwmaux0_carry__1_i_5
       (.I0(preset__268_carry__9_n_6),
        .I1(pwmaux0_carry_i_10_n_0),
        .I2(preset__483_carry__4_n_7),
        .I3(count[21]),
        .I4(count[20]),
        .O(pwmaux0_carry__1_i_5_n_0));
  LUT5 #(
    .INIT(32'hA80802A2)) 
    pwmaux0_carry__1_i_6
       (.I0(pwmaux0_carry__1_i_10_n_0),
        .I1(preset__483_carry__3_n_5),
        .I2(pwmaux0_carry_i_10_n_0),
        .I3(preset__268_carry__8_n_4),
        .I4(count[18]),
        .O(pwmaux0_carry__1_i_6_n_0));
  LUT5 #(
    .INIT(32'hA80802A2)) 
    pwmaux0_carry__1_i_7
       (.I0(pwmaux0_carry__1_i_11_n_0),
        .I1(preset__483_carry__3_n_7),
        .I2(pwmaux0_carry_i_10_n_0),
        .I3(preset__268_carry__8_n_6),
        .I4(count[16]),
        .O(pwmaux0_carry__1_i_7_n_0));
  LUT5 #(
    .INIT(32'hAAEFAA20)) 
    pwmaux0_carry__1_i_8
       (.I0(preset__268_carry__8_n_4),
        .I1(preset0_n_78),
        .I2(preset__371_carry__4_n_6),
        .I3(preset__430_carry__5_n_1),
        .I4(preset__483_carry__3_n_5),
        .O(preset[18]));
  LUT5 #(
    .INIT(32'hAAEFAA20)) 
    pwmaux0_carry__1_i_9
       (.I0(preset__268_carry__8_n_6),
        .I1(preset0_n_78),
        .I2(preset__371_carry__4_n_6),
        .I3(preset__430_carry__5_n_1),
        .I4(preset__483_carry__3_n_7),
        .O(preset[16]));
  LUT6 #(
    .INIT(64'h2F022F2F2F020202)) 
    pwmaux0_carry_i_1
       (.I0(preset[6]),
        .I1(count[6]),
        .I2(count[7]),
        .I3(preset__268_carry__6_n_7),
        .I4(pwmaux0_carry_i_10_n_0),
        .I5(preset__483_carry__0_n_4),
        .O(pwmaux0_carry_i_1_n_0));
  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT3 #(
    .INIT(8'hF4)) 
    pwmaux0_carry_i_10
       (.I0(preset0_n_78),
        .I1(preset__371_carry__4_n_6),
        .I2(preset__430_carry__5_n_1),
        .O(pwmaux0_carry_i_10_n_0));
  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT5 #(
    .INIT(32'hAAEFAA20)) 
    pwmaux0_carry_i_11
       (.I0(preset__268_carry__5_n_6),
        .I1(preset0_n_78),
        .I2(preset__371_carry__4_n_6),
        .I3(preset__430_carry__5_n_1),
        .I4(preset__483_carry__0_n_7),
        .O(preset[4]));
  LUT5 #(
    .INIT(32'hAAEFAA20)) 
    pwmaux0_carry_i_12
       (.I0(preset__268_carry__4_n_4),
        .I1(preset0_n_78),
        .I2(preset__371_carry__4_n_6),
        .I3(preset__430_carry__5_n_1),
        .I4(preset__483_carry_n_5),
        .O(preset[2]));
  LUT5 #(
    .INIT(32'hAAEFAA20)) 
    pwmaux0_carry_i_13
       (.I0(preset__268_carry__4_n_6),
        .I1(preset0_n_78),
        .I2(preset__371_carry__4_n_6),
        .I3(preset__430_carry__5_n_1),
        .I4(preset__483_carry_n_7),
        .O(preset[0]));
  LUT6 #(
    .INIT(64'hA9A9AAA959595559)) 
    pwmaux0_carry_i_14
       (.I0(count[7]),
        .I1(preset__483_carry__0_n_4),
        .I2(preset__430_carry__5_n_1),
        .I3(preset__371_carry__4_n_6),
        .I4(preset0_n_78),
        .I5(preset__268_carry__6_n_7),
        .O(pwmaux0_carry_i_14_n_0));
  LUT6 #(
    .INIT(64'hA9A9AAA959595559)) 
    pwmaux0_carry_i_15
       (.I0(count[5]),
        .I1(preset__483_carry__0_n_6),
        .I2(preset__430_carry__5_n_1),
        .I3(preset__371_carry__4_n_6),
        .I4(preset0_n_78),
        .I5(preset__268_carry__5_n_5),
        .O(pwmaux0_carry_i_15_n_0));
  LUT6 #(
    .INIT(64'hA9A9AAA959595559)) 
    pwmaux0_carry_i_16
       (.I0(count[3]),
        .I1(preset__483_carry_n_4),
        .I2(preset__430_carry__5_n_1),
        .I3(preset__371_carry__4_n_6),
        .I4(preset0_n_78),
        .I5(preset__268_carry__5_n_7),
        .O(pwmaux0_carry_i_16_n_0));
  LUT6 #(
    .INIT(64'hA9A9AAA959595559)) 
    pwmaux0_carry_i_17
       (.I0(count[1]),
        .I1(preset__483_carry_n_6),
        .I2(preset__430_carry__5_n_1),
        .I3(preset__371_carry__4_n_6),
        .I4(preset0_n_78),
        .I5(preset__268_carry__4_n_5),
        .O(pwmaux0_carry_i_17_n_0));
  LUT6 #(
    .INIT(64'h2F022F2F2F020202)) 
    pwmaux0_carry_i_2
       (.I0(preset[4]),
        .I1(count[4]),
        .I2(count[5]),
        .I3(preset__268_carry__5_n_5),
        .I4(pwmaux0_carry_i_10_n_0),
        .I5(preset__483_carry__0_n_6),
        .O(pwmaux0_carry_i_2_n_0));
  LUT6 #(
    .INIT(64'h2F022F2F2F020202)) 
    pwmaux0_carry_i_3
       (.I0(preset[2]),
        .I1(count[2]),
        .I2(count[3]),
        .I3(preset__268_carry__5_n_7),
        .I4(pwmaux0_carry_i_10_n_0),
        .I5(preset__483_carry_n_4),
        .O(pwmaux0_carry_i_3_n_0));
  LUT6 #(
    .INIT(64'h2F022F2F2F020202)) 
    pwmaux0_carry_i_4
       (.I0(preset[0]),
        .I1(count[0]),
        .I2(count[1]),
        .I3(preset__268_carry__4_n_5),
        .I4(pwmaux0_carry_i_10_n_0),
        .I5(preset__483_carry_n_6),
        .O(pwmaux0_carry_i_4_n_0));
  LUT5 #(
    .INIT(32'hA80802A2)) 
    pwmaux0_carry_i_5
       (.I0(pwmaux0_carry_i_14_n_0),
        .I1(preset__483_carry__0_n_5),
        .I2(pwmaux0_carry_i_10_n_0),
        .I3(preset__268_carry__5_n_4),
        .I4(count[6]),
        .O(pwmaux0_carry_i_5_n_0));
  LUT5 #(
    .INIT(32'hA80802A2)) 
    pwmaux0_carry_i_6
       (.I0(pwmaux0_carry_i_15_n_0),
        .I1(preset__483_carry__0_n_7),
        .I2(pwmaux0_carry_i_10_n_0),
        .I3(preset__268_carry__5_n_6),
        .I4(count[4]),
        .O(pwmaux0_carry_i_6_n_0));
  LUT5 #(
    .INIT(32'hA80802A2)) 
    pwmaux0_carry_i_7
       (.I0(pwmaux0_carry_i_16_n_0),
        .I1(preset__483_carry_n_5),
        .I2(pwmaux0_carry_i_10_n_0),
        .I3(preset__268_carry__4_n_4),
        .I4(count[2]),
        .O(pwmaux0_carry_i_7_n_0));
  LUT5 #(
    .INIT(32'hA80802A2)) 
    pwmaux0_carry_i_8
       (.I0(pwmaux0_carry_i_17_n_0),
        .I1(preset__483_carry_n_7),
        .I2(pwmaux0_carry_i_10_n_0),
        .I3(preset__268_carry__4_n_6),
        .I4(count[0]),
        .O(pwmaux0_carry_i_8_n_0));
  LUT5 #(
    .INIT(32'hAAEFAA20)) 
    pwmaux0_carry_i_9
       (.I0(preset__268_carry__5_n_4),
        .I1(preset0_n_78),
        .I2(preset__371_carry__4_n_6),
        .I3(preset__430_carry__5_n_1),
        .I4(preset__483_carry__0_n_5),
        .O(preset[6]));
  (* SOFT_HLUTNM = "soft_lutpair29" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    pwmaux_i_1
       (.I0(p_0_in),
        .I1(pwmaux),
        .I2(pwm),
        .O(pwmaux_i_1_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    pwmaux_i_2
       (.I0(reset),
        .O(resetaux));
  LUT5 #(
    .INIT(32'hFFFFFFBF)) 
    pwmaux_i_3
       (.I0(pwmaux_i_4_n_0),
        .I1(count[12]),
        .I2(count[13]),
        .I3(pwmaux_i_5_n_0),
        .I4(pwmaux_i_6_n_0),
        .O(pwmaux));
  LUT5 #(
    .INIT(32'hFFFFFFFE)) 
    pwmaux_i_4
       (.I0(pwmaux_i_7_n_0),
        .I1(count[14]),
        .I2(count[18]),
        .I3(count[5]),
        .I4(count[4]),
        .O(pwmaux_i_4_n_0));
  LUT5 #(
    .INIT(32'hFFFF7FFF)) 
    pwmaux_i_5
       (.I0(count[19]),
        .I1(count[6]),
        .I2(count[17]),
        .I3(count[16]),
        .I4(pwmaux_i_8_n_0),
        .O(pwmaux_i_5_n_0));
  LUT4 #(
    .INIT(16'hFFEF)) 
    pwmaux_i_6
       (.I0(count[8]),
        .I1(count[9]),
        .I2(count[11]),
        .I3(count[22]),
        .O(pwmaux_i_6_n_0));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFBF)) 
    pwmaux_i_7
       (.I0(count[0]),
        .I1(count[15]),
        .I2(count[7]),
        .I3(count[21]),
        .I4(count[20]),
        .I5(count[23]),
        .O(pwmaux_i_7_n_0));
  LUT4 #(
    .INIT(16'h7FFF)) 
    pwmaux_i_8
       (.I0(count[3]),
        .I1(count[2]),
        .I2(count[1]),
        .I3(count[10]),
        .O(pwmaux_i_8_n_0));
  FDCE #(
    .INIT(1'b0)) 
    pwmaux_reg
       (.C(clk),
        .CE(1'b1),
        .CLR(resetaux),
        .D(pwmaux_i_1_n_0),
        .Q(pwm));
endmodule

(* CHECK_LICENSE_TYPE = "semana3_PWM_V1_0_0,PWM_V1,{}" *) (* downgradeipidentifiedwarnings = "yes" *) (* ip_definition_source = "module_ref" *) 
(* x_core_info = "PWM_V1,Vivado 2018.3" *) 
(* NotValidForBitStream *)
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix
   (dc,
    clk,
    reset,
    pwm);
  input [7:0]dc;
  (* x_interface_info = "xilinx.com:signal:clock:1.0 clk CLK" *) (* x_interface_parameter = "XIL_INTERFACENAME clk, ASSOCIATED_RESET reset, FREQ_HZ 100000000, PHASE 0.000, CLK_DOMAIN semana3_processing_system7_0_0_FCLK_CLK0, INSERT_VIP 0" *) input clk;
  (* x_interface_info = "xilinx.com:signal:reset:1.0 reset RST" *) (* x_interface_parameter = "XIL_INTERFACENAME reset, POLARITY ACTIVE_LOW, INSERT_VIP 0" *) input reset;
  output pwm;

  wire clk;
  wire [7:0]dc;
  wire pwm;
  wire reset;

  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_PWM_V1 U0
       (.clk(clk),
        .dc(dc),
        .pwm(pwm),
        .reset(reset));
endmodule
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

endmodule
`endif
