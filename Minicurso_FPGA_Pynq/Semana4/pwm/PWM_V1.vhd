---------------------------------------------------------------------------------
-- Company: University of Brasilia
-- Engineer: M.Sc Mario Andrés Pastrana Triana
-- 
-- Create Date: 18.06.2021 13:31:12
-- Design Name: PWM
-- Module Name: Module PWM
-- Project Name: Maria Robot
-- Target Devices: Minized Xilinx
-- Tool Versions: 
-- Description: This module receive the duty cycle unsigned (8 bits) and
--              give PWM signal output
-- Dependencies: 
-- 
-- Revision: M.Sc Mario Andrés Pastrana Triana on 21 december of 2022
-- Revision 0.01 - File Created
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
----------------------------------------------------------------------------------
-- Module PWM
-- Input
-- dc       : Is the duty cicle
-- reset    : Reset Button
-- clk      : Clock input
-- Output
-- PWM      : PWM output
-- Generic
-- f        : Frequency
----------------------------------------------------------------------------------

entity PWM_V1 is
    Port ( 
        dc      : in STD_LOGIC_VECTOR (7 DOWNTO 0);
        clk     : in STD_LOGIC;
        reset   : in STD_LOGIC;
        pwm     : out STD_LOGIC
    );
end PWM_V1;

architecture Behavioral of PWM_V1 is
    --------------------------------------
    --Init Behavioral
    --------------------------------------
    Signal preset   : unsigned (31 downto 0);
    Signal p        : unsigned (23 downto 0):=(others=>'0');
    SIGNAL pwmaux   : STD_LOGIC:='0';
    SIGNAL resetaux : STD_LOGIC:='0';
    SIGNAL count    : unsigned (23 downto 0):=(others=>'0');
    SIGNAL f       : STD_LOGIC_VECTOR (7 DOWNTO 0):="10000010";
    
begin

    preset<=to_unsigned (((to_integer(unsigned(dc))*1000000)/(to_integer(unsigned(f)))),preset'length); -- Time in one
    p<=to_unsigned(100000000/(to_integer(unsigned(f))),p'length);                                       -- Period 
    pwm<=pwmaux;      
    resetaux <= not reset;                                                                                  -- output PWM
    --------------------------------------
    -- Process PWM
    --------------------------------------
    PROCESS(clk,resetaux,preset,p) 
    BEGIN
        if (resetaux='1') then             -- Reset PWM
            pwmaux<='0';                -- PWM zero
            count<=(others=>'0');       -- Count zero
        elsif rising_edge(clk) then     
            if  (count=p) then          -- when count = p count is zero
                count<=(others=>'0');   -- count zero
            elsif(count<=preset) then   -- count <= preset
                count<=count+1;         -- count = count + 1
                pwmaux<='1';            -- PWM in Hight
            else                        -- count >= preset
                count<=count+1;         -- count = count + 1
                pwmaux<='0';            -- PWM in Low
            end if;
        end if;
    END PROCESS;
end Behavioral;                         -- End Behavioral
