-- Copyright 1986-2018 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2018.3 (lin64) Build 2405991 Thu Dec  6 23:36:41 MST 2018
-- Date        : Thu Dec 29 21:51:07 2022
-- Host        : albano-Satellite-C655 running 64-bit Ubuntu 22.04.1 LTS
-- Command     : write_vhdl -force -mode funcsim
--               /home/albano/VideosTutoriales/PynqFPGA/Semana3/TestUDMASemana3/TestUDMASemana3.srcs/sources_1/bd/UDMA/ip/UDMA_comblock_0_0/UDMA_comblock_0_0_sim_netlist.vhdl
-- Design      : UDMA_comblock_0_0
-- Purpose     : This VHDL netlist is a functional simulation representation of the design and should not be modified or
--               synthesized. This netlist cannot be used for SDF annotated simulation.
-- Device      : xc7z020clg400-1
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity UDMA_comblock_0_0_AXIL is
  port (
    axil_awready : out STD_LOGIC;
    axil_wready : out STD_LOGIC;
    axil_arready : out STD_LOGIC;
    axil_rdata : out STD_LOGIC_VECTOR ( 0 to 0 );
    axil_bvalid : out STD_LOGIC;
    axil_rvalid : out STD_LOGIC;
    Q : out STD_LOGIC_VECTOR ( 1 downto 0 );
    axil_wdata_0_sp_1 : out STD_LOGIC;
    \axil_wdata[0]_0\ : out STD_LOGIC;
    \axil_wdata[0]_1\ : out STD_LOGIC;
    \axil_wdata[0]_2\ : out STD_LOGIC;
    axil_aclk : in STD_LOGIC;
    \axi_rdata_reg[0]_0\ : in STD_LOGIC;
    axil_aresetn : in STD_LOGIC;
    axil_bready : in STD_LOGIC;
    axil_awvalid : in STD_LOGIC;
    axil_wvalid : in STD_LOGIC;
    axil_arvalid : in STD_LOGIC;
    axil_rready : in STD_LOGIC;
    axil_wdata : in STD_LOGIC_VECTOR ( 0 to 0 );
    reg0_o : in STD_LOGIC_VECTOR ( 0 to 0 );
    reg1_o : in STD_LOGIC_VECTOR ( 0 to 0 );
    reg2_o : in STD_LOGIC_VECTOR ( 0 to 0 );
    reg3_o : in STD_LOGIC_VECTOR ( 0 to 0 );
    axil_awaddr : in STD_LOGIC_VECTOR ( 5 downto 0 );
    axil_araddr : in STD_LOGIC_VECTOR ( 5 downto 0 )
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of UDMA_comblock_0_0_AXIL : entity is "AXIL";
end UDMA_comblock_0_0_AXIL;

architecture STRUCTURE of UDMA_comblock_0_0_AXIL is
  signal aw_en_i_1_n_0 : STD_LOGIC;
  signal aw_en_reg_n_0 : STD_LOGIC;
  signal axi_arready0 : STD_LOGIC;
  signal axi_awready0 : STD_LOGIC;
  signal axi_bvalid_i_1_n_0 : STD_LOGIC;
  signal \axi_rdata[0]_i_1_n_0\ : STD_LOGIC;
  signal axi_rvalid_i_1_n_0 : STD_LOGIC;
  signal axi_wready0 : STD_LOGIC;
  signal \^axil_arready\ : STD_LOGIC;
  signal \^axil_awready\ : STD_LOGIC;
  signal \^axil_bvalid\ : STD_LOGIC;
  signal \^axil_rvalid\ : STD_LOGIC;
  signal axil_wdata_0_sn_1 : STD_LOGIC;
  signal \^axil_wready\ : STD_LOGIC;
  signal p_0_in : STD_LOGIC;
  signal reg_rd_adr : STD_LOGIC_VECTOR ( 7 downto 6 );
  signal reg_wr_adr : STD_LOGIC_VECTOR ( 7 downto 2 );
  signal \regs_out[0][0]_i_2_n_0\ : STD_LOGIC;
  signal \regs_out[0][0]_i_3_n_0\ : STD_LOGIC;
  signal \regs_out[1][0]_i_2_n_0\ : STD_LOGIC;
  signal sel0 : STD_LOGIC_VECTOR ( 3 downto 2 );
  signal slv_reg_rden : STD_LOGIC;
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \axi_rdata[0]_i_2\ : label is "soft_lutpair2";
  attribute SOFT_HLUTNM of axi_rvalid_i_1 : label is "soft_lutpair2";
  attribute SOFT_HLUTNM of axi_wready_i_1 : label is "soft_lutpair3";
  attribute SOFT_HLUTNM of \regs_out[0][0]_i_1\ : label is "soft_lutpair1";
  attribute SOFT_HLUTNM of \regs_out[0][0]_i_3\ : label is "soft_lutpair3";
  attribute SOFT_HLUTNM of \regs_out[1][0]_i_1\ : label is "soft_lutpair0";
  attribute SOFT_HLUTNM of \regs_out[2][0]_i_1\ : label is "soft_lutpair1";
  attribute SOFT_HLUTNM of \regs_out[3][0]_i_1\ : label is "soft_lutpair0";
begin
  axil_arready <= \^axil_arready\;
  axil_awready <= \^axil_awready\;
  axil_bvalid <= \^axil_bvalid\;
  axil_rvalid <= \^axil_rvalid\;
  axil_wdata_0_sp_1 <= axil_wdata_0_sn_1;
  axil_wready <= \^axil_wready\;
aw_en_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"F8F8F8F808F8F8F8"
    )
        port map (
      I0 => \^axil_bvalid\,
      I1 => axil_bready,
      I2 => aw_en_reg_n_0,
      I3 => axil_wvalid,
      I4 => axil_awvalid,
      I5 => \^axil_awready\,
      O => aw_en_i_1_n_0
    );
aw_en_reg: unisim.vcomponents.FDSE
     port map (
      C => axil_aclk,
      CE => '1',
      D => aw_en_i_1_n_0,
      Q => aw_en_reg_n_0,
      S => p_0_in
    );
\axi_araddr_reg[2]\: unisim.vcomponents.FDSE
     port map (
      C => axil_aclk,
      CE => axi_arready0,
      D => axil_araddr(0),
      Q => Q(0),
      S => p_0_in
    );
\axi_araddr_reg[3]\: unisim.vcomponents.FDSE
     port map (
      C => axil_aclk,
      CE => axi_arready0,
      D => axil_araddr(1),
      Q => Q(1),
      S => p_0_in
    );
\axi_araddr_reg[4]\: unisim.vcomponents.FDSE
     port map (
      C => axil_aclk,
      CE => axi_arready0,
      D => axil_araddr(2),
      Q => sel0(2),
      S => p_0_in
    );
\axi_araddr_reg[5]\: unisim.vcomponents.FDSE
     port map (
      C => axil_aclk,
      CE => axi_arready0,
      D => axil_araddr(3),
      Q => sel0(3),
      S => p_0_in
    );
\axi_araddr_reg[6]\: unisim.vcomponents.FDSE
     port map (
      C => axil_aclk,
      CE => axi_arready0,
      D => axil_araddr(4),
      Q => reg_rd_adr(6),
      S => p_0_in
    );
\axi_araddr_reg[7]\: unisim.vcomponents.FDSE
     port map (
      C => axil_aclk,
      CE => axi_arready0,
      D => axil_araddr(5),
      Q => reg_rd_adr(7),
      S => p_0_in
    );
axi_arready_i_1: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => axil_arvalid,
      I1 => \^axil_arready\,
      O => axi_arready0
    );
axi_arready_reg: unisim.vcomponents.FDRE
     port map (
      C => axil_aclk,
      CE => '1',
      D => axi_arready0,
      Q => \^axil_arready\,
      R => p_0_in
    );
\axi_awaddr_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => axil_aclk,
      CE => axi_awready0,
      D => axil_awaddr(0),
      Q => reg_wr_adr(2),
      R => p_0_in
    );
\axi_awaddr_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => axil_aclk,
      CE => axi_awready0,
      D => axil_awaddr(1),
      Q => reg_wr_adr(3),
      R => p_0_in
    );
\axi_awaddr_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => axil_aclk,
      CE => axi_awready0,
      D => axil_awaddr(2),
      Q => reg_wr_adr(4),
      R => p_0_in
    );
\axi_awaddr_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => axil_aclk,
      CE => axi_awready0,
      D => axil_awaddr(3),
      Q => reg_wr_adr(5),
      R => p_0_in
    );
\axi_awaddr_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => axil_aclk,
      CE => axi_awready0,
      D => axil_awaddr(4),
      Q => reg_wr_adr(6),
      R => p_0_in
    );
\axi_awaddr_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => axil_aclk,
      CE => axi_awready0,
      D => axil_awaddr(5),
      Q => reg_wr_adr(7),
      R => p_0_in
    );
axi_awready_i_1: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => axil_aresetn,
      O => p_0_in
    );
axi_awready_i_2: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0080"
    )
        port map (
      I0 => aw_en_reg_n_0,
      I1 => axil_wvalid,
      I2 => axil_awvalid,
      I3 => \^axil_awready\,
      O => axi_awready0
    );
axi_awready_reg: unisim.vcomponents.FDRE
     port map (
      C => axil_aclk,
      CE => '1',
      D => axi_awready0,
      Q => \^axil_awready\,
      R => p_0_in
    );
axi_bvalid_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"55555555C0000000"
    )
        port map (
      I0 => axil_bready,
      I1 => \^axil_wready\,
      I2 => axil_awvalid,
      I3 => axil_wvalid,
      I4 => \^axil_awready\,
      I5 => \^axil_bvalid\,
      O => axi_bvalid_i_1_n_0
    );
axi_bvalid_reg: unisim.vcomponents.FDRE
     port map (
      C => axil_aclk,
      CE => '1',
      D => axi_bvalid_i_1_n_0,
      Q => \^axil_bvalid\,
      R => p_0_in
    );
\axi_rdata[0]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000020000000000"
    )
        port map (
      I0 => slv_reg_rden,
      I1 => reg_rd_adr(7),
      I2 => sel0(3),
      I3 => \axi_rdata_reg[0]_0\,
      I4 => sel0(2),
      I5 => reg_rd_adr(6),
      O => \axi_rdata[0]_i_1_n_0\
    );
\axi_rdata[0]_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"08"
    )
        port map (
      I0 => \^axil_arready\,
      I1 => axil_arvalid,
      I2 => \^axil_rvalid\,
      O => slv_reg_rden
    );
\axi_rdata_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => axil_aclk,
      CE => '1',
      D => \axi_rdata[0]_i_1_n_0\,
      Q => axil_rdata(0),
      R => '0'
    );
axi_rvalid_i_1: unisim.vcomponents.LUT4
    generic map(
      INIT => X"08F8"
    )
        port map (
      I0 => axil_arvalid,
      I1 => \^axil_arready\,
      I2 => \^axil_rvalid\,
      I3 => axil_rready,
      O => axi_rvalid_i_1_n_0
    );
axi_rvalid_reg: unisim.vcomponents.FDRE
     port map (
      C => axil_aclk,
      CE => '1',
      D => axi_rvalid_i_1_n_0,
      Q => \^axil_rvalid\,
      R => p_0_in
    );
axi_wready_i_1: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0080"
    )
        port map (
      I0 => aw_en_reg_n_0,
      I1 => axil_wvalid,
      I2 => axil_awvalid,
      I3 => \^axil_wready\,
      O => axi_wready0
    );
axi_wready_reg: unisim.vcomponents.FDRE
     port map (
      C => axil_aclk,
      CE => '1',
      D => axi_wready0,
      Q => \^axil_wready\,
      R => p_0_in
    );
\regs_out[0][0]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FB08"
    )
        port map (
      I0 => axil_wdata(0),
      I1 => \regs_out[0][0]_i_2_n_0\,
      I2 => reg_wr_adr(3),
      I3 => reg0_o(0),
      O => axil_wdata_0_sn_1
    );
\regs_out[0][0]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000000020"
    )
        port map (
      I0 => reg_wr_adr(6),
      I1 => reg_wr_adr(5),
      I2 => \regs_out[0][0]_i_3_n_0\,
      I3 => reg_wr_adr(4),
      I4 => reg_wr_adr(2),
      I5 => reg_wr_adr(7),
      O => \regs_out[0][0]_i_2_n_0\
    );
\regs_out[0][0]_i_3\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8000"
    )
        port map (
      I0 => \^axil_awready\,
      I1 => axil_wvalid,
      I2 => axil_awvalid,
      I3 => \^axil_wready\,
      O => \regs_out[0][0]_i_3_n_0\
    );
\regs_out[1][0]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FB08"
    )
        port map (
      I0 => axil_wdata(0),
      I1 => \regs_out[1][0]_i_2_n_0\,
      I2 => reg_wr_adr(3),
      I3 => reg1_o(0),
      O => \axil_wdata[0]_0\
    );
\regs_out[1][0]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000200000"
    )
        port map (
      I0 => reg_wr_adr(6),
      I1 => reg_wr_adr(5),
      I2 => \regs_out[0][0]_i_3_n_0\,
      I3 => reg_wr_adr(4),
      I4 => reg_wr_adr(2),
      I5 => reg_wr_adr(7),
      O => \regs_out[1][0]_i_2_n_0\
    );
\regs_out[2][0]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"BF80"
    )
        port map (
      I0 => axil_wdata(0),
      I1 => \regs_out[0][0]_i_2_n_0\,
      I2 => reg_wr_adr(3),
      I3 => reg2_o(0),
      O => \axil_wdata[0]_1\
    );
\regs_out[3][0]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"BF80"
    )
        port map (
      I0 => axil_wdata(0),
      I1 => \regs_out[1][0]_i_2_n_0\,
      I2 => reg_wr_adr(3),
      I3 => reg3_o(0),
      O => \axil_wdata[0]_2\
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity UDMA_comblock_0_0_ComBlock is
  port (
    reg0_o : out STD_LOGIC_VECTOR ( 0 to 0 );
    reg1_o : out STD_LOGIC_VECTOR ( 0 to 0 );
    reg2_o : out STD_LOGIC_VECTOR ( 0 to 0 );
    reg3_o : out STD_LOGIC_VECTOR ( 0 to 0 );
    \regs_out_reg[3][0]_0\ : out STD_LOGIC;
    \regs_out_reg[0][0]_0\ : in STD_LOGIC;
    axil_aclk : in STD_LOGIC;
    \regs_out_reg[1][0]_0\ : in STD_LOGIC;
    \regs_out_reg[2][0]_0\ : in STD_LOGIC;
    \regs_out_reg[3][0]_1\ : in STD_LOGIC;
    Q : in STD_LOGIC_VECTOR ( 1 downto 0 )
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of UDMA_comblock_0_0_ComBlock : entity is "ComBlock";
end UDMA_comblock_0_0_ComBlock;

architecture STRUCTURE of UDMA_comblock_0_0_ComBlock is
  signal \^reg0_o\ : STD_LOGIC_VECTOR ( 0 to 0 );
  signal \^reg1_o\ : STD_LOGIC_VECTOR ( 0 to 0 );
  signal \^reg2_o\ : STD_LOGIC_VECTOR ( 0 to 0 );
  signal \^reg3_o\ : STD_LOGIC_VECTOR ( 0 to 0 );
begin
  reg0_o(0) <= \^reg0_o\(0);
  reg1_o(0) <= \^reg1_o\(0);
  reg2_o(0) <= \^reg2_o\(0);
  reg3_o(0) <= \^reg3_o\(0);
\axi_rdata[0]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \^reg3_o\(0),
      I1 => \^reg2_o\(0),
      I2 => Q(1),
      I3 => \^reg1_o\(0),
      I4 => Q(0),
      I5 => \^reg0_o\(0),
      O => \regs_out_reg[3][0]_0\
    );
\regs_out_reg[0][0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axil_aclk,
      CE => '1',
      D => \regs_out_reg[0][0]_0\,
      Q => \^reg0_o\(0),
      R => '0'
    );
\regs_out_reg[1][0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axil_aclk,
      CE => '1',
      D => \regs_out_reg[1][0]_0\,
      Q => \^reg1_o\(0),
      R => '0'
    );
\regs_out_reg[2][0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axil_aclk,
      CE => '1',
      D => \regs_out_reg[2][0]_0\,
      Q => \^reg2_o\(0),
      R => '0'
    );
\regs_out_reg[3][0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axil_aclk,
      CE => '1',
      D => \regs_out_reg[3][0]_1\,
      Q => \^reg3_o\(0),
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity UDMA_comblock_0_0_axi_comblock is
  port (
    axil_awready : out STD_LOGIC;
    axil_wready : out STD_LOGIC;
    axil_arready : out STD_LOGIC;
    axil_rdata : out STD_LOGIC_VECTOR ( 0 to 0 );
    axil_rvalid : out STD_LOGIC;
    reg3_o : out STD_LOGIC_VECTOR ( 0 to 0 );
    reg2_o : out STD_LOGIC_VECTOR ( 0 to 0 );
    reg1_o : out STD_LOGIC_VECTOR ( 0 to 0 );
    reg0_o : out STD_LOGIC_VECTOR ( 0 to 0 );
    axil_bvalid : out STD_LOGIC;
    axil_aclk : in STD_LOGIC;
    axil_awaddr : in STD_LOGIC_VECTOR ( 5 downto 0 );
    axil_araddr : in STD_LOGIC_VECTOR ( 5 downto 0 );
    axil_wvalid : in STD_LOGIC;
    axil_awvalid : in STD_LOGIC;
    axil_arvalid : in STD_LOGIC;
    axil_aresetn : in STD_LOGIC;
    axil_bready : in STD_LOGIC;
    axil_rready : in STD_LOGIC;
    axil_wdata : in STD_LOGIC_VECTOR ( 0 to 0 )
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of UDMA_comblock_0_0_axi_comblock : entity is "axi_comblock";
end UDMA_comblock_0_0_axi_comblock;

architecture STRUCTURE of UDMA_comblock_0_0_axi_comblock is
  signal AXIL_inst_n_10 : STD_LOGIC;
  signal AXIL_inst_n_11 : STD_LOGIC;
  signal AXIL_inst_n_8 : STD_LOGIC;
  signal AXIL_inst_n_9 : STD_LOGIC;
  signal comblock_i_n_4 : STD_LOGIC;
  signal \^reg0_o\ : STD_LOGIC_VECTOR ( 0 to 0 );
  signal \^reg1_o\ : STD_LOGIC_VECTOR ( 0 to 0 );
  signal \^reg2_o\ : STD_LOGIC_VECTOR ( 0 to 0 );
  signal \^reg3_o\ : STD_LOGIC_VECTOR ( 0 to 0 );
  signal sel0 : STD_LOGIC_VECTOR ( 1 downto 0 );
begin
  reg0_o(0) <= \^reg0_o\(0);
  reg1_o(0) <= \^reg1_o\(0);
  reg2_o(0) <= \^reg2_o\(0);
  reg3_o(0) <= \^reg3_o\(0);
AXIL_inst: entity work.UDMA_comblock_0_0_AXIL
     port map (
      Q(1 downto 0) => sel0(1 downto 0),
      \axi_rdata_reg[0]_0\ => comblock_i_n_4,
      axil_aclk => axil_aclk,
      axil_araddr(5 downto 0) => axil_araddr(5 downto 0),
      axil_aresetn => axil_aresetn,
      axil_arready => axil_arready,
      axil_arvalid => axil_arvalid,
      axil_awaddr(5 downto 0) => axil_awaddr(5 downto 0),
      axil_awready => axil_awready,
      axil_awvalid => axil_awvalid,
      axil_bready => axil_bready,
      axil_bvalid => axil_bvalid,
      axil_rdata(0) => axil_rdata(0),
      axil_rready => axil_rready,
      axil_rvalid => axil_rvalid,
      axil_wdata(0) => axil_wdata(0),
      \axil_wdata[0]_0\ => AXIL_inst_n_9,
      \axil_wdata[0]_1\ => AXIL_inst_n_10,
      \axil_wdata[0]_2\ => AXIL_inst_n_11,
      axil_wdata_0_sp_1 => AXIL_inst_n_8,
      axil_wready => axil_wready,
      axil_wvalid => axil_wvalid,
      reg0_o(0) => \^reg0_o\(0),
      reg1_o(0) => \^reg1_o\(0),
      reg2_o(0) => \^reg2_o\(0),
      reg3_o(0) => \^reg3_o\(0)
    );
comblock_i: entity work.UDMA_comblock_0_0_ComBlock
     port map (
      Q(1 downto 0) => sel0(1 downto 0),
      axil_aclk => axil_aclk,
      reg0_o(0) => \^reg0_o\(0),
      reg1_o(0) => \^reg1_o\(0),
      reg2_o(0) => \^reg2_o\(0),
      reg3_o(0) => \^reg3_o\(0),
      \regs_out_reg[0][0]_0\ => AXIL_inst_n_8,
      \regs_out_reg[1][0]_0\ => AXIL_inst_n_9,
      \regs_out_reg[2][0]_0\ => AXIL_inst_n_10,
      \regs_out_reg[3][0]_0\ => comblock_i_n_4,
      \regs_out_reg[3][0]_1\ => AXIL_inst_n_11
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity UDMA_comblock_0_0 is
  port (
    reg0_o : out STD_LOGIC_VECTOR ( 0 to 0 );
    reg1_o : out STD_LOGIC_VECTOR ( 0 to 0 );
    reg2_o : out STD_LOGIC_VECTOR ( 0 to 0 );
    reg3_o : out STD_LOGIC_VECTOR ( 0 to 0 );
    axil_aclk : in STD_LOGIC;
    axil_aresetn : in STD_LOGIC;
    axil_awaddr : in STD_LOGIC_VECTOR ( 7 downto 0 );
    axil_awprot : in STD_LOGIC_VECTOR ( 2 downto 0 );
    axil_awvalid : in STD_LOGIC;
    axil_awready : out STD_LOGIC;
    axil_wdata : in STD_LOGIC_VECTOR ( 31 downto 0 );
    axil_wstrb : in STD_LOGIC_VECTOR ( 3 downto 0 );
    axil_wvalid : in STD_LOGIC;
    axil_wready : out STD_LOGIC;
    axil_bresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
    axil_bvalid : out STD_LOGIC;
    axil_bready : in STD_LOGIC;
    axil_araddr : in STD_LOGIC_VECTOR ( 7 downto 0 );
    axil_arprot : in STD_LOGIC_VECTOR ( 2 downto 0 );
    axil_arvalid : in STD_LOGIC;
    axil_arready : out STD_LOGIC;
    axil_rdata : out STD_LOGIC_VECTOR ( 31 downto 0 );
    axil_rresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
    axil_rvalid : out STD_LOGIC;
    axil_rready : in STD_LOGIC
  );
  attribute NotValidForBitStream : boolean;
  attribute NotValidForBitStream of UDMA_comblock_0_0 : entity is true;
  attribute CHECK_LICENSE_TYPE : string;
  attribute CHECK_LICENSE_TYPE of UDMA_comblock_0_0 : entity is "UDMA_comblock_0_0,axi_comblock,{}";
  attribute downgradeipidentifiedwarnings : string;
  attribute downgradeipidentifiedwarnings of UDMA_comblock_0_0 : entity is "yes";
  attribute ip_definition_source : string;
  attribute ip_definition_source of UDMA_comblock_0_0 : entity is "package_project";
  attribute x_core_info : string;
  attribute x_core_info of UDMA_comblock_0_0 : entity is "axi_comblock,Vivado 2018.3";
end UDMA_comblock_0_0;

architecture STRUCTURE of UDMA_comblock_0_0 is
  signal \<const0>\ : STD_LOGIC;
  signal \^axil_rdata\ : STD_LOGIC_VECTOR ( 0 to 0 );
  attribute x_interface_info : string;
  attribute x_interface_info of axil_aclk : signal is "xilinx.com:signal:clock:1.0 axil_aclk CLK";
  attribute x_interface_parameter : string;
  attribute x_interface_parameter of axil_aclk : signal is "XIL_INTERFACENAME axil_aclk, ASSOCIATED_RESET axil_aresetn, ASSOCIATED_BUSIF AXIL, FREQ_HZ 100000000, PHASE 0.000, CLK_DOMAIN UDMA_processing_system7_0_0_FCLK_CLK0, INSERT_VIP 0";
  attribute x_interface_info of axil_aresetn : signal is "xilinx.com:signal:reset:1.0 axil_aresetn RST";
  attribute x_interface_parameter of axil_aresetn : signal is "XIL_INTERFACENAME axil_aresetn, POLARITY ACTIVE_LOW, INSERT_VIP 0";
  attribute x_interface_info of axil_arready : signal is "xilinx.com:interface:aximm:1.0 AXIL ARREADY";
  attribute x_interface_info of axil_arvalid : signal is "xilinx.com:interface:aximm:1.0 AXIL ARVALID";
  attribute x_interface_info of axil_awready : signal is "xilinx.com:interface:aximm:1.0 AXIL AWREADY";
  attribute x_interface_info of axil_awvalid : signal is "xilinx.com:interface:aximm:1.0 AXIL AWVALID";
  attribute x_interface_info of axil_bready : signal is "xilinx.com:interface:aximm:1.0 AXIL BREADY";
  attribute x_interface_info of axil_bvalid : signal is "xilinx.com:interface:aximm:1.0 AXIL BVALID";
  attribute x_interface_info of axil_rready : signal is "xilinx.com:interface:aximm:1.0 AXIL RREADY";
  attribute x_interface_info of axil_rvalid : signal is "xilinx.com:interface:aximm:1.0 AXIL RVALID";
  attribute x_interface_info of axil_wready : signal is "xilinx.com:interface:aximm:1.0 AXIL WREADY";
  attribute x_interface_info of axil_wvalid : signal is "xilinx.com:interface:aximm:1.0 AXIL WVALID";
  attribute x_interface_info of axil_araddr : signal is "xilinx.com:interface:aximm:1.0 AXIL ARADDR";
  attribute x_interface_info of axil_arprot : signal is "xilinx.com:interface:aximm:1.0 AXIL ARPROT";
  attribute x_interface_info of axil_awaddr : signal is "xilinx.com:interface:aximm:1.0 AXIL AWADDR";
  attribute x_interface_parameter of axil_awaddr : signal is "XIL_INTERFACENAME AXIL, DATA_WIDTH 32, PROTOCOL AXI4LITE, FREQ_HZ 100000000, ID_WIDTH 0, ADDR_WIDTH 8, AWUSER_WIDTH 0, ARUSER_WIDTH 0, WUSER_WIDTH 0, RUSER_WIDTH 0, BUSER_WIDTH 0, READ_WRITE_MODE READ_WRITE, HAS_BURST 0, HAS_LOCK 0, HAS_PROT 1, HAS_CACHE 0, HAS_QOS 0, HAS_REGION 0, HAS_WSTRB 1, HAS_BRESP 1, HAS_RRESP 1, SUPPORTS_NARROW_BURST 0, NUM_READ_OUTSTANDING 8, NUM_WRITE_OUTSTANDING 8, MAX_BURST_LENGTH 1, PHASE 0.000, CLK_DOMAIN UDMA_processing_system7_0_0_FCLK_CLK0, NUM_READ_THREADS 4, NUM_WRITE_THREADS 4, RUSER_BITS_PER_BYTE 0, WUSER_BITS_PER_BYTE 0, INSERT_VIP 0";
  attribute x_interface_info of axil_awprot : signal is "xilinx.com:interface:aximm:1.0 AXIL AWPROT";
  attribute x_interface_info of axil_bresp : signal is "xilinx.com:interface:aximm:1.0 AXIL BRESP";
  attribute x_interface_info of axil_rdata : signal is "xilinx.com:interface:aximm:1.0 AXIL RDATA";
  attribute x_interface_info of axil_rresp : signal is "xilinx.com:interface:aximm:1.0 AXIL RRESP";
  attribute x_interface_info of axil_wdata : signal is "xilinx.com:interface:aximm:1.0 AXIL WDATA";
  attribute x_interface_info of axil_wstrb : signal is "xilinx.com:interface:aximm:1.0 AXIL WSTRB";
  attribute x_interface_info of reg0_o : signal is "ictp:user:OREGS:1.0 OUT_REGS reg0_o";
  attribute x_interface_info of reg1_o : signal is "ictp:user:OREGS:1.0 OUT_REGS reg1_o";
  attribute x_interface_info of reg2_o : signal is "ictp:user:OREGS:1.0 OUT_REGS reg2_o";
  attribute x_interface_info of reg3_o : signal is "ictp:user:OREGS:1.0 OUT_REGS reg3_o";
begin
  axil_bresp(1) <= \<const0>\;
  axil_bresp(0) <= \<const0>\;
  axil_rdata(31) <= \<const0>\;
  axil_rdata(30) <= \<const0>\;
  axil_rdata(29) <= \<const0>\;
  axil_rdata(28) <= \<const0>\;
  axil_rdata(27) <= \<const0>\;
  axil_rdata(26) <= \<const0>\;
  axil_rdata(25) <= \<const0>\;
  axil_rdata(24) <= \<const0>\;
  axil_rdata(23) <= \<const0>\;
  axil_rdata(22) <= \<const0>\;
  axil_rdata(21) <= \<const0>\;
  axil_rdata(20) <= \<const0>\;
  axil_rdata(19) <= \<const0>\;
  axil_rdata(18) <= \<const0>\;
  axil_rdata(17) <= \<const0>\;
  axil_rdata(16) <= \<const0>\;
  axil_rdata(15) <= \<const0>\;
  axil_rdata(14) <= \<const0>\;
  axil_rdata(13) <= \<const0>\;
  axil_rdata(12) <= \<const0>\;
  axil_rdata(11) <= \<const0>\;
  axil_rdata(10) <= \<const0>\;
  axil_rdata(9) <= \<const0>\;
  axil_rdata(8) <= \<const0>\;
  axil_rdata(7) <= \<const0>\;
  axil_rdata(6) <= \<const0>\;
  axil_rdata(5) <= \<const0>\;
  axil_rdata(4) <= \<const0>\;
  axil_rdata(3) <= \<const0>\;
  axil_rdata(2) <= \<const0>\;
  axil_rdata(1) <= \<const0>\;
  axil_rdata(0) <= \^axil_rdata\(0);
  axil_rresp(1) <= \<const0>\;
  axil_rresp(0) <= \<const0>\;
GND: unisim.vcomponents.GND
     port map (
      G => \<const0>\
    );
U0: entity work.UDMA_comblock_0_0_axi_comblock
     port map (
      axil_aclk => axil_aclk,
      axil_araddr(5 downto 0) => axil_araddr(7 downto 2),
      axil_aresetn => axil_aresetn,
      axil_arready => axil_arready,
      axil_arvalid => axil_arvalid,
      axil_awaddr(5 downto 0) => axil_awaddr(7 downto 2),
      axil_awready => axil_awready,
      axil_awvalid => axil_awvalid,
      axil_bready => axil_bready,
      axil_bvalid => axil_bvalid,
      axil_rdata(0) => \^axil_rdata\(0),
      axil_rready => axil_rready,
      axil_rvalid => axil_rvalid,
      axil_wdata(0) => axil_wdata(0),
      axil_wready => axil_wready,
      axil_wvalid => axil_wvalid,
      reg0_o(0) => reg0_o(0),
      reg1_o(0) => reg1_o(0),
      reg2_o(0) => reg2_o(0),
      reg3_o(0) => reg3_o(0)
    );
end STRUCTURE;
