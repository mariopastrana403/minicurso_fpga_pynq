'''
Description: This scripts configures leds
'''

def leds():
	for i in range(10):
		led0 = 1
		led1 = 1
		led2 = 1
		led3 = 1
		app(f'x_write_reg 0 {led0}') # Led0
		app(f'x_write_reg 1 {led1}') # Led1  
		app(f'x_write_reg 2 {led2}') # Led2
		app(f'x_write_reg 3 {led3}') # Led3
		for i in range(10000000):
			pass
		led0 = 0
		led1 = 0
		led2 = 0
		led3 = 0
		app(f'x_write_reg 0 {led0}') # Led0    
		app(f'x_write_reg 1 {led1}') # Led1  
		app(f'x_write_reg 2 {led2}') # Led2
		app(f'x_write_reg 3 {led3}') # Led3
		for i in range(10000000):
			pass

leds()
