%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%Algoritmo para fazer a implementa��o do controlador PID
%%no rob� em V-REP com as constantes PID anal�ticas
%%Mario Andr�s Pastrana Triana
%%Estudante de mestrado da UnB
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%clear all
%clc
%% Para fazer a comunica��o com o V-REP
vrep=remApi('remoteApi');
vrep.simxFinish(-1);
clientID=vrep.simxStart('127.0.0.1',19999,true,true,5000,5);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
a=1;%% variavel para acabar a simula��o
%kpi=0.16;  %Kp anal�tica
%kii=0.29;  %Ki anal�tica
%kdi=0.052; %Kd anal�tica
kpi=0.4731;  %Kp bio MFO -1run
kii=0.5401;  %Ki bio MFO-1run
kdi=0.0233; %Kd bio MFO-1run
deltaT=0.05; % Tempo de amostragem
%unomenosalfaana=0.3092; %1-Alfa Filtro KP
%alfaana=0.6908;% Alfa analitico filtro KP
%unomenosalfaana = 0 % 1- Alfa bio
%alfaana= 1          % Alfana bio
raizes = roots([kdi kpi kii]);
absoluto = abs(raizes);       % Valor absoluto das raices
mayor = max(absoluto);        % Valor maximo das raices
e1i = 1/(mayor*10);            % Valor do filtro para o Kp
unomenosalfaana = exp(-(deltaT/e1i));
alfaana = 1 - unomenosalfaana;
interror=0; % Error Integral
deerror=0;  % Error deribativo
errorant=0; % Error anterior
fant=0;     % filtro da media movel exponencial anterior
i=0;        % Contador das amostras
numero_muestras=100; % N�mero de amostras
x=1:numero_muestras  % Para graficar o valor do sensor IR
pii = 0
while (clientID>-1) % Loop para iniciar a simula��o
    disp('Conected'); %Apresenta que foi feita a comunica��o com V-REP
    while (a==1) % Loop de simula��o
        i=i+1;    % Sumatoria de n�mero de amostras
        if (i==numero_muestras) % Quando o contador de amostras � igual ao n�mero de amostras a simula��o para
            a=0; % Para a simula��o
        end
        [returnCode,Motori]=vrep.simxGetObjectHandle(clientID,'motori',vrep.simx_opmode_blocking); %obtem o objeto do motor esquerdo
        [returnCode,Motord]=vrep.simxGetObjectHandle(clientID,'motord',vrep.simx_opmode_blocking); %obtem o objeto do motor direito
        [returnCode,cuerpo]=vrep.simxGetObjectHandle(clientID,'chasis',vrep.simx_opmode_blocking); %obtem o objeto do chasis
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%Sensores de proximidad
        [returnCode,sensord1]=vrep.simxGetObjectHandle(clientID,'sensor1',vrep. simx_opmode_oneshot_wait); %obtem o objeto do sensor IR
        [rtd1,sd1,sdd1]=vrep.simxReadProximitySensor(clientID,sensord1,vrep.simx_opmode_oneshot_wait);    %obtem o valor do sensor IR em "m" 
        rsensord1a=sdd1(:,3); %Obtem somente a distancia no eixo Z
        if rsensord1a<0.0001 || rsensord1a>=0.8 %Quando o sensor faz dete��o de um objeto coloca Zero, mudase para 80 cm
            rsensord1a=0.8;
        end
        ideal=30; % Set Point
        inputpid=double(rsensord1a*100); %Conver��o da entrada do sensor para cm
        disp('Distance cm.');
        disp(inputpid); %Distancia medida em cm do sensor
        plotinput(i)=inputpid; %Variavel para fazer a gr�fica do sensor
        error=ideal-inputpid;  %Eror da distancia ideal
        interror=(interror+error); %Error da integral
        f(i)=unomenosalfaana*fant+alfaana*error; %Filtro da media movel exponencial
        fant=f(i);
        if (i==1)
            deerror(i)=(f(i))/(0.05);  %%Error derivativo num primeiro instante
        else
            deerror(i)=(f(i)-f(i-1))/(0.05); %% Error derivativo
        end
        pii(i)= double( kpi*error+kii*interror*deltaT+deerror(i)*kdi); %Contolador PID da roda izquerda
        %pii=pii+3; %Quando o error � zero a roda diereita e a roda esquerda tem a mesma velocidade
        pdi=3; %Velocidade da roda direita
        if (pii(i) >= 5)
            pii(i) = 5;
        elseif (pii(i)<0)
            pii(i) = 0;
        end
        [returnCode]=vrep.simxSetJointTargetVelocity(clientID,Motori,pii(i),vrep.simx_opmode_blocking);%Velocidade encaminhada para V-REP da roda esquerda
        [returnCode]=vrep.simxSetJointTargetVelocity(clientID,Motord,pdi,vrep.simx_opmode_blocking);  %Velocidade encaminhada para V-REP da roda direita
        pause(0.05); %Tempo de amostragem
    end
    %vrep.simxFinish(-1);
    break
end
t = 1:1:numero_muestras;
t = t.*0.05;
setpoint=ones(numero_muestras)*ideal; %Setpoint
figure(4)
plot(t,plotinput,"b-*")
hold on
plot(t,setpoint,"r-o"); % Gr�fica Setpoint
grid()
xlabel("Time (s)")
ylabel("SISO system output (cm)")