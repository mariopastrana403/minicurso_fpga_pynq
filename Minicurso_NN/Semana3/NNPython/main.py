# This is a sample Python script.

# Press Shift+F10 to execute it or replace it with your code.
# Press Double Shift to search everywhere for classes, files, tool windows, actions, and settings.

from math import e
import matplotlib.pyplot as plt
import numpy as np

class ImplementNN:

    def __init__(self):
        self.ys = []
        self.fitVector = []
        self.numero_muestras = 250

    def exp(self, x):
        return e**x

    def Sigmoid(self, s):
        return 1/(1+self.exp(-s))

    def Sensorfucntion(self, x):
        y = ((0.0005)*(x**2)) - (0.3933*x) + 88.718
        return y

    def NN (self, x, input, IdealOut, NumAmostras):

        sumErrorR = 0

        for i in range(NumAmostras):
            inputnormal = (input[i] - 200)/250
            sensorNN = (x[0]*(inputnormal**2)) + x[1]*inputnormal + x[2]
            # sensorNN = (x[0] * inputnormal + x[1])
            sensorNNs = self.Sigmoid(sensorNN)
            sensordes = (sensorNNs * 30)
            sumErrorR = sumErrorR + ((sensordes - IdealOut[i]) * (sensordes - IdealOut[i]))

        MSER = sumErrorR / NumAmostras
        mse = MSER
        # print("MSE")
        # print(mse)
        return mse

    def PSO(self, input, IdealOut):
        S = 10
        N = 3
        maxIter = 100
        w0 = 0.9
        wf = 0.1
        c1 = 2.05
        c2 = 2.05
        vMax = 10
        vIni = vMax / 10
        xMax = 10
        xMin = -10
        '''
            Unpack the configuration parameters from key arguments.
        '''
        fitVector = []

        '''
            PSO Initializations
        '''
        w, dw = w0, (wf - w0) / maxIter
        x = xMin + (xMax - xMin) * np.random.rand(S, N)
        y, v = 1e10 * np.ones((S, N)), vIni * np.ones((S, N))
        fInd, k = 1e10 * np.ones(S), 1

        '''
            PSO Main Loop
        '''
        print("PSO")
        while k <= maxIter:
            print(k)
            '''
                Loop to find the best individual particle
            '''
            for i in range(S):

                fx = self.NN(x[i, :], input, IdealOut, self.numero_muestras)
                # print(fx)
                # print("___________________")
                if fx < fInd[i]:
                    y[i, :] = x[i, :]
                    fInd[i] = fx

            '''
                Find the best overall particle from the swarm
            '''
            bestFit = min(fInd)
            self.fitVector.append(bestFit)
            p = np.where(fInd == bestFit)[0][0]
            self.ys = y[p, :]
            print(self.ys)
            print(bestFit)

            '''
                Particles' speed update using inertia factor.
            '''
            for j in range(N):
                for i in range(S):
                    u1, u2 = np.random.rand(), np.random.rand()
                    v[i, j] = w * v[i, j] + c1 * u1 * (y[i, j] - x[i, j]) + c2 * u2 * (self.ys[j] - x[i, j])
                    v[i, j] = min(vMax, max(-vMax, v[i, j]))

                    x[i, j] += v[i, j]
                    if x[i, j] > xMax: x[i, j] = xMax - np.random.rand()
                    if x[i, j] < xMin: x[i, j] = xMin + np.random.rand()

            k += 1
            w += dw
        print("--------ys-------")
        print(self.ys)
        print(self.fitVector)
        print("--------ys-------")
        plt.plot(self.fitVector)
        plt.title("PSO")
        plt.show()
        return self.ys


# Press the green button in the gutter to run the script.
if __name__ == '__main__':
    TestVectorinput = []
    TestVectoroutput = []
    TestvectoNNdes = []
    TestvectoNN = 0
    TestvectoNNS = 0
    classNN = ImplementNN()
    for i in range(200, 450):
        TestVectorinput.append(i)
        TestVectoroutput.append(classNN.Sensorfucntion(i))

    pesosebias = classNN.PSO(TestVectorinput, TestVectoroutput)

    for i in range(200, 450):
        inputnormal = (i - 200) / 250
        TestvectoNN = (pesosebias[0]*(inputnormal**2)) + pesosebias[1]*inputnormal + pesosebias[2] # GMDH
        #TestvectoNN = pesosebias[0] * inputnormal + pesosebias[1]   # Clasica
        TestvectoNNS = classNN.Sigmoid(TestvectoNN)
        TestvectoNN = (TestvectoNNS * 30)
        TestvectoNNdes.append(TestvectoNN)


    plt.plot(TestVectoroutput)
    plt.plot(TestvectoNNdes)
    plt.title("Com paracion Ecuación VS NN")
    plt.show()

