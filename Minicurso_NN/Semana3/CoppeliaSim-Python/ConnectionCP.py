"""
ConnectionCP.py

This code connect from Python3 to Coppeliasim

Author:
    Mario Pastrana (mariopastrana403@gmail.com)
            MARIA PROJECT - Universidade de Brasília

Version:
    0.0.1 (beta)

Release Date:
    Jan 23, 2023
"""

import sim
import numpy as np
import pandas as pd
import scipy
import matplotlib.pyplot as plt
from math import e

class LFDmethodology():

    def __init__(self):
        self.rvelocidadf = []
        self.rsensorf = []
        self.numero_muestras = 100
        self.ys = []
        self.fitVector = []
        self.valuerecRI = 0
        self.valuerecLI = 0

    def exp(self, x):
        return e**x

    def Sigmoid(self, s):
        return 1/(1+self.exp(-s))

    def NN32b(self, x, rsensor, rspeed, NumAmostras, ELFD):
        sumSR = 0
        sumSL = 0
        MsumSR = 0
        MsumSL = 0
        if ELFD == 1:
            for i in range(NumAmostras):
                NNSR = x[0]*rsensor[0][i] + x[1]*rsensor[1][i] + x[2]*rsensor[2][i] + x[3]*rsensor[3][i] + x[4]*rsensor[4][i] + x[5]*rsensor[5][i] + x[12]
                NNSL = x[6]*rsensor[0][i] + x[7]*rsensor[1][i] + x[8]*rsensor[2][i] + x[9]*rsensor[3][i] + x[10]*rsensor[4][i] + x[11]*rsensor[5][i] + x[13]
                NNSRS = self.Sigmoid(NNSR)
                NNSLS = self.Sigmoid(NNSL)
                sumSR = sumSR + ((NNSRS - rspeed[0][i]) * (NNSRS - rspeed[0][i]))
                sumSL = sumSL + ((NNSLS - rspeed[1][i]) * (NNSLS - rspeed[1][i]))

            MsumSR = sumSR/NumAmostras
            MsumSL = sumSL/NumAmostras
            mse = MsumSR + MsumSL
            return mse
        else:

            NNSR = x[0] * rsensor[0] + x[1] * rsensor[1] + x[2] * rsensor[2] + x[3] * rsensor[3] + x[4] * rsensor[4] + \
                   x[5] * rsensor[5] + x[12]
            NNSL = x[6] * rsensor[0] + x[7] * rsensor[1] + x[8] * rsensor[2] + x[9] * rsensor[3] + x[10] * rsensor[4] + \
                   x[11] * rsensor[5] + x[13]
            NNSRS = self.Sigmoid(NNSR)
            NNSLS = self.Sigmoid(NNSL)

            self.valuerecRI = NNSRS
            self.valuerecLI = NNSLS


    def PSO(self,rsensor, rspeed):
        S = 10
        N = 14
        maxIter = 500
        w0 = 0.9
        wf = 0.1
        c1 = 2.05
        c2 = 2.05
        vMax = 10
        vIni = vMax / 10
        xMax = 10
        xMin = -10
        '''
            Unpack the configuration parameters from key arguments.
        '''
        fitVector = []

        '''
            PSO Initializations
        '''
        w, dw = w0, (wf - w0) / maxIter
        x = xMin + (xMax - xMin) * np.random.rand(S, N)
        y, v = 1e10 * np.ones((S, N)), vIni * np.ones((S, N))
        fInd, k = 1e10 * np.ones(S), 1

        '''
            PSO Main Loop
        '''
        print("PSO")
        while k <= maxIter:
            print(k)
            '''
                Loop to find the best individual particle
            '''
            for i in range(S):

                fx = self.NN32b(x[i, :], rsensor, rspeed, self.numero_muestras, 1)
                # print(fx)
                # print("___________________")
                if fx < fInd[i]:
                    y[i, :] = x[i, :]
                    fInd[i] = fx

            '''
                Find the best overall particle from the swarm
            '''
            bestFit = min(fInd)
            self.fitVector.append(bestFit)
            p = np.where(fInd == bestFit)[0][0]
            self.ys = y[p, :]
            # print(self.ys)
            # print(bestFit)

            '''
                Particles' speed update using inertia factor.
            '''
            for j in range(N):
                for i in range(S):
                    u1, u2 = np.random.rand(), np.random.rand()
                    v[i, j] = w * v[i, j] + c1 * u1 * (y[i, j] - x[i, j]) + c2 * u2 * (self.ys[j] - x[i, j])
                    v[i, j] = min(vMax, max(-vMax, v[i, j]))

                    x[i, j] += v[i, j]
                    if x[i, j] > xMax: x[i, j] = xMax - np.random.rand()
                    if x[i, j] < xMin: x[i, j] = xMin + np.random.rand()

            k += 1
            w += dw
        print("--------ys-------")
        print(self.ys)
        print(self.fitVector)
        print("--------ys-------")
        plt.plot(self.fitVector)
        plt.title("PSO")
        plt.show()


    def connect(self, port):
        sim.simxFinish(-1)
        clientID = sim.simxStart('127.0.0.1', port, True, True, 2000, 5)
        if clientID == 0:
            print("Conectado a", port)
        else:
            print("no se pudo conectar")
        return clientID

    def dataAdquicition(self):
        clientID = self.connect(19999)
        a = 1
        i = 0
        position = []
        sdd1 = []
        rsensord1 = []
        rsensord2 = []
        rsensord3 = []
        rsensord4 = []
        rsensord5 = []
        rsensord6 = []
        mdg = []
        mig = []

        if (sim.simxGetConnectionId(clientID) !=-1):
            print("Connect")
            while (a == 1):
                print(i)
                position.append([])
                if i == self.numero_muestras:
                    a = 2
                else:
                    a = 1
                #%%% Obtendos os objetos dos motores e a posi��o do rob� %%%
                returnCode,Motori = sim.simxGetObjectHandle(clientID, 'EngineL',
                                                            sim.simx_opmode_blocking) #Obteniendo el objeto "Pioneer_p3dx_leftmotor" de v-rep (motor izquierdo)
                returnCode, Motord = sim.simxGetObjectHandle(clientID, 'EngineR',
                                                             sim.simx_opmode_blocking) # Obteniendo el objeto "Pioneer_p3dx_rightMotor" de v - rep(motor derecho)
                returnCode, cuerpo = sim.simxGetObjectHandle(clientID, 'Chasis6',
                                                             sim.simx_opmode_blocking) # Obteniendo el objeto "Pioneer_p3dx" de v - rep(Robot Movil)
                #%%% Obtendos os objetos dos sensores do rob� %%%
                returnCode, sensord1 = sim.simxGetObjectHandle(clientID, 'Proximity_sensor4',
                                                                  sim.simx_opmode_oneshot_wait)
                returnCode, sensord2 = sim.simxGetObjectHandle(clientID, 'Proximity_sensor5',
                                                                  sim.simx_opmode_oneshot_wait)
                returnCode, sensord3 = sim.simxGetObjectHandle(clientID, 'Proximity_sensor6',
                                                                  sim.simx_opmode_oneshot_wait)
                returnCode, sensord4 = sim.simxGetObjectHandle(clientID, 'Proximity_sensor7',
                                                                  sim.simx_opmode_oneshot_wait)
                returnCode, sensord5 = sim.simxGetObjectHandle(clientID, 'Proximity_sensor8',
                                                                  sim.simx_opmode_oneshot_wait)
                returnCode, sensord6 = sim.simxGetObjectHandle(clientID, 'Proximity_sensor9',
                                                                  sim.simx_opmode_oneshot_wait)
                #%%% Obtendo os valores de distancia de cada objeto %%%

                rtd1, sd1, sdd1,_,_ = sim.simxReadProximitySensor(clientID, sensord1, sim.simx_opmode_oneshot_wait)
                rtd2, sd2, sdd2,_,_ = sim.simxReadProximitySensor(clientID, sensord2, sim.simx_opmode_oneshot_wait)
                rtd3, sd3, sdd3,_,_ = sim.simxReadProximitySensor(clientID, sensord3, sim.simx_opmode_oneshot_wait)
                rtd4, sd4, sdd4,_,_ = sim.simxReadProximitySensor(clientID, sensord4, sim.simx_opmode_oneshot_wait)
                rti1, sd5, sdd5,_,_ = sim.simxReadProximitySensor(clientID, sensord5, sim.simx_opmode_oneshot_wait)
                rti1, sd6, sdd6,_,_ = sim.simxReadProximitySensor(clientID, sensord6, sim.simx_opmode_oneshot_wait)
                s, position[i] = sim.simxGetObjectPosition(clientID, cuerpo, -1, sim.simx_opmode_streaming)
                rsensord1.append(sdd1[2])
                if rsensord1[i] < 0.05 or rsensord1[i] > 0.9:
                    rsensord1[i] = 0.9
                rsensord2.append(sdd2[2])
                if rsensord2[i] < 0.05 or rsensord1[i] > 0.9:
                    rsensord2[i] = 0.9
                rsensord3.append(sdd3[2])
                if rsensord3[i] < 0.05 or rsensord1[i] > 0.9:
                    rsensord3[i] = 0.9
                rsensord4.append(sdd4[2])
                if rsensord4[i] < 0.05 or rsensord1[i] > 0.9:
                    rsensord4[i] = 0.9
                rsensord5.append(sdd5[2])
                if rsensord5[i] < 0.05 or rsensord1[i] > 0.9:
                    rsensord5[i] = 0.9
                rsensord6.append(sdd6[2])
                if rsensord6[i] < 0.05 or rsensord1[i] > 0.9:
                    rsensord6[i] = 0.9
                # %%% Obten��o das velocidades do rob� em cm/s %%%
                returnCode, md, _ = sim.simxGetObjectVelocity(clientID, Motord, sim.simx_opmode_blocking); # Obtendo a velocidade da roda direita
                returnCode, mi, _ = sim.simxGetObjectVelocity(clientID, Motori, sim.simx_opmode_blocking); # Obtendo a velocidade da roda esquerda
                mdg.append((md[1]**2 + md[0]**2)**1/2);
                mig.append((mi[1]**2 + mi[0]**2)** 1/2);
                i = i + 1

            self.rsensorf = [rsensord1, rsensord2, rsensord3, rsensord4, rsensord5, rsensord6]
            Vd = scipy.signal.medfilt(mdg, kernel_size=3)
            Vi = scipy.signal.medfilt(mig, kernel_size=3)
            self.rvelocidadf = [Vd, Vi]
            dframedata = [rsensord1, rsensord2, rsensord3, rsensord4, rsensord5, rsensord6,Vd, Vi]
            dfd = pd.DataFrame(dframedata)
            dfd.to_csv("DatatoTraining.csv")

            print("stop")
            # %%% Satura��o dos sensores, Nota: Aqui n�o tem necesita de normalizar, devido a que a distancia esta normalizada desde V-REP %%%

    def training(self):
        print("Training")
        trainingobject = self.PSO(self.rsensorf, self.rvelocidadf)

    def imitation(self, WB):

        clientID = self.connect(19999)
        a = 1
        i = 0
        position = []
        rsensord1 = []
        rsensord2 = []
        rsensord3 = []
        rsensord4 = []
        rsensord5 = []
        rsensord6 = []

        if (sim.simxGetConnectionId(clientID) != -1):
            print("Connect")
            while (a == 1):
                print(i)
                position.append([])
                if i == self.numero_muestras*5:
                    a = 2
                else:
                    a = 1
                # %%% Obtendos os objetos dos motores e a posi��o do rob� %%%
                returnCode, Motori = sim.simxGetObjectHandle(clientID, 'EngineL',
                                                             sim.simx_opmode_blocking)  # Obteniendo el objeto "Pioneer_p3dx_leftmotor" de v-rep (motor izquierdo)
                returnCode, Motord = sim.simxGetObjectHandle(clientID, 'EngineR',
                                                             sim.simx_opmode_blocking)  # Obteniendo el objeto "Pioneer_p3dx_rightMotor" de v - rep(motor derecho)
                returnCode, cuerpo = sim.simxGetObjectHandle(clientID, 'Chasis6',
                                                             sim.simx_opmode_blocking)  # Obteniendo el objeto "Pioneer_p3dx" de v - rep(Robot Movil)
                # %%% Obtendos os objetos dos sensores do rob� %%%
                returnCode, sensord1 = sim.simxGetObjectHandle(clientID, 'Proximity_sensor4',
                                                               sim.simx_opmode_oneshot_wait)
                returnCode, sensord2 = sim.simxGetObjectHandle(clientID, 'Proximity_sensor5',
                                                               sim.simx_opmode_oneshot_wait)
                returnCode, sensord3 = sim.simxGetObjectHandle(clientID, 'Proximity_sensor6',
                                                               sim.simx_opmode_oneshot_wait)
                returnCode, sensord4 = sim.simxGetObjectHandle(clientID, 'Proximity_sensor7',
                                                               sim.simx_opmode_oneshot_wait)
                returnCode, sensord5 = sim.simxGetObjectHandle(clientID, 'Proximity_sensor8',
                                                               sim.simx_opmode_oneshot_wait)
                returnCode, sensord6 = sim.simxGetObjectHandle(clientID, 'Proximity_sensor9',
                                                               sim.simx_opmode_oneshot_wait)
                # %%% Obtendo os valores de distancia de cada objeto %%%

                rtd1, sd1, sdd1, _, _ = sim.simxReadProximitySensor(clientID, sensord1, sim.simx_opmode_oneshot_wait)
                rtd2, sd2, sdd2, _, _ = sim.simxReadProximitySensor(clientID, sensord2, sim.simx_opmode_oneshot_wait)
                rtd3, sd3, sdd3, _, _ = sim.simxReadProximitySensor(clientID, sensord3, sim.simx_opmode_oneshot_wait)
                rtd4, sd4, sdd4, _, _ = sim.simxReadProximitySensor(clientID, sensord4, sim.simx_opmode_oneshot_wait)
                rti1, sd5, sdd5, _, _ = sim.simxReadProximitySensor(clientID, sensord5, sim.simx_opmode_oneshot_wait)
                rti1, sd6, sdd6, _, _ = sim.simxReadProximitySensor(clientID, sensord6, sim.simx_opmode_oneshot_wait)
                s, position[i] = sim.simxGetObjectPosition(clientID, cuerpo, -1, sim.simx_opmode_streaming)
                rsensord1.append(sdd1[2])
                if rsensord1[i] < 0.05 or rsensord1[i] > 0.9:
                    rsensord1[i] = 0.9
                rsensord2.append(sdd2[2])
                if rsensord2[i] < 0.05 or rsensord1[i] > 0.9:
                    rsensord2[i] = 0.9
                rsensord3.append(sdd3[2])
                if rsensord3[i] < 0.05 or rsensord1[i] > 0.9:
                    rsensord3[i] = 0.9
                rsensord4.append(sdd4[2])
                if rsensord4[i] < 0.05 or rsensord1[i] > 0.9:
                    rsensord4[i] = 0.9
                rsensord5.append(sdd5[2])
                if rsensord5[i] < 0.05 or rsensord1[i] > 0.9:
                    rsensord5[i] = 0.9
                rsensord6.append(sdd6[2])
                if rsensord6[i] < 0.05 or rsensord1[i] > 0.9:
                    rsensord6[i] = 0.9
                rsensor = [rsensord1[i], rsensord2[i], rsensord3[i], rsensord4[i], rsensord5[i], rsensord6[i]]
                self.NN32b(self.ys, rsensor, self.rvelocidadf, self.numero_muestras, 2)
                # self.NN32b(WB, rsensor, self.rvelocidadf, self.numero_muestras, 2)
                sim.simxSetJointTargetVelocity(clientID, Motord, ((self.valuerecRI*-1)/0.002275), sim.simx_opmode_blocking);
                sim.simxSetJointTargetVelocity(clientID, Motori, ((self.valuerecLI*-1)/0.002275), sim.simx_opmode_blocking);
                i = i + 1

if __name__=="__main__":

    lfdobject = LFDmethodology()
    lfdobject.dataAdquicition()
    lfdobject.training()
    WB = [-9.3293, 5.9141, -7.6094, 7.7630, -0.67085, -2.555, -5.9899, -8.5990, 9.1741, -9.5473, 7.7539, -4.7407, 0.0057056, 0.5030]
    lfdobject.imitation(WB)
