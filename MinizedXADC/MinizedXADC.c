/******************************************************************************
*
* Copyright (C) 2009 - 2014 Xilinx, Inc.  All rights reserved.
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
*
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
*
* Use of the Software is limited solely to applications:
* (a) running on a Xilinx device, or
* (b) that interact with a Xilinx device through a bus or interconnect.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
* XILINX  BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
* WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF
* OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
* SOFTWARE.
*
* Except as contained in this notice, the name of the Xilinx shall not be used
* in advertising or otherwise to promote the sale, use or other dealings in
* this Software without prior written authorization from Xilinx.
*
******************************************************************************/

/*
 * helloworld.c: simple test application
 *
 * This application configures UART 16550 to baud rate 9600.
 * PS7 UART (Zynq) is not initialized by this application, since
 * bootrom/bsp configures it to baud rate 115200
 *
 * ------------------------------------------------
 * | UART TYPE   BAUD RATE                        |
 * ------------------------------------------------
 *   uartns550   9600
 *   uartlite    Configurable only in HW design
 *   ps7_uart    115200 (configured by bootrom/bsp)
 */

/*Reflexión final
 * Querido lector actualmente el funcionamiento de este código lo sabes Dios, la virgen, los santos y
 * Mario Pastrana
 */
#include <stdio.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>
#include <math.h>
#include "platform.h"
#include "xil_printf.h"
#include "xparameters.h"
#include "xil_io.h"
#include "xadcps.h"
#include "xuartps.h"
#include "xtime_l.h"
#include "xgpiops.h"

XAdcPs XAdc;
XAdcPs_Config *ConfigPtr;

int XAdcFractionToInt(float FloatNum)
{
	float Temp;

	Temp = FloatNum;
	if (FloatNum < 0) {
		Temp = -(FloatNum);
	}
	return( ((int)((Temp -(float)((int)Temp)) * (1000000.0f))));
}


int main()
{
	u32 VccPintRawData0=0;
	// u32 VccPintRawData1=0;
	// u32 VccPintRawData8=0;
	//u32 VccPintRawData9=0;
	float data0=0;
	//data1=0, data8=0, data9=0;
	//u8 Aux0 = 3;
	init_platform ();
	int status;
	printf ("Zynq using vivado \n\r");
	//XADC initialization
	ConfigPtr = XAdcPs_LookupConfig(XPAR_XADCPS_0_DEVICE_ID);
	if (ConfigPtr == NULL) {
			return XST_FAILURE;
		}
	XAdcPs_CfgInitialize(&XAdc, ConfigPtr, ConfigPtr->BaseAddress);
	status = XAdcPs_SelfTest(&XAdc);
	if (status != XST_SUCCESS) {
		return XST_FAILURE;
	}
	///////////////
	xil_printf("Init program \n");
	int i = 0;
	for (i=0;i<=300;i=i+1)
	{
		xil_printf("Iteration %d \n", i);
		VccPintRawData0 = XAdcPs_GetAdcData(&XAdc, XADCPS_CH_AUX_MIN+0);
		//VccPintRawData1 = XAdcPs_GetAdcData(&XAdc, XADCPS_CH_AUX_MIN+1);
		//VccPintRawData8 = XAdcPs_GetAdcData(&XAdc, XADCPS_CH_AUX_MIN+8);
		//VccPintRawData9 = XAdcPs_GetAdcData(&XAdc, XADCPS_CH_AUX_MIN+9);
		data0 = XAdcPs_RawToVoltage(VccPintRawData0);
		//data1 = XAdcPs_RawToVoltage(VccPintRawData1);
		//data8 = XAdcPs_RawToVoltage(VccPintRawData8);
		//data9 = XAdcPs_RawToVoltage(VccPintRawData9);
		xil_printf("Tensão 0 ==> %d.%d \n\r",(int)(data0), XAdcFractionToInt(data0));
		xil_printf("raw 0 ==> %d.%d \n\r",(int)(VccPintRawData0), XAdcFractionToInt(VccPintRawData0));
		//xil_printf("Tensão 1 ==> %d.%d \n\r",(int)(data1), XAdcFractionToInt(data1));
		//xil_printf("raw 1 ==> %d.%d \n\r",(int)(VccPintRawData1), XAdcFractionToInt(VccPintRawData1));
		//xil_printf("Tensão 8 ==> %d.%d \n\r",(int)(data8), XAdcFractionToInt(data8));
		//xil_printf("raw 8 ==> %d.%d \n\r",(int)(VccPintRawData8), XAdcFractionToInt(VccPintRawData8));
		//xil_printf("Tensão 9 ==> %d.%d \n\r",(int)(data9), XAdcFractionToInt(data9));
		//xil_printf("raw 9 ==> %d.%d \n\r",(int)(VccPintRawData9), XAdcFractionToInt(VccPintRawData9));
		sleep(1);
	}

}

